/**
 * Created by bokanstruphansen on 14/06/16.
*/



var section_images_meta = {
    1: {svg_id: 'psyk_large', anim_function: happy_anim}
};

function happy_anim(doc){

    var mtl = new TimelineMax();

    mtl.insert(rotateButterfly(doc, "#butterfly"));
    mtl.insert(moveUpAndDown(doc, "#glasses"));
    mtl.insert(sway(doc, "#psyk"));
    mtl.insert(twitch_right(doc, "#eyebrow_x5F_right", "down", 0.5, 2));
    mtl.insert(twitch_right(doc, "#eyebrow_x5F_right", "up", 0.5, 4));
    mtl.insert(twitch_left(doc, "#eyebrow_x5F_left", "down", 0.3, 2));
    //mtl.insert(move_glasses(doc, "#glasses", "up", 0.5, 4));

    return mtl;
}

var defaultPos = {
  y: 0,
  x: 0,
  rotationZ: 0
}

function sway(doc, elem_id){
    console.log('anim sway', elem_id);

    var balloon = doc.querySelector(elem_id);
    var tl = new TimelineMax({
        repeat: -1,
        ease:Linear.easeNone
    });

    //Up and down motion
    tl.insert(TweenMax.fromTo(balloon, 3, {
        y: 0
    }, {
        y: -25,
        repeat: -1,
        yoyo: true,
        //ease:Linear.easeNone
        ease: Quad.easeInOut
    }));
    //Pendulum motion
    tl.insert(TweenMax.fromTo(balloon, 2, {
        rotation: 1
    }, {
        transformOrigin: "70% 50%",
        rotation: -1,
        repeat: -1,
        yoyo: true,
        ease: Quad.easeInOut
    }));

    return tl;
}

function twitch_left(doc, elem_id, direction, duration, delay){
  console.log('twitch right', elem_id);

  var twitch = {
    y: 25,
    x: 10,
    rotationZ: -30,
    transformOrigin:"bottom center",
    ease: Quad.easeInOut
  }
  var tl = new TimelineMax();

  if (direction =="up"){
    tl.to(doc.querySelector(elem_id), duration, defaultPos, delay);
  } else {
    tl.to(doc.querySelector(elem_id), duration, twitch, delay);
  }
  return tl;
}
function twitch_right(doc, elem_id, direction, duration, delay){
  console.log('twitch right', elem_id);

  var twitch = {
    y: 25,
    x: 10,
    rotationZ: 30,
    transformOrigin:"bottom center",
    ease: Quad.easeInOut
  }
  var tl = new TimelineMax();

  if (direction =="up"){
    tl.to(doc.querySelector(elem_id), duration, defaultPos, delay);
  } else {
    tl.to(doc.querySelector(elem_id), duration, twitch, delay);
  }
  return tl;
}

function rotateButterfly(doc, elem1){
    console.log('anim', doc);

    var tl = new TimelineMax({repeat: -1});

    tl.insert(TweenMax.to(doc.querySelector(elem1), 1, {rotationZ:720, transformOrigin:"51% 55%", ease:Circ.easeInOut, delay: 5}));

    return tl;
}

function submarine_anim(doc){

    var mtl = new TimelineMax();
    console.log('Loaded submarine');

    mtl.add(rock_submarine(doc, "#sub"));
    mtl.insert(moveGround(doc, -472.6, 10));
    mtl.insert(rotatePropeller(doc));
    mtl.insert(moveUpAndDown(doc, "#periscope"));
    createBubble(doc.querySelector("#bubble1"), 0);
    createBubble(doc.querySelector("#bubble2"), 0.2);
    createBubble(doc.querySelector("#bubble3"), 0.5);

    return mtl;
}


function fishing_ship_anim(doc){
    var mtl = new TimelineMax();
    console.log('Loaded fishing ship');

    mtl.add(rock_fishing_ship(doc, "#ship"));
    mtl.insert(moveGround(doc, -730, 20));
    mtl.insert(rotatePropeller(doc));
    mtl.insert(moveCloud(doc, '#cloud_3', 730, 424, 15));
    mtl.insert(moveCloud(doc, '#cloud_2', 730, 250, 8));
    mtl.insert(moveCloud(doc, '#cloud_1', 730, 99, 22));
    mtl.insert(rotateFlag(doc, "#flag"));

    return mtl;

}

function balloon_anim(doc){
    var mtl = new TimelineMax();
    console.log('Loaded balloon');

    mtl.add(float_balloon(doc, "#balloon"));
    mtl.insert(moveGround(doc, -360, 30));

    mtl.insert(moveCloud(doc, '#cloud_bottom', 730, 427, 50));
    mtl.insert(moveCloud(doc, '#cloud_top', 730, 435, 25));
    mtl.insert(moveCloud(doc, '#cloud_middle', 730, 99, 15));

    return mtl;

}

function space_shuttle_anim(doc){
    var mtl = new TimelineMax();
    console.log('Loaded Space shuttle');

    mtl.add(orbit_motion(doc, '#satellite'));

    mtl.insert(blinking(doc, "#star-1", 0.8, 2));
    mtl.insert(blinking(doc, "#star-2", 0.8, 1));
    mtl.insert(blinking(doc, "#star-3", 0.8, 5));

    mtl.insert(rolling_ball(doc, "#planet", 30, 360));

    mtl.insert(flying_space_shuttle(doc, "#shuttle"));

    return mtl;

}

function car_anim(doc){
    console.log('Loaded Car');

    var mtl = new TimelineMax();

    mtl.add(moveCloud(doc, '#cloud', 730, 196, 18));
    mtl.insert(moveCloud(doc, '#tree', 730, 500, 5));
    mtl.insert(driving_vehicle(doc, "#car"));
    mtl.insert(rolling_ball(doc, "#wheel-front", 0.8, -360));
    mtl.insert(rolling_ball(doc, "#wheel-back", 0.8, -360));

    mtl.insert(change_light(doc, "#head_light_light", 1, 2, "rgb(255,255,0)"));
    mtl.insert(change_light(doc, "#rear_light_light", 1, 2, "rgb(255,0,0)"));

    return mtl;

}

function scooter_anim(doc){
    var mtl = new TimelineMax();

    console.log('Loaded scooter');

    mtl.add(moveCloud(doc, '#cloud', 730, 196, 18));
    mtl.insert(moveCloud(doc, '#tree', 730, 500, 5));
    mtl.insert(driving_vehicle(doc, "#scooter_1_"));
    mtl.insert(rolling_ball(doc, "#wheel-front", 0.8, -360));
    mtl.insert(rolling_ball(doc, "#wheel-back", 0.8, -360));
    mtl.insert(moveGround(doc, -1405, 30));
    mtl.insert(scarf_in_wind(doc, 68.8,1));
    return mtl;
}

function plane_anim(doc){

    var mtl = new TimelineMax();
    console.log('Loaded Plane');

    mtl.add(flying_plane(doc, "#plane"));

    mtl.insert(moveCloud(doc, '#cloud-back-1', 730, 99, 33));
    mtl.insert(moveCloud(doc, '#cloud-back-2', 730, 390, 25));
    mtl.insert(moveCloud(doc, '#cloud-back-3', 730, 217, 20));
    mtl.insert(moveCloud(doc, '#cloud-front-1', 730, 531, 18));
    mtl.insert(moveCloud(doc, '#cloud-front-2', 730, 226, 13));

    return mtl;
}



function scarf_in_wind(doc, ground_width, speed) {
    var tmax_tl = new TimelineMax({delay: 0.1675, repeat: -1});

    var globe_continents = [
         doc.querySelector('#scarf1'),
         doc.querySelector('#scarf2')
        ];


    var map_from = { x: -ground_width};
    var map_to = {x: 0, ease: Linear.easeOut };

    tmax_tl.fromTo(globe_continents, speed, map_from, map_to, 0);

  return tmax_tl;
}




function change_light(doc, elem_id, speed, delay, color){
    var light = doc.querySelector(elem_id);
    return TweenMax.to(light, speed, {repeatDelay: delay, fill: color, repeat: -1, ease:Circ.easeInOut, yoyo:true});
}

function driving_vehicle(doc, elem_id){
    var vehicle = doc.querySelector(elem_id);
    return TweenMax.to(vehicle, 0.2, {rotationY:1, rotationZ:0.15, transformOrigin:"right", ease: Quad.easeInOut, repeat: -1, yoyo:true});
}

function blinking(doc, elem_id, speed, delay){
    var element = doc.querySelector(elem_id);

    var tl = new TimelineMax({repeat: -1});

    tl.insert(TweenMax.to(element, 0, {opacity:0, scale:0.9, ease:Circ.easeOut}));
    tl.insert(TweenMax.to(element, speed, {repeatDelay: delay,opacity:1, scale:1, repeat: -1, ease:Circ.easeOut}));

    return tl;
}

function rolling_ball(doc, elem_id, speed, rotation){
    var ball = doc.querySelector(elem_id);
    return TweenMax.to(ball, speed, {rotation: rotation, transformOrigin:"center", ease: Linear.easeNone, repeat: -1})
}

function flying_plane(doc, elem_id){
    var plane = doc.querySelector(elem_id);

    var tl = new TimelineMax({repeat: -1,ease:Linear.easeNone});
    //Rocking motion
    tl.insert(TweenMax.to(plane, 1, {rotationY:5, rotationZ:0.5, transformOrigin:"right", ease: Quad.easeInOut, repeat: -1, yoyo:true}));
    //Up and down motion
    tl.insert(TweenMax.fromTo(plane, 5, {
        y: 0
    }, {
        y: 50,
        repeat: -1,
        yoyo: true,
        ease: Quad.easeInOut
    }));
    return tl;
}

function flying_space_shuttle(doc, elem_id){
    var shuttle = doc.querySelector(elem_id);
    return TweenMax.to(shuttle, 2, {y: 10, transformOrigin:"right", ease: Quad.easeInOut, repeat: -1, yoyo:true})
}


function orbit_motion(doc, elem_id){

    var satellite = doc.querySelector(elem_id);
    var maxx = new TimelineMax({repeat:-1,delay:.2, repeatDelay: 0.7});
    maxx.timeScale(.2);
    maxx.to(satellite, 1, {css:{y:450, scale:.03, rotation: 30}, ease:Linear.easeNone});
    maxx.insert(TweenMax.to(satellite, 0.5, {css:{x:380}, ease:Circ.easeOut, repeat:1, yoyo:true}));

    return maxx;
}


function rock_submarine(doc, elem_id){

    var sub = doc.querySelector(elem_id);
    return TweenMax.fromTo(sub, 1, {
        y: 0,
        rotation: 0
    }, {
        transformOrigin: "0% 100%",
        y: 9,
        rotation: -1,
        repeat: -1,
        delay: 0.30,
        yoyo: true
        //ease: Back.easeIn
    });
}

function rock_fishing_ship(doc, elem_id){

    var sub = doc.querySelector(elem_id);
    return TweenMax.fromTo(sub, 1, {
        y: 0,
        rotation: 0
    }, {
        transformOrigin: "0% 100%",
        y: 20,
        rotation: -1,
        repeat: -1,
        delay: 0.30,
        yoyo: true
        //ease: Back.easeIn
    });
}


function moveCloud(doc, elem_id, ground_width, x, speed) {

    var cloud = doc.querySelector(elem_id);

    var tl = new TimelineMax({
    repeat: -1,
    ease:Linear.easeNone
    });

    var speed_1 = speed * (x / ground_width );
    var speed_2 = speed - speed_1;

    tl.to(cloud, speed_2, {
        x: ground_width-x,
        ease:Linear.easeNone
      })
    .to(cloud, 0, {x: -x, ease:Linear.easeNone})
    .to(cloud, speed_1, {x: 0, ease:Linear.easeNone});

    return tl;
}

function moveGround(doc, ground_width, speed) {
      var tmax_tl = new TimelineMax({
            delay: 0.1675,
            repeat: -1
          });

      var globe_continents = [
           doc.querySelector('#ground'),
           doc.querySelector('#ground_left')
          ];


      var map_from = {
        x: ground_width
      };

      var map_to = {
        x: 0,
        ease: Linear.easeOut
      };

      tmax_tl.fromTo(globe_continents, speed, map_from, map_to, 0);

      return tmax_tl;
}

function rotateFlag(doc, elem_id){
    return TweenMax.to(doc.querySelector(elem_id), 0.8, {rotationY:50, transformOrigin:"left", repeat: -1, yoyo: true, ease:Linear.easeIn});
}

function moveUpAndDown(doc, elem_id){
    return TweenMax.to(doc.querySelector(elem_id), 1.5, {y:10,  yoyo: true, repeat: -1, ease:Linear.easeNone});
}

function createBubble(bubble, delay) {


    //create timeline for each bubble
    var tl = new TimelineMax({
        delay: delay,
        onComplete:complete,
        onCompleteParams:[bubble]
    });

    //how much wiggling / zig zagging
    var wiggle =randomRange(10,20);
    //zig or zag?
    wiggle=Math.random() > .5? -wiggle: wiggle;
    //how fast and big
    var speed=randomRange(2,4);

    var size = speed / 3;
    //fade and grow
    tl.insert(TweenMax.to(bubble, 1, {opacity:randomRange(.5,1), scaleX:size, scaleY:size}));
    //go up
    tl.insert(TweenMax.to(bubble,speed, {y:-400, ease:Quad.easeIn}));
    //zig zag
    tl.insert(TweenMax.to(bubble, speed*.25, {x:String(wiggle), repeat:-1, yoyo:true, ease:Linear.easeNone}));
    //Fadeout
    tl.insert(TweenMax.to(bubble, 1, {opacity:0, scaleX:0, scaleY:0, delay:speed*0.5}));
    //Reset
    tl.insert(TweenMax.to(bubble, 0, {delay: speed, y:0, x:0}));

    function complete(bubble) {
        createBubble(bubble, 0);
    }

}

function randomRange(min, max) {
    return min + (Math.random() * (max - min));
}
