from django.conf.urls import include, url

from django.conf.urls import *
from django.conf.urls.static import static
from django.contrib import admin

from django.conf.urls import url
from django.conf import settings

from django.contrib import admin
from vejdirektoratet.main import views
from vejdirektoratet.main.fb_views import BotView
from vejdirektoratet.main.livechat.consumers import ChatConsumer

admin.autodiscover()

urlpatterns = [

    url(r'^admin/', admin.site.urls),

    #Views
    #url(r'^$', views.home_view, name='overview-view'),
    #url(r'^chat/$', views.chat_view, name='chat-view'),
    #url(r'^chat/(?P<conversation_id>[^/]+)/$', views.conversation, name='conversation'),

    #Chat consumers
    url(r"^chat/$", ChatConsumer),

    #url(r'^' + settings.FB_URL + '/?$', BotView.as_view()),

    url('^', include('django.contrib.auth.urls')),


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)