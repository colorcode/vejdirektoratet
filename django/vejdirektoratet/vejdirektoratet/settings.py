"""
Django settings for vejdirektoratet project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'eik@f@w&ladad3xlo%j(f4e8)l4nxe-^5bs0!3$780)@_tgqs@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ADMINS = (
    ('Bo Kanstrup Hansen', 'bo+vejdirektoratet@mu.st'),
)

ALLOWED_HOSTS = ['vejdirektoratet.colorcode.dk', 'localhost', 'vejdirektoratet2.colorcode.dk', '127.0.0.1']


# Application definition

INSTALLED_APPS = (
    'channels',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'vejdirektoratet.main',

)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'vejdirektoratet', 'templates')],
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders' : [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                ]
        },
    },
]

ROOT_URLCONF = 'vejdirektoratet.urls'

WSGI_APPLICATION = 'vejdirektoratet.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

PAGE_ACCESS_TOKEN = "EAAIEOV2aFJQBAB7Hmw5TdxOpolX3t2JqKMLHr3sXAuGotyszV7ABsx89tPIcxMqtIVFnIFq53BDZChLxd51wKHUpJl7wdeZBmfOqPxiTqeXfXYTFfmyeXRwSwHjeT6XqvMKQ4g2DRwakAdJn2If9a0ZBDKX9pZCZBfm2EynFH1AZDZD"
FB_URL = "4dfb5fb1c1ce130799905bdde902f4a68b42f9c59b0863dfd4"
VERIFY_TOKEN = "15ea1eacdebc342270337e82596a"

###EMAIL SMTP
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'AKIAJ5DYHF6K6EX3KXEQ'
EMAIL_HOST_PASSWORD = 'Al1jZnp7olih9oLD2sAtbCpiustYF3id62vAiUzUa+UQ'
DEFAULT_FROM_EMAIL = 'noreply@faellesskabsprisen.dk'
SERVER_EMAIL = 'noreply@faellesskabsprisen.dk'

#AWS BUCKET
from boto.s3.connection import S3Connection
S3Connection.DefaultHost = 's3-eu-west-1.amazonaws.com'

LOCAL_IMAGE_STORAGE = True
AWS_ACCESS_KEY_ID = "AKIAJP7CLR4FBTYADZEQ"
AWS_SECRET_ACCESS_KEY = "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"
AWS_STORAGE_BUCKET_NAME = "cdn-colorcode-dk"
AWS_S3_FILE_BUFFER_SIZE = 1024 * 1024 * 2
AWS_QUERYSTRING_AUTH = False
AWS_S3_FILE_OVERWRITE = False

#AWS_SUB_PATH = 'uploaded_files'
AWS_LOCATION = '/vejdirektoratet_clip_boards/'


ASGI_APPLICATION = "vejdirektoratet.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
        "CAPACITY": 10000,
    },
}
ASGI_THREADS = 1000

from .localsettings import *