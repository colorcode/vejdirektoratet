#coding: utf-8
import os
import sys

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
HOME_DIR = os.path.expanduser("/home/ubuntu/web_apps/vejdirektoratet")
SECURE_SSL_REDIRECT = False

BASE_URL = 'https://vejdirektoratet.colorcode.dk'

DEBUG = False
COMPRESS_ENABLED = False

MEDIA_ROOT = os.path.join(HOME_DIR, 'media')

STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
#STATIC_ROOT = ''
#STATIC_ROOT = os.path.join(HOME_DIR, 'apps', 'static')
#STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'staticfiles'),)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'vejdirektoratet',
        'USER': 'vejdirektoratet',
        'PASSWORD': 'FssGwUs9SNxWeh6w',
        'HOST': 'localhost',
    }
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True
        },
        'vejdirektoratet_error_logger': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': '/home/ubuntu/web_apps/vejdirektoratet/logs/vejdirektoratet.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        '': {
            'handlers': ['mail_admins', 'vejdirektoratet_error_logger'],
            'level': 'ERROR',
            'propagate': True,
        },
        'management_commands': {
            'handlers': ['mail_admins', 'vejdirektoratet_error_logger'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['mail_admins', 'vejdirektoratet_error_logger'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.request': {
            'handlers': ['mail_admins', 'vejdirektoratet_error_logger'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}

#Caching
CACHES = {
    'HTH_UserCache': {
        'BACKEND': 'redis_cache.cache.RedisCache',
        'LOCATION': '127.0.0.1:6379',
        'KEY_PREFIX': 'vejdirektoratet',
        'OPTIONS': {
            'CLIENT_CLASS': 'redis_cache.client.DefaultClient',
            'MAX_ENTRIES': 10000
        },
        'TIMEOUT': 60,
    },
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

LOCAL_IMAGE_STORAGE = False