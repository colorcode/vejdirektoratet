#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json, requests, random, re
from pprint import pprint
from django.conf import settings

from django.views import generic
from django.http.response import HttpResponse

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from vejdirektoratet.main.fbmessenger import attachments
from vejdirektoratet.main.fbmessenger.attachments import Video
from vejdirektoratet.main.fbmessenger.elements import Element, Button, Text, Address
from vejdirektoratet.main.fbmessenger.templates import GenericTemplate, ButtonTemplate
from vejdirektoratet.main.models import FBUser

default_messages = ["""Jeg hører hvad du siger, men du må forstå, at vi er en forretning.""",
                     """Hmm, det er et spørgsmål om prioriteter.""",
                     """Det afhænger af dit budget.""",
                     """Det gør vi!""",
                     """Ja!""",
                     """Skal vi nu ikke lige se at komme i sommerhumør?""",
                     """Mon ikke et banner kan klare det?""",
                     """Det er klart. Vi er et full service bureau""",
                     """Det kræver vist en opringning""",
                     """Life is about learning how to dance in the rain - Vin Diesel""",
                     """Det har vi gjort mange gange før"""
                    ]

POST_MESSAGE_URL = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s' % settings.PAGE_ACCESS_TOKEN

def send_message(fbid, message,):

    response_msg = json.dumps({"recipient":{"id":fbid}, "message": message})
    status = requests.post(POST_MESSAGE_URL, headers={"Content-Type": "application/json"},data=response_msg)
    pprint(status.json())


def process_facebook_message(fbid, received_message):
    if '?' in received_message:
        message = "BOT: %s" % random.choice(default_messages)
    else:
        message = "BOT: %s" % "Prøv at stil mig et spørgsmål. Ellers vil en medarbejder være med dig lige straks"

    send_message(fbid, Text(text=message).to_dict())


def send_welcome(fbid, ):

    image = attachments.Image(url='http://test.colorcode.dk/img/piglet.jpg')
    send_message(fbid, image.to_dict())

    links_template = ButtonTemplate('Kontakt',[
        Button(button_type='web_url', title="colorcode.dk", url="http://colorcode.dk"),
        Button(button_type='web_url', title="bannerannoncering.dk", url="http://bannerannoncering.dk"),
        #Button("trigger Postback", "DEVELOPED_DEFINED_PAYLOAD"),
        Button(button_type='phone_number', title="Ring til os", payload="+4571996306")
    ])
    send_message(fbid, links_template.to_dict())

    send_options(fbid)

    fb_user = FBUser(fbid=fbid)
    fb_user.save()

def send_options(fbid):
    options_template = ButtonTemplate('Menu',[
        Button(button_type='postback', title="Om os", payload="ABOUT_US_PAYLOAD"),
        Button(button_type='postback', title="Hvad vi tilbyder", payload="WHAT_WE_OFFER_PAYLOAD"),
        Button(button_type='postback', title="Hvor bor I?", payload="WHERE_DO_WE_LIVE"),
    ])
    send_message(fbid, options_template.to_dict())

def send_options_button(fbid):

    template = ButtonTemplate(' - ',[
        Button(button_type='postback', title="Menu", payload="OPTIONS_PAYLOAD")
    ])
    send_message(fbid, template.to_dict())


def send_offers(fbid):

    send_message(fbid, Text(text=u'Her et lille udplug af services, men kig endelig på vores hjemmeside').to_dict())

    template = ButtonTemplate('Kompetencer',[
        Button(button_type='postback', title="Animationsfilm", payload="ABOUT_ANIMATIONS"),
        Button(button_type='postback', title="Websites", payload="ABOUT_WEBSITES"),
        Button(button_type='postback', title="Bannere", payload="ABOUT_BANNERS"),
    ])
    send_message(fbid, template.to_dict())

def send_about_us(fbid):

    send_message(fbid, Text(text=u'Colorcode er et sammentømret hold af erfarne udviklere, med kompetencer der spænder over alle discipliner inden for digital kommunikation og markedsføring. ').to_dict())
    send_message(fbid, Text(text=u'\n\nVi kan styre dine digitale initiativer effektivt og sikkert fra start til slut, eller vi kan være et digitalt supplement til dit eksisterende markedsføringsteam eller udviklingsenhed.').to_dict())

    template = ButtonTemplate(' - ',[
        Button(button_type='postback', title="Persongalleri", payload="ABOUT_PERSONS"),
        Button(button_type='postback', title="Egne projekter", payload="ABOUT_OWN_PROJECTS"),
    ])
    send_message(fbid, template.to_dict())

def send_own_projects(fbid):

    template = ButtonTemplate('Projekter',[
        Button(button_type='postback', title="Animationsfilm", payload="ABOUT_ANIMATIONS"),
        Button(button_type='web_url', title="Børnebog", url="http://minbog.colorcode.dk"),
    ])
    send_message(fbid, template.to_dict())

def send_persons(fbid):

    template = ButtonTemplate('Persongalleri',[
        Button(button_type='postback', title="Jon", payload="ABOUT_JON_PAYLOAD"),
        Button(button_type='postback', title="Bo", payload="ABOUT_BO_PAYLOAD"),
        Button(button_type='postback', title="Dennis", payload="ABOUT_DENNIS_PAYLOAD"),
    ])
    send_message(fbid, template.to_dict())


def send_animations(fbid):
    send_message(fbid, Text(text=u'Vi kommunikerer dit budskab fængende og præcist med video og animation, og sikrer at dit brands værdier eller komplekse koncepter bliver fortalt på en spændende og saglig måde.').to_dict())
    send_message(fbid, Text(text=u'Vi vil meget gerne hjælpe med at gøre din TVC velegnet til YouTubes TrueView-format, sprogversionere den eller klippe den om til program-sponsorater.').to_dict())
    send_message(fbid, Text(text=u'Vi kan også opgradere dit eksisterende materiale, ved at konvertere stillbilleder til cinemagraphs eller designe motion graphics til dit videomateriale.').to_dict())
    video = Video(url='https://s3-eu-west-1.amazonaws.com/test.colorcode.dk/video/sommer-promo.mp4')
    send_message(fbid, video.to_dict())


def send_websites(fbid):
    send_message(fbid, Text(text=u'Vi designer og bygger både simple landing- og kampagnesites, Facebook-applikationer og komplekse websites med CMS-backend, og sikrer at vores websites lever op til gældende browser- og SEO-standarder. Og vi forsyner dem også meget gerne med tracking- og social share-funktionaliteter.').to_dict())
    send_message(fbid, Text(text=u'Vi har et bredt arsenal af ydelser, der gør at vi hurtigt kan udrulle et site med email-udsendelse, ét-klik Facebook-signup, hosting og de nyeste frontend-teknologier.').to_dict())


def send_banners(fbid):
    send_message(fbid, Text(text=u'Vi leverer alle typer displaybannere - fra simple billedbannere, bevægelige html5 bannere, til komplekse dynamiske og intelligente bannersystemer. Herunder bannere, hvor man på ganske få sekunder kan versionere og optimere indhold, eller automatisk tilpasser sig bruger, tid og sted.').to_dict())


def send_jon(fbid):

    image = attachments.Image(url='http://test.colorcode.dk/img/jon.png')
    send_message(fbid, image.to_dict())

    send_message(fbid, Text(text=u'Jon er vores digitale alt-mulig-mand. Han er uddannet cand. it med en bachelor i Dansk og et lille ottetal i Livets Skole. ').to_dict())
    send_message(fbid, Text(text=u'\n\nHan har tidligere arbejdet i et mindre kreativt bureau og for et større mediebureau. Jon har en bred digital profil med kompetencer inden for design og udvikling. Giv ham en animation eller video- produktion, og se ham trylle, eller lad ham få dit brands click-through-rate til at gå i gennem taget.').to_dict())

def send_bo(fbid):

    image = attachments.Image(url='https://s3-eu-west-1.amazonaws.com/test.colorcode.dk/video/bo_grill_bgr.gif')
    #image = attachments.Image(url='http://test.colorcode.dk/img/bo.png')
    send_message(fbid, image.to_dict())

    send_message(fbid, Text(text=u'Bo er uddannet softwareingeniør og har baggrund i et af Danmarks største IT-huse, hvor han arbejdede som seniorkonsulent. ').to_dict())
    send_message(fbid, Text(text=u'\n\nI et afsavn af kreative muligheder, skiftede han til et digitalt reklamebureau, hvor han indtog stillingen som Lead Developer. Han er jeres mand når det gælder det tunge i livet. Backend, kode, servere, API’er og et par af livets spørgsmål.').to_dict())

def send_dennis(fbid):

    image = attachments.Image(url='http://test.colorcode.dk/img/dennis.png')
    send_message(fbid, image.to_dict())

    send_message(fbid, Text(text=u'Dennis er uddannet cand. it. Og har i hele sin karriere beskæftiget sig med digital kommunikation og markedsføring. ').to_dict())
    send_message(fbid, Text(text=u'\n\nHan er en erfaren webdesigner og frontend-udvikler, og har udviklet bannere for mere end 50 brands, samt drevet udviklingen af intelligente bannersystemer. Og så er han bannercertificeret op til begge ører').to_dict())


def send_address(fbid):

    send_message(fbid, Text(text=u'Livjægergade 17B 1. Tv'
                                 u'\n2100 Kbh Ø'
                                 u'\nDanmark').to_dict())


def send_default(fbid):
    send_message(fbid, Text(text=u'Det er ikke forstået').to_dict())

# Create your views here.
def process_payload(fbid, payload):

    choices = {
        'GET_START': send_welcome,

        'ABOUT_US_PAYLOAD': send_about_us,
        'WHAT_WE_OFFER_PAYLOAD': send_offers,
        'WHERE_DO_WE_LIVE': send_address,

        'ABOUT_PERSONS': send_persons,
        'ABOUT_OWN_PROJECTS': send_own_projects,

        'ABOUT_ANIMATIONS': send_animations,
        'ABOUT_WEBSITES': send_websites,
        'ABOUT_BANNERS': send_banners,

        'ABOUT_JON_PAYLOAD': send_jon,
        'ABOUT_BO_PAYLOAD': send_bo,
        'ABOUT_DENNIS_PAYLOAD': send_dennis,

        'OPTIONS_PAYLOAD': send_options,
    }
    result_function = choices.get(payload, send_default)
    result_function(fbid)

    if payload != 'GET_START' and payload != 'OPTIONS_PAYLOAD':
        send_options_button(fbid)


class BotView(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['hub.verify_token'] == settings.VERIFY_TOKEN:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')
        
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle Facebook messages
    def post(self, request, *args, **kwargs):
        # Converts the text payload into a python dictionary
        incoming_message = json.loads(self.request.body.decode('utf-8'))

        # Facebook recommends going through every entry since they might send
        # multiple messages in a single call during high load
        for entry in incoming_message['entry']:
            for message in entry['messaging']:
                # Check to make sure the received call is a message call
                # This might be delivery, optin, postback for other events

                fbid = message['sender']['id']

                if 'message' in message and 'text' in message['message']:
                    # Print the message to the terminal

                    if not FBUser.objects.filter(fbid=fbid).exists():
                        send_welcome(fbid)

                    pprint(message)    
                    # Assuming the sender only sends text. Non-text messages like stickers, audio, pictures
                    # are sent as attachments and must be handled accordingly. 
                    process_facebook_message(fbid, message['message']['text'])
                    send_options_button(fbid)


                if 'postback' in message and 'payload' in message['postback']:

                    process_payload(fbid, message['postback']['payload'])

        return HttpResponse()