from django.db import models

class FBUser(models.Model):
    fbid = models.CharField(max_length=200)


class TrafficQuestion(models.Model):
    user_variable = models.CharField(max_length=100, unique=True)
    question_class = models.CharField(max_length=300)
    question_text = models.CharField(max_length=300)


class TrafficAnswer(models.Model):
    question = models.ForeignKey(TrafficQuestion, verbose_name='Question', on_delete=models.CASCADE)
    answer = models.CharField(max_length=100)
    count = models.IntegerField(default=0)