from django.conf import settings
from django.core.files.storage import FileSystemStorage

from storages.backends import s3boto


class MediaStorage(s3boto.S3BotoStorage):

    def _clean_name(self, name):
        return name

    def _normalize_name(self, name):
        return self.location + "/" + name

def get_image_storage():
    sub_path = 'vejdirektoratet_clip_boards'

    if settings.LOCAL_IMAGE_STORAGE:
        image_storage = FileSystemStorage(location=settings.MEDIA_ROOT+'/'+sub_path, base_url=settings.MEDIA_URL+sub_path)
    else:
        image_storage = MediaStorage(location=sub_path)

    return image_storage
