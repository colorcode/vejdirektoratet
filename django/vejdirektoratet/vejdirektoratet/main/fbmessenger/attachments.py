class BaseAttachment(object):
    def __init__(self, attachment_type, url):
        self.type = attachment_type
        self.url = url

    def to_dict(self):
        return {
            'attachment': {
                'type': self.type,
                'payload': {
                    'url': self.url
                }
            }
        }


class Image(BaseAttachment):
    def __init__(self, url):
        self.attachment_type = 'image'
        self.url = url
        super(Image, self).__init__(self.attachment_type, self.url)


class Audio(BaseAttachment):
    def __init__(self, url):
        self.attachment_type = 'audio'
        self.url = url
        super(Audio, self).__init__(self.attachment_type, self.url)


class Video(BaseAttachment):
    def __init__(self, url):
        self.attachment_type = 'video'
        self.url = url
        super(Video, self).__init__(self.attachment_type, self.url)


class File(BaseAttachment):
    def __init__(self, url):
        self.attachment_type = 'file'
        self.url = url
        super(File, self).__init__(self.attachment_type, self.url)


#https://secure.vimeo.com/moogaloop.swf?clip_id=177677797&autoplay=1
#https://f.vimeocdn.com/p/flash/moogaloop/6.4.3/moogaloop.swf?autoplay=1&clip_id=177677797&amp;controller=Vimeo%5CController%5CPlayer2Controller&amp;view=moogaloop_swf&amp;cdn_url=https%3A%2F%2Ff.vimeocdn.com&amp;player_url=player.vimeo.com&amp;moogaloop_type=moogaloop&amp;old_embed_code=1" type="application/x-shockwave-flash">