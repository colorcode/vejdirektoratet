from django.contrib import admin

# Register your models here.
from vejdirektoratet.main.models import TrafficAnswer, TrafficQuestion

class TrafficAnswerInline(admin.TabularInline):

    model = TrafficAnswer
    extra = 0
    readonly_fields = ('answer', 'count',)
    can_delete = False

class TrafficQuestionAdmin(admin.ModelAdmin):

    model = TrafficQuestion
    list_display = ['id', 'user_variable', 'question_class', 'question_text']
    inlines = [
        TrafficAnswerInline
    ]

admin.site.register(TrafficQuestion, TrafficQuestionAdmin)
admin.site.register(TrafficAnswer)