from django.conf.urls import url
from vejdirektoratet.main.livechat import consumers

websocket_urlpatterns = [
    url(r'^ws/chat/(?P<conversation_id>[^/]+)/$', consumers.ChatConsumer),
    url(r'^ws/chat/(?P<continue_param>continue)/(?P<conversation_id>[^/]+)/$', consumers.ChatConsumer),


    url(r'^chat/(?P<conversation_id>[^/]+)/$', consumers.ChatConsumer),
    url(r'^chat/(?P<continue_param>continue)/(?P<conversation_id>[^/]+)/$', consumers.ChatConsumer),
]