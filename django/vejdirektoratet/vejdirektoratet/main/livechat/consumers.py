# chat/consumers.py
import random
import string
from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
import json
import logging
from vejdirektoratet.main.state_machine.traffic_bot import ChatBot
from django.core.cache import caches
from vejdirektoratet.main.state_machine.user_choice import UserChoices

chat_bot = ChatBot()
cache = caches['HTH_UserCache']

class ChatConsumer(JsonWebsocketConsumer):
    user_choices = {}

    def connect(self):

        def new_connection():
            self.accept()
            #Welcome message
            self.scope['user_choices'] = {}
            UserChoices.set_user_choice("conversation_id", conversation_id, self)
            chat_bot.start_machine(chat_bot.intro, self)

        kwargs = self.scope['url_route']['kwargs']
        conversation_id = kwargs['conversation_id']

        if "continue_param" in kwargs and kwargs['conversation_id'] != "christ":

            cached_session = cache.get(conversation_id)
            if cached_session is None:
                new_connection()
            else:
                #Continue old conversation
                self.scope['user_choices'] = cached_session
                if UserChoices.get_current_state(self) is None:
                    new_connection()
                else:
                    self.accept()
        else:
            new_connection()

    def disconnect(self, close_code):
        # Leave room group
        pass

    # Receive message from WebSocket
    def receive_json(self, content, **kwargs):
        try:
            text_data_json = content
            message = text_data_json['value']

            #Next state
            chat_bot.run_next(message, self, chat_bot, context=text_data_json)
        except Exception as e:
            logging.exception('Error in handling request')
            #Restart conversation
            chat_bot.start_machine(chat_bot.intro, self)