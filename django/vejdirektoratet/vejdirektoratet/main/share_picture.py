#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import random
import string

import textwrap
from PIL import ImageFont, ImageDraw, Image
from django.conf import settings
import os
import cv2
from django.utils import timezone
import numpy as np
from vejdirektoratet.main.storage import get_image_storage

input_dir = os.path.join(settings.STATIC_ROOT, 'clipboard')
output_dir = os.path.join(settings.MEDIA_ROOT, )

def add_product_texts(vis, sign_off):

    def draw_text_in_lines(input_text, font_size, font_path, current_w, start_h, max_w, max_characters, pad, max_h=None):

        def _draw_text_in_lines(font, cur_h, debug):
            para = textwrap.wrap(input_text, width=max_characters)
            end_w = None
            for line in para:
                w, h = draw.textsize(line, font=font)
                if not debug:
                    #draw.text((current_w + (max_w - w) / 2, cur_h), line, font=font, fill="#000000")
                    draw.text((current_w , cur_h), line, font=font, fill="#000000")
                cur_h += h + pad
                end_w = current_w + w
            return cur_h, end_w

        font = ImageFont.truetype(font_path, font_size)
        if max_h:
            while True:
                new_h, new_w = _draw_text_in_lines(font, start_h, True)
                if font_size < 4 or new_h - start_h <= max_h:
                    break
                font_size -= 1
                font = ImageFont.truetype(font_path, font_size)

        return _draw_text_in_lines(font, start_h, False)

    cv2_im_rgb = cv2.cvtColor(vis, cv2.COLOR_BGR2RGB)
    pil_im = Image.fromarray(cv2_im_rgb)

    font_title_path = "%s/%s" % (input_dir, 'JustAnotherHand-Regular.ttf')

    draw = ImageDraw.Draw(pil_im)
    # Choose a font
    font= ImageFont.truetype(font_title_path, 35)
    font_headlines= ImageFont.truetype(font_title_path, 23)
    font_text_size = 22
    font_text_pad = 5
    font_text= ImageFont.truetype(font_title_path, font_text_size)
    x_change = 290
    y_change = -51

    #User name
    input_text = sign_off['user_name']
    draw.text((186+x_change, 237+y_change), input_text, font=font, fill="#000000")

    #Date
    input_text = timezone.now().strftime('%d/%m')
    draw.text((445+x_change, 237+y_change), input_text, font=font, fill="#000000")

    board_width = 391
    max_charters_in_text = 82
    main_x = 116+x_change

    #Diagnose headline:
    input_text = "DIAGNOSE"
    w, h = draw.textsize(input_text, font=font_headlines)

    diagnose_y = 370+y_change
    draw.text((main_x, diagnose_y), input_text, font=font_headlines, fill="#000000")

    #Diagnose line:
    input_text = sign_off['sign_off_diagnosis']
    cur_y, cur_x = draw_text_in_lines(input_text, font_text_size, font_title_path, main_x, diagnose_y+h+10, board_width, max_characters=max_charters_in_text, pad=font_text_pad)

    #Note headline:
    input_text = "NOTER"
    w, h = draw.textsize(input_text, font=font_headlines)
    note_y = cur_y + 10
    draw.text((main_x, note_y), input_text, font=font_headlines, fill="#000000")

    #Note text
    input_text = sign_off['sign_off_notes']
    cur_y, cur_x = draw_text_in_lines(input_text, font_text_size, font_title_path, main_x, note_y+h+10, board_width, max_characters=max_charters_in_text, pad=font_text_pad)

    #Advice headline:
    input_text = "GODE RÅD"
    w, h = draw.textsize(input_text, font=font_headlines)
    advice_y = cur_y + 10
    draw.text((main_x, advice_y), input_text, font=font_headlines, fill="#000000")

    #Note text
    input_text = sign_off['sign_off_advice']
    cur_y, cur_x = draw_text_in_lines(input_text, font_text_size, font_title_path, main_x, advice_y+h+10, board_width, max_characters=max_charters_in_text, pad=font_text_pad)

    # Save the image
    result = cv2.cvtColor(np.array(pil_im), cv2.COLOR_RGB2BGR)
    return result

def create_share_picture(sign_off):

    vis = cv2.imread("%s/%s%s.jpg" % (input_dir, 'clip_board_v2_', sign_off['score']), cv2.IMREAD_UNCHANGED)
    result = add_product_texts(vis, sign_off)

    # cv2.imshow('image',result)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    #Save image
    image_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10)) + ".jpg"
    if settings.DEBUG:
        image_name = "result.jpg"

    temp_file_path = "%s/%s" % (output_dir, image_name)
    cv2.imwrite(temp_file_path, result)

    if settings.DEBUG:
        return "%s/media/%s" % (settings.BASE_URL, image_name)

    with open(temp_file_path, 'rb') as f:
        from django.core.files import File as FileWrapper
        file_wrapper = FileWrapper(f)
        save_url = save_input_file(file_wrapper,image_name)

    os.remove(temp_file_path)

    return save_url



def save_input_file(input_file, file_name):

    fs = get_image_storage()
    filename = fs.save(file_name, input_file)

    if settings.LOCAL_IMAGE_STORAGE:
        uploaded_file_url = settings.BASE_URL + fs.url(filename)
    else:
        uploaded_file_url = fs.url(filename)

    return uploaded_file_url