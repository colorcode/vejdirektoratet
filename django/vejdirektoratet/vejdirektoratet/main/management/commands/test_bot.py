from django.core.management import BaseCommand
from vejdirektoratet.main.share_picture import create_share_picture
from vejdirektoratet.main.state_machine.afmelding import SignOff_message


class Command(BaseCommand):
    help = ""

    def handle(self, *args, **options):
        sign_off = self.fake_sign_off()
        create_share_picture(sign_off)


    def fake_sign_off(self, ):

        sign_off = SignOff_message()

        points = 4
        user_type, score = sign_off.get_user_type_and_score(points)
        label = sign_off.user_type_labels[user_type]
        user_problem = 'Politi'
        if user_problem is None:
            user_problem = "Skraldespand"
        user_name = "Bo Kanstrup"
        notes_extra = sign_off.user_problem_labels[user_problem]["notes_extra"].format(name=user_name)

        response = {
            'user_type': user_type,
            'user_problem': user_problem,
            'user_name': user_name,
            'sign_off_headline': label['headline'],
            'sign_off_diagnosis': label['diagnosis_base'],
            'sign_off_notes': label['notes_base'] + " " + notes_extra,
            'sign_off_advice': label['advice_base'],
            'sign_off_share_picture': "",
            'score': score
        }
        print ('get_sign_off', response)
        return response