from django.core.management import BaseCommand
from vejdirektoratet.main.models import TrafficAnswer
from vejdirektoratet.main.share_picture import create_share_picture
from vejdirektoratet.main.state_machine.afmelding import SignOff_message


class Command(BaseCommand):
    help = ""

    def handle(self, *args, **options):

        for answer in TrafficAnswer.objects.all():
            answer.count = 0
            answer.save()