#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, ChatBot
from vejdirektoratet.main.state_machine.state_machine import StateMachine
from vejdirektoratet.main.state_machine.user_choice import UserChoices

############## 
#Besvær
class Besvaer_message_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis jeg forstår dig rigtigt, så er det, fordi du synes, det er besværligt hele tiden at skifte bane, at du ikke trækker helt ind i højre vejbane."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.besvaer_message_1a
ChatBot.besvaer_message_1 = Besvaer_message_1()

class Besvaer_message_1a(OptionQuestion):
    emotion = "suttleSurprise_anim"

    def get_text(self, session):
        return "Er det korrekt?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.besvaer_answer_1,
            "Nej. Det er ikke så meget det": ChatBot.skraldespand_message_1
        }
ChatBot.besvaer_message_1a = Besvaer_message_1a()

class Besvaer_answer_1(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2
    analytics_event = "Besvaer"

    def get_text(self, session):
        return "Bliver du stresset, når du skal skifte bane ofte?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.besvaer_answer_1_A,
            "Nej": ChatBot.ikkeStresset_message_1
        }

    def run(self, session):
        UserChoices.set_user_choice('mainline-problem', "Besvær", session)
        return super(Besvaer_answer_1, self).run(session)

ChatBot.besvaer_answer_1 = Besvaer_answer_1()

class Besvaer_answer_1_A(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvordan stresset? <br>Gør det dig nervøs for at ende i en ulykke? Eller er det mere noget med, at du er bange for ikke at komme i den rigtige bane ved forgreninger eller afkørsler?"

    def init_transitions(self, ):
        self.transitions = {
            "Jeg er nok først og fremmest bange for at komme i en ulykke": ChatBot.utryghed_message_2, 
            "Det er mest det med at komme i den rigtige vejbane": ChatBot.besvaer_answer_1_A_1,
            "Nej, det er ikke nogen af de ting": ChatBot.besvaer_answer_1_A_2,
        }
ChatBot.besvaer_answer_1_A = Besvaer_answer_1_A()

class Besvaer_answer_1_A_1(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Har du nogensinde prøvet, at du ikke kom i den rigtige vejbane ved en forgrening?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.lukketInde_answer_5, 
            "Nej": ChatBot.besvaer_message_2
        }
ChatBot.besvaer_answer_1_A_1 = Besvaer_answer_1_A_1()

class Besvaer_answer_1_A_2(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Er det så, fordi det kræver mental energi hele tiden at skulle være opmærksom, når man skifter vejbane?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.besvaer_answer_1_A_2_A, 
            "Måske": ChatBot.besvaer_answer_1_A_2_A
        }
        self.defaultTransition = ChatBot.besvaer_answer_1_A_2_A
ChatBot.besvaer_answer_1_A_2 = Besvaer_answer_1_A_2()

class Besvaer_answer_1_A_2_A(Text):
    bad_driver_points = 1
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Uanset hvad, så bliver man i hvert fald meget mindre stresset, desto mere korrekt man kører på motorvejen."

    def init_transitions(self, ):
        self.transitions = ChatBot.endMessage_3_B
ChatBot.besvaer_answer_1_A_2_A = Besvaer_answer_1_A_2_A()

#Besvaer_answer_1_A_2_AA slettet

class Besvaer_message_2(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så er det måske ikke så svært at skifte bane, som du føler det er?"

    def init_transitions(self, ):
        self.transitions = {
            "Nej, det er det nok ikke": ChatBot.besvaer_answer_2_A, 
            "Måske ikke. Men jeg bliver stadig lidt stresset": ChatBot.besvaer_answer_2_B,
            "Jeg føler det som om, det er et problem": ChatBot.besvaer_answer_2_B
        }
ChatBot.besvaer_message_2 = Besvaer_message_2()

class Besvaer_answer_2_A(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så i fremtiden, vil du så forsøge at trække helt ind til højre, når der er plads?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.endMessage_3, 
            "Nej": ChatBot.endMessage_3_disappoint, 
        }
ChatBot.besvaer_answer_2_A = Besvaer_answer_2_A()

class Besvaer_answer_2_B(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det kan jeg godt forstå. Ofte er vores følelser mærkelige på den måde. <br>Vi kan sagtens føle os usikre, også selv om vi ved, at vi ikke har grund til det."

    def init_transitions(self, ):
        self.transitions = {
            "Ok": ChatBot.besvaer_message_3, 
            "Hmmmm": ChatBot.besvaer_message_3, 
        }
ChatBot.besvaer_answer_2_B = Besvaer_answer_2_B()

class Besvaer_message_3(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det kan hjælpe at huske på, at det altid plejer at gå, når man er i situationen. <br>Uanset hvad, så bliver man i hvert fald meget mindre stresset, desto mere korrekt man kører på motorvejen."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.endMessage_2
ChatBot.besvaer_message_3 = Besvaer_message_3()


############## 
# Ikke stresset
class IkkeStresset_message_1(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så du foretrækker egentlig bare at køre ligeud det meste af tiden?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.dovnskab_message_1 
ChatBot.ikkeStresset_message_1 = IkkeStresset_message_1()


############## 
# Dovnskab
class Dovnskab_message_1(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Har du svært ved at finde ro, hvis du er nødt til at skifte spor ofte?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.dovnskab_message_2, 
            "Måske": ChatBot.dovnskab_message_2, 
            "Nej": ChatBot.dovnskab_message_2 
        }
ChatBot.dovnskab_message_1 = Dovnskab_message_1()

class Dovnskab_message_2(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kender du den der rytme, man kan komme i, når man kører på motorvejen. Hvor det hele bare kører, og landskabet farer forbi?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.dovnskab_answer_2_D_1, 
            "Måske": ChatBot.dovnskab_answer_2_D_1, 
            "Nej. Ikke rigtig..": ChatBot.dovnskab_message_2a 
        }
ChatBot.dovnskab_message_2 = Dovnskab_message_2()

class Dovnskab_message_2a(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kan du uddybe dit svar?"

    def init_transitions(self, ):
        self.transitions = ChatBot.dovnskab_answer_2_D
ChatBot.dovnskab_message_2a = Dovnskab_message_2a()

class Dovnskab_answer_2_D(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men du synes stadig, det er for besværligt at skulle zig-zagge mellem sporene?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utryghed_message_4, 
            "Nej, det er faktisk noget helt andet, der generer mig": ChatBot.skraldespand_message_1 
        }
ChatBot.dovnskab_answer_2_D = Dovnskab_answer_2_D()

class Dovnskab_answer_2_D_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men det er ikke i overhalingsbanen, at du skal finde den ro. For der skal du hele tiden være opmærksom på, om der er plads i inderbanen."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.dovnskab_answer_2_D_1a
ChatBot.dovnskab_answer_2_D_1 = Dovnskab_answer_2_D_1()

class Dovnskab_answer_2_D_1a(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "For når der er plads, så har du pligt til at trække ind."

    def init_transitions(self, ):
        self.transitions = {
            "Ok": ChatBot.dovnskab_answer_2_D_1_A, 
            "Hmmm": ChatBot.dovnskab_answer_2_D_1_A
        }
ChatBot.dovnskab_answer_2_D_1a = Dovnskab_answer_2_D_1a()

class Dovnskab_answer_2_D_1_A(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det er i inderbanen, du har mulighed for at tage den lidt mere med ro."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.endMessage_3_B
ChatBot.dovnskab_answer_2_D_1_A = Dovnskab_answer_2_D_1_A()
