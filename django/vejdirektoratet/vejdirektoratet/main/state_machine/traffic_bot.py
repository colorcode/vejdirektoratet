#!/usr/bin/python
# -*- coding: utf-8 -*-

# StateMachine/ChatBot2/ChatBot2Test.py
# A better ChatBot using tables
import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, ChatBot
from vejdirektoratet.main.state_machine.state_machine import StateMachine
from vejdirektoratet.main.state_machine.user_choice import UserChoices
import vejdirektoratet.main.state_machine.besvaer
import vejdirektoratet.main.state_machine.fartglaede
import vejdirektoratet.main.state_machine.lukket_inde
import vejdirektoratet.main.state_machine.skraldespand
import vejdirektoratet.main.state_machine.utryghed
import vejdirektoratet.main.state_machine.politi
import vejdirektoratet.main.state_machine.afmelding

class Intro(Text):
    emotion = "mouth_anim"
    analytics_event = "Intro"

    def get_text(self, session):
        return "Hej. Jeg er en chatbot, der er ekspert i psykoedukation. Jeg hjælper bilister til at blive bedre i trafikken og vurdere din kørsel."

    def init_transitions(self, ):
        self.transitions = ChatBot.intro_2
ChatBot.intro = Intro()

class Intro_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men det kræver, at du vil fortælle mig om dine trafikvaner."

    def init_transitions(self, ):
        self.transitions = ChatBot.message_1
ChatBot.intro_2 = Intro_2()

class Message_1(TextQuestion):
    emotion = "mouth_anim"
    user_variable = "intro-user_name"

    def get_text(self, session):
        return "Hvad hedder du?"

    def next(self, text_input, session, context=None):
        text_modified = text_input.replace('Jeg hedder ', '').replace('jeg hedder ', '').replace('Mit navn er ', '').replace('mit navn er ', '')
        return super(Message_1, self).next(text_modified, session, context)

    def init_transitions(self, ):
        self.transitions = ChatBot.message_2
ChatBot.message_1 = Message_1()

class Message_2(OptionQuestion):
    emotion = "mouth_anim"
    statistic_state = True
    user_variable = "intro-koerer_du_i_bil"

    def get_text(self, session):
        return "Hej, %s<br> Og bare lige for en god ordens skyld: Du kører ikke bil nu, vel?" % UserChoices.get_user_choice('intro-user_name', session)

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.answer_2_A,
            "Nej": ChatBot.answer_2_B
        }
ChatBot.message_2 = Message_2()

class Answer_2_A(Text):
    emotion = "sceptical_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Jeg håber, at du laver sjov med mig."

    def init_transitions(self, ):
        self.transitions = ChatBot.message_3
ChatBot.answer_2_A = Answer_2_A()

class Answer_2_B(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Man ved jo aldrig. Folk er jo vanvittige en gang i mellem."

    def init_transitions(self, ):
        self.transitions = ChatBot.message_3
ChatBot.answer_2_B = Answer_2_B()

class Message_3(OptionQuestion):
    emotion = "mouth_anim"
    statistic_state = True
    user_variable = "TypeOfDriver"

    def get_text(self, session):
        return "Hvordan er du ellers i trafikken? Hvilken type er du?"

    def init_transitions(self, ):
        self.transitions = {
            "Forsigtig": ChatBot.answer_3_A,
            "Lovlydig, men med kant": ChatBot.answer_3_B,
            "Jeg lever nok livet lidt farligt": ChatBot.answer_3_C
        }
ChatBot.message_3 = Message_3()

class Answer_3_A(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det er der mange, der er, %s. Måske er der en gang i mellem nogen, der dytter af dig, men i det lange løb er du det bedste, der er sket for dine medbilister." % UserChoices.get_user_choice('intro-user_name', session)

    def init_transitions(self, ):
        self.transitions = ChatBot.message_4
ChatBot.answer_3_A = Answer_3_A()

class Answer_3_B(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Du er nok som bilister er flest. Men måske vi kan få slebet lidt af kanten <i class='far fa-smile-beam'></i> <br>Når du har travlt, får du så nogle gange for meget fart på?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.answer_3_B_1,
            "Nej": ChatBot.answer_3_B_2
        }
ChatBot.answer_3_B = Answer_3_B()

class Answer_3_C(OptionQuestion):
    emotion = "surprise_anim"
    bad_driver_points = 3

    def get_text(self, session):
        return "Det er jeg ked af at høre, %s. Er du også typen, der altid ligger i overhalingsbanen?" % UserChoices.get_user_choice('intro-user_name', session)

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.fartGlaede_message_1,
            "Nej": ChatBot.message_4
        }
ChatBot.answer_3_C = Answer_3_C()

class Answer_3_B_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du skal have point for at være ærlig. Men det kan aldrig forsvares at køre for stærkt. Og i øvrigt kommer man næsten ikke hurtigere frem. <br>Til gengæld er det farligt. Men det ved du jo godt alt sammen <i class='far fa-smile-beam'></i>"

    def init_transitions(self, ):
        self.transitions = ChatBot.message_4
ChatBot.answer_3_B_1 = Answer_3_B_1()

class Answer_3_B_2(Text):
    bad_driver_points = 1
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Uanset hvorfor der engang imellem går en djævel i dig, så går det altså ikke."

    def init_transitions(self, ):
        self.transitions = ChatBot.message_4
ChatBot.answer_3_B_2 = Answer_3_B_2()

# Bro
class Message_4(OptionQuestion):
    emotion = "mouth_anim"
    statistic_state = True
    user_variable = "ProblemPeopleDontDriveInRight"

    def get_text(self, session):
        return "Hvordan har du det med den højre vejbane? <br>Synes du, det er et problem, at folk ikke trækker ind til højre på motorvejen?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.answer_4_A,
            "Nej": ChatBot.answer_4_B
        }
ChatBot.message_4 = Message_4()

class Answer_4_A(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du er ikke alene. <br>Undersøgelser viser, at det faktisk er 70 pct. af alle bilister, der synes, det er et stort problem."

    def init_transitions(self, ):
        self.transitions = ChatBot.message_5
ChatBot.answer_4_A = Answer_4_A()

class Answer_4_B(Text):
    emotion = "surprise_anim"

    def get_text(self, session):
        return "Du er en af de få. <br>De fleste bilister synes, at det er et stort problem. Undersøgelser har vist, at 70 pct. er trætte af det."

    def init_transitions(self, ):
        self.transitions = ChatBot.message_5
ChatBot.answer_4_B = Answer_4_B()

class Message_5(OptionQuestion):
    emotion = "mouth_anim"
    statistic_state = True
    user_variable = "FavouriteLane"

    def get_text(self, session):
        return "Hvor placerer du dig oftest på motorvejen?"

    def init_transitions(self, ):
        self.transitions = {
            "I inderbanen": ChatBot.answer_5_A,
            "I midterbanen": ChatBot.mainline_message_1,
            "I overhalingsbanen": ChatBot.answer_5_B
        }
ChatBot.message_5 = Message_5()

class Answer_5_A(Text):
    bad_driver_points = -1
    emotion = "surprise_anim"

    def get_text(self, session):
        return "Du lyder jo umiddelbart som en mønsterbilist. <br>Men hvad nu hvis det er en søndag eftermiddag med moderat trafik og god plads på motorvejen?"

    def init_transitions(self, ):
        self.transitions = ChatBot.answer_5_AA
ChatBot.answer_5_A = Answer_5_A()

class Answer_5_AA(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Er du så også altid kun i den højre vejbane, når du selvfølgelig ikke er midt i en overhaling?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.answer_5_A_1,
            "Nej, jeg kan måske godt blive i midtersporet en gang i mellem": ChatBot.mainline_answer_1
        }
ChatBot.answer_5_AA = Answer_5_AA()

class Answer_5_A_1(Text):
    emotion = "surprise_anim"
    bad_driver_points = -1
    analytics_event = "Moensterbilisten"

    def get_text(self, session):
        return "Du er jo alt det, vi drømmer om. <i class='fas fa-heart'></i>"

    def init_transitions(self, ):
        self.transitions = ChatBot.endMessage_3_A
ChatBot.answer_5_A_1 = Answer_5_A_1()

class Answer_5_B(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Er det fordi du overhaler ofte?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.fartGlaede_message_1,
            "Nej": ChatBot.mainline_message_1
        }
ChatBot.answer_5_B = Answer_5_B()




##############
#Mainline
class Mainline_message_1(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Lad os tale lidt om dine andre vaner i trafikken <i class='far fa-smile-beam'></i>. Glemmer du nogen gange at trække ind til højre, selv om du godt ved, at du bør?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.mainline_answer_1,
            "En sjælden gang i mellem": ChatBot.mainline_answer_1,
            "Nej": ChatBot.mainline_answer_1_C
        }
ChatBot.mainline_message_1 = Mainline_message_1()


class Mainline_answer_1_C(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        favourite_lane = UserChoices.get_user_choice('FavouriteLane', session)
        if favourite_lane == "I inderbanen":
            return "Ok, har du lyst til at tale videre om hvorfor, man ikke altid holder til højre?"
        elif favourite_lane == "I midterbanen":
            return "Hmm, men forstod jeg det ikke rigtigt, at du som oftest opholder dig i midterbanen?"
        return "Hmm, men forstod jeg det ikke rigtigt, at du som oftest opholder dig i venstresporet?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja..": ChatBot.mainline_answer_1,
            "Nej": ChatBot.answer_5_A_1
        }
ChatBot.mainline_answer_1_C = Mainline_answer_1_C()


class Mainline_answer_1(TextQuestion):
    emotion = "mouth_anim"
    user_variable = "mainline-problem"
    statistic_state = True
    analytics_event = "TypeAfBilist"

    # Politi keywords & sætninger
    politi_keywords = {
        'politi' ,
        'ulovligt' ,
        'tilladte' ,
        'regler' ,
        'lovligt' ,
        'dygtig' ,
        'perfekt',
        'god',
        'godt'
    }
    politi_sentences = [
        'andre kører' ,
        'kører lovligt' ,
        'har ret',
        'kører det tilladte',
        'kører perfekt' ,
        'min kørsel er' ,
        'jeg bestemmer' ,
        'følger trafikken' 
    ]

    # Besvær keywords & sætninger
    besvaer_keywords = {
        'besværligt',
        'besvær',
        'kedeligt',
        'stresset',
        'stress',
        'irriterende'
    }
    besvaer_sentences = [
        'ofte skifte',
        'skal ikke bestemme',
        'træt af',
        'jeg gider ikke',
        'skifte baner hele tiden'
    ]

    # Fartglæde keywords & sætninger
    fartglaede_keywords = {
        'travlt',
        'travl',
        'tid'
    }
    fartglaede_sentences = [
        'elsker at race',
        'lide at køre hurtigt',
        'har ikke tid',
        'går for langsomt',
        'jeg vil køre stærkt',
        'de er for langsomme',
        'har travl',
        'elsker at køre stærkt',
        'spare tid',
        'jeg sparer tid',
        'har ikke tid'
    ]

    # Lukket inde keywords & sætninger
    lukketInde_keywords = {
        'lukket',
        'inde' ,
        'misse' ,
        'missede' ,
        'afkørsel',
        'mast',
        'presset',
        'bange',
    }
    lukketInde_sentences = [
        'lukket inde',
        'misse afkørsel',
        'kan ikke lide det',
        'jeg bliver mast',
        'kan ikke komme ud',
        'jeg bliver presset',
        'bange for at misse en afkørsel',
        'det er svært'
    ]

    # Utryghed inde keywords & sætninger
    utryghed_keywords = {
        'utryg',
        'utrygt',
        'bange' ,
        'frygt' ,
        'frygter' ,
        'ulykke' ,
        'ulykker' ,
        'usikkerhed',
        'sammenfletning',
        'lastbiler'
    }
    utryghed_sentences = [
        'utrygt på motorvej',
        'bange for ulykker',
        'det er væmmeligt',
        'kan ikke lide at flette',
        'der er andre biler',
        'mange biler',
        'stresset af'
    ]

    def get_text(self, session):
        return "Kan du med et par enkelte ord forklare, hvorfor du ikke altid trækker ind til højre?"

    def next(self, text_input, session, context=None):
        def get_user_type():
            def keyword_in_list(dict, word_list):
                return any(word in dict for word in word_list)

            def sentence_in_string(sentences, text):
                for sentence in sentences:
                    if sentence in text:
                        return True
                return False

            word_list = text_input.split()
            text_to_lower = text_input.lower()
            if keyword_in_list(self.politi_keywords, word_list) or \
                    sentence_in_string(self.politi_sentences, text_to_lower):
                return 'Politi'
            if keyword_in_list(self.besvaer_keywords, word_list) or \
                    sentence_in_string(self.besvaer_sentences, text_to_lower):
                return 'Besvær'
            if keyword_in_list(self.fartglaede_keywords, word_list) or \
                    sentence_in_string(self.fartglaede_sentences, text_to_lower):
                return 'Fartglæde'
            if keyword_in_list(self.lukketInde_keywords, word_list) or \
                    sentence_in_string(self.lukketInde_sentences, text_to_lower):
                return 'LukketInde'
            if keyword_in_list(self.utryghed_keywords, word_list) or \
                    sentence_in_string(self.utryghed_sentences, text_to_lower):
                return 'Utryghed'

            return 'Skraldespand'

        user_type = get_user_type()
        return super(Mainline_answer_1, self).next(user_type, session, context)

    def init_transitions(self, ):
        self.transitions = {
            "Fartglæde": ChatBot.fartGlaede_message_1,
            "Politi": ChatBot.politi_message_1_1,
            "Besvær": ChatBot.besvaer_message_1,
            "LukketInde": ChatBot.lukketInde_message_1,
            "Utryghed": ChatBot.utryghed_message_1,
            "Skraldespand": ChatBot.skraldespand_message_1,
        }


ChatBot.mainline_answer_1 = Mainline_answer_1()




##############
# Slut
class EndMessage(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så nu er vi enige om, at der faktisk ikke er nogen god grund til ikke at trække ind til højre, når der er plads?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.endMessage_2,
        }
        self.defaultTransition = ChatBot.endMessage_2
ChatBot.endMessage = EndMessage()

class EndMessage_2(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Og når du i fremtiden ligger i en overhalingsbane, hvad gør du så, når der er plads til højre?"

    def init_transitions(self, ):
        self.transitions = {
            "Så trækker jeg ind til højre": ChatBot.endMessage_3,
        }
        self.defaultTransition = ChatBot.endMessage_3
ChatBot.endMessage_2 = EndMessage_2()

class EndMessage_3(Text):
    type = "end_message"
    def get_text(self, session):
        return "Det lyder godt. Kan du have en god dag!"

    def init_transitions(self, ):
        self.transitions = ChatBot.signOff_message

ChatBot.endMessage_3 = EndMessage_3()

class EndMessage_3_A(Text):
    type = "end_message"
    def get_text(self, session):
        return "Fortsæt den gode kørsel, og ha’ en dejlig dag!"

    def init_transitions(self, ):
        self.transitions = ChatBot.signOff_message

ChatBot.endMessage_3_A = EndMessage_3_A()

class EndMessage_3_B(Text):
    type = "end_message"
    def get_text(self, session):
        return "Husk, at holde til højre når der er plads til det, og ha’ en fortsat god dag."

    def init_transitions(self, ):
        self.transitions = ChatBot.signOff_message
ChatBot.endMessage_3_B = EndMessage_3_B()

class EndMessage_3_C(Text):
    type = "end_message"
    def get_text(self, session):
        return "Du gør alt det rigtige. Fortsæt med det, og husk altid at holde til højre, når det er muligt. Ha' en dejlig dag."

    def init_transitions(self, ):
        self.transitions = ChatBot.signOff_message

ChatBot.endMessage_3_C = EndMessage_3_C()

class EndMessage_3_D(Text):
    type = "end_message"
    def get_text(self, session):
        return "Ok. Jeg håber, at du vil overveje at holde til højre når der er plads til det, og ha’ en fortsat god dag."

    def init_transitions(self, ):
        self.transitions = ChatBot.signOff_message        

ChatBot.endMessage_3_D = EndMessage_3_D()

class EndMessage_3_disappoint(Text):
    emotion = "mouth_anim"
    bad_driver_points = 3
    type = "end_message"
    analytics_event = "HaaberDuLaverSjov"

    def get_text(self, session):
        return "Jeg håber, du bare laver sjov med mig. Pas på dig selv og dine medtrafikanter. Ha’ en dejlig dag!"

    def init_transitions(self, ):
        self.transitions = ChatBot.signOff_message

ChatBot.endMessage_3_disappoint = EndMessage_3_disappoint()
