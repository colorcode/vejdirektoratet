#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, NumberQuestion, ChatBot
from vejdirektoratet.main.state_machine.state_machine import StateMachine
from vejdirektoratet.main.state_machine.user_choice import UserChoices

############## 
#Lukket inde
class LukketInde_message_1(Text): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du er med andre ord lidt nervøs for at blive lukket inde, hvis du trækker helt ind til højre."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lukketInde_message_2
ChatBot.lukketInde_message_1 = LukketInde_message_1()

class LukketInde_message_2(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Har jeg ret?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.lukketInde_message_3, 
            "Nej. Det er ikke så meget det": ChatBot.skraldespand_message_1
        }
ChatBot.lukketInde_message_2 = LukketInde_message_2()

class LukketInde_message_3(MixQuestion): 
    emotion = "mouth_anim"
    bad_driver_points = 1
    analytics_event = "LukketInde"

    def get_text(self, session):
        return "Hvad er det, der gør, at du synes, det er så ubehageligt at være lukket inde?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jeg er bange for, at jeg ikke kan komme ud igen": ChatBot.lukketInde_message_4, 
            "Jeg bliver utryg, hvis jeg skal flette": ChatBot.lukketInde_answer_3, 
            "Der ligger mange lastbiler i inderbanen, jeg kan ikke lide at være fanget mellem dem": ChatBot.lukketInde_answer_3
        }
        self.defaultTransition = ChatBot.lukketInde_answer_3

    def run(self, session):
        UserChoices.set_user_choice('mainline-problem', "LukketInde", session)
        return super(LukketInde_message_3, self).run(session)

ChatBot.lukketInde_message_3 = LukketInde_message_3()

class LukketInde_answer_3(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Er det i virkeligheden, fordi du føler dig usikker i trafikken, at du ikke vil trække ind til højre?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utryghed_message_2, 
            "Nej. Jeg er egentlig ikke usikker i trafikken. Jeg kan bare ikke lide følelsen af, at jeg ikke kan komme ud": ChatBot.lukketInde_message_4, 
        }
ChatBot.lukketInde_answer_3 = LukketInde_answer_3()

class LukketInde_message_4(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvorfor er det vigtigt at kunne komme ud?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Det kan være, at jeg skal skifte spor ved en forgrening": ChatBot.lukketInde_message_5, 
            "Det kan være, at jeg gerne vil overhale en lastbil eller en anden bil, der kører langsomt": ChatBot.lukketInde_message_5, 
            "Jeg vil bare gerne have muligheden for det": ChatBot.lukketInde_message_5 
        }
        self.defaultTransition = ChatBot.lukketInde_message_5
ChatBot.lukketInde_message_4 = LukketInde_message_4()

class LukketInde_message_5(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Har du nogensinde prøvet, at du ikke kunne komme ud igen, efter du havde trukket ind til højre?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det har jeg da": ChatBot.lukketInde_answer_5,
            "Nej. Ikke rigtig": ChatBot.lukketInde_message_10
        }
ChatBot.lukketInde_message_5 = LukketInde_message_5()

class LukketInde_answer_5(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvad skete der?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jeg måtte vente med at overhale": ChatBot.lukketInde_answer_5_A, 
            "Jeg missede en afkørsel": ChatBot.lukketInde_answer_5_B, 
            "Jeg missede en forgrening": ChatBot.lukketInde_answer_5_B, 
            "Jeg var fanget": ChatBot.lukketInde_answer_5_A
        }
ChatBot.lukketInde_answer_5 = LukketInde_answer_5()

class LukketInde_answer_5_A(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det kan også være ubehageligt at føle sig fanget i trafikken. <br>Men siden du er her i dag, kan jeg forstå, at du overlevede <i class='far fa-smile-beam'></i>"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ha ha ha <i class='far fa-smile-beam'></i>": ChatBot.lukketInde_message_10, 
            "Ok. Jeg kan se, hvad du mener": ChatBot.lukketInde_message_10
        }
        self.defaultTransition = ChatBot.lukketInde_message_10
ChatBot.lukketInde_answer_5_A = LukketInde_answer_5_A()

class LukketInde_answer_5_B(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Måske var det fordi at du glemte at skifte vejbane i tide?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jo, måske": ChatBot.lukketInde_message_6,
            "Nej": ChatBot.lukketInde_answer_5_B_1 
        }
ChatBot.lukketInde_answer_5_B = LukketInde_answer_5_B()

class LukketInde_answer_5_B_1(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Ok. Kan du beskrive situationen for mig, hvor du blev lukket inde, og du endte i den forkerte vejbane?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jeg var uopmærksom": ChatBot.lukketInde_message_8, 
            "Jeg kendte ikke vejen": ChatBot.lukketInde_message_8, 
            "Det var nok min egen skyld": ChatBot.lukketInde_message_8 
        }
        self.defaultTransition = ChatBot.lukketInde_message_6
ChatBot.lukketInde_answer_5_B_1 = LukketInde_answer_5_B_1()

class LukketInde_message_6(Text): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det kan jeg godt se, var virkelig træls. Hvad var konsekvensen?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lukketInde_message_7
ChatBot.lukketInde_message_6 = LukketInde_message_6()

class LukketInde_message_7(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du måtte køre en anden vej. Hvordan gjorde du?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jeg ringede efter hjælp": ChatBot.lukketInde_message_8, 
            "Jeg kiggede på et kort": ChatBot.lukketInde_message_8,
            "Jeg fandt selv vej": ChatBot.lukketInde_message_8 
        }
        self.defaultTransition = ChatBot.lukketInde_message_8
ChatBot.lukketInde_message_7 = LukketInde_message_7()

class LukketInde_message_8(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Ok. Endte du med at komme frem til dit mål?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.lukketInde_message_10,
            "Nej": ChatBot.lukketInde_answer_8 
        }
ChatBot.lukketInde_message_8 = LukketInde_message_8()

class LukketInde_answer_8(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du kom aldrig frem? <br>Var det kun fordi du trak ind til højre og endte i det forkerte spor?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.lukketInde_message_10, 
            "Ok nej. Det var ikke kun derfor": ChatBot.lukketInde_message_10, 
            "Ok. Jeg kom faktisk frem i sidste ende": ChatBot.lukketInde_message_10 
        }
ChatBot.lukketInde_answer_8 = LukketInde_answer_8()

############## 
#2.4.2.1
#LukketInde_message_9 Slettet
#LukketInde_answer_9_A Slettet
#LukketInde_answer_9_A_1 Slettet
#LukketInde_answer_9_A_1a Slettet

############## 
#2.4.3 Ingen konsekvenser
class LukketInde_message_10(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return 'Så selvom det var ubehageligt at være ”lukket inde”, så havde det ikke de store konsekvenser?'
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det må jeg nok indrømme": ChatBot.lukketInde_message_11, 
            "Ok. Ja": ChatBot.lukketInde_message_11 
        }
        self.defaultTransition = ChatBot.lukketInde_message_11
ChatBot.lukketInde_message_10 = LukketInde_message_10()

class LukketInde_message_11(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kan vi blive enige om, at det med at blive lukket inde ikke er et meget stort problem?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.endMessage, 
            "Ok. Ja": ChatBot.endMessage, 
            "Jeg må nok acceptere det": ChatBot.endMessage 
        }
        self.defaultTransition = ChatBot.endMessage 
ChatBot.lukketInde_message_11 = LukketInde_message_11()
