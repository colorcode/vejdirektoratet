#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, NumberQuestion, ChatBot
from vejdirektoratet.main.state_machine.state_machine import StateMachine
from vejdirektoratet.main.state_machine.user_choice import UserChoices

############## 
#Fart glæde
class FartGlaede_message_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis jeg forstår dig rigtigt, så holder du dig i overhalingsbanen, fordi det går for langsomt i højre side."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.fartGlaede_message_2
ChatBot.fartGlaede_message_1 = FartGlaede_message_1()

class FartGlaede_message_2(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Har jeg ret?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.fartGlaede_answer_2,
            "Nej. Det er ikke så meget det": ChatBot.skraldespand_message_1
        }
ChatBot.fartGlaede_message_2 = FartGlaede_message_2()

class FartGlaede_answer_2(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 3
    analytics_event = "Fartglaede"

    def get_text(self, session):
        return "Kører du for stærkt?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.hft_message_1,
            "Nej": ChatBot.fartGlaede_answer_2_B, 
            "Ikke nødvendigvis": ChatBot.fartGlaede_answer_2_B 
        }

    def run(self, session):
        UserChoices.set_user_choice('mainline-problem', "Fartglæde", session)
        return super(FartGlaede_answer_2, self).run(session)

ChatBot.fartGlaede_answer_2 = FartGlaede_answer_2()


class FartGlaede_answer_2_B(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du holder dig altid under fartgrænsen, men du synes alligevel, at det går for langsomt i det inderste spor?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.fartGlaede_answer_2_B_1,
            "Nej. Jeg kører nok lidt for hurtigt": ChatBot.hft_message_1, 
            "Ok. Jeg kører for stærkt fra tid til anden": ChatBot.hft_message_1 
        }
ChatBot.fartGlaede_answer_2_B = FartGlaede_answer_2_B()

class FartGlaede_answer_2_B_1(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = -1

    def get_text(self, session):
        return "Det sker fra tid til anden i indersporet, at de andre biler kører hurtigere end det tilladte. Når du oplever det, følger du da trafikken?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Ok. Så følger jeg trafikken": ChatBot.fartGlaede_answer_2_B_1_A, 
            "Nej. Jeg holder mig altid under fartgrænsen": ChatBot.politi_question_3_2b
        }
ChatBot.fartGlaede_answer_2_B_1 = FartGlaede_answer_2_B_1()

class FartGlaede_answer_2_B_1_A(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men du bliver i overhalingssporet, selv om der er plads i inderbanen, fordi du gerne vil køre stærkt. Holder du dig som regel på den rigtige side af fartgrænsen, eller kommer du nogle gange over?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Altid på den rigtige side af fartgrænsen": ChatBot.politi_question_3, 
            "Over en gang i mellem": ChatBot.hft_message_1,
            "Altid over": ChatBot.hft_message_1,
        }
ChatBot.fartGlaede_answer_2_B_1_A = FartGlaede_answer_2_B_1_A()

#FartGlaede_message_3 Slettet
#FartGlaede_message_4 Slettet

############## 
#Høj fart travl
class HFT_message_1(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Har du altid travlt, når du kører på motorvejen?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Jeg skal nå frem i tide": ChatBot.hft_message_2,
            "Nej, jeg kan bare godt lide at køre stærkt": ChatBot.gratisTid_message_6, 
            "Ja. Altså nogle gange er det bare nødvendigt, at man kommer hurtigt frem": ChatBot.hft_message_2 
        }
ChatBot.hft_message_1 = HFT_message_1()

class HFT_message_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis du skal være helt ærlig, hvor meget tid tror du, det drejer sig om?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.hft_message_3
ChatBot.hft_message_2 = HFT_message_2()

class HFT_message_3(NumberQuestion):
    emotion = "mouth_anim"
    user_variable = "HFT-antal_minutter"

    def get_text(self, session):
        return "Altså: Hvor meget tidligere er du fremme, hvis du kører sådan, som du plejer (hvor du kører stærkt og ikke trækker ind til højre, selv om der er plads) i forhold til, hvis du kørte efter reglerne? <br>(Antal minutter)"

    def next(self, input, session, context=None):
        input = ''.join(c for c in input if c.isdigit())
        return super(HFT_message_3, self).next(input, session, context)

    def init_transitions(self, ):
        self.transitions = ChatBot.hft_message_4
ChatBot.hft_message_3 = HFT_message_3()

class HFT_message_4(NumberQuestion):
    emotion = "mouth_anim"
    user_variable = "HFT-antal_kilometer"

    def get_text(self, session):
        return "Og hvor langt kører du? <br>(Antal kilometer)"

    def next(self, input, session, context=None):
        input = ''.join(c for c in input if c.isdigit())
        return super(HFT_message_4, self).next(input, session, context)
    
    def init_transitions(self, ):
        self.transitions = ChatBot.hft_message_7
ChatBot.hft_message_4 = HFT_message_4()

#HFT_message_5 slettet
#HFT_message_6 slettet

class HFT_message_7(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        antal_minutter = UserChoices.get_user_choice('HFT-antal_minutter', session)
        antal_kilometer = UserChoices.get_user_choice('HFT-antal_kilometer', session)
        sparer = round((int(antal_minutter)/int(antal_kilometer))*60 , 2)
        UserChoices.set_user_choice('HFT-sparer_tid', str(sparer), session)    

        return "Du siger, at du sparer %s minutter på %s kilometer, det svarer til  %s sekunder pr. kilometer"  % (antal_minutter,antal_kilometer,sparer)  
    
    def init_transitions(self, ):
        self.transitions = ChatBot.hft_message_8  
ChatBot.hft_message_7 = HFT_message_7()


class HFT_message_8(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        try:
            sparer = float(UserChoices.get_user_choice('HFT-sparer_tid', session))
        except:
            sparer = 10
            UserChoices.set_user_choice('HFT-sparer_tid', sparer, session)
        print('sparer',sparer)
        if sparer >= 10:
            return "Flyver du i virkeligheden i en rumraket?"
        elif sparer < 10 and sparer >= 7.2:
            return "Du kører med andre ord bil, sådan lidt som man gør i computerspil eller i The Fast and The Furious?"
        elif sparer < 7.2 and sparer >= 3.6:
            return "Du kører med andre ord virkelig meget for hurtigt. Altså. Som i...<br><br>HOLD DA FAST. Du kører hurtigt."
        elif sparer < 3.6:
            return "Så du sparer %s sekunder pr. kilometer, fordi du synes, du skal køre hurtigt og derfor ikke kan trække ind til højre, selv om der er plads?" % (sparer)
    
    def init_transitions_with_session(self, session):
        sparer = float(UserChoices.get_user_choice('HFT-sparer_tid', session))
        if sparer >= 10:
            self.transitions = {
                "Ja": ChatBot.hft_answer_8_A,
                "Nej": ChatBot.hft_answer_8_B
            }
        elif sparer < 10 and sparer >= 7.2:
            self.transitions = {
                "Ja": ChatBot.hft_answer_8_C,
                "Nej. Jeg kører bare stærkt": ChatBot.hft_answer_8_C
            }
        elif sparer < 7.2 and sparer >= 3.6:
            self.transitions = {
                "Ja, det gør jeg nok": ChatBot.hft_answer_8_D,
                "Hmm. Det kan godt være, at jeg kører lidt for stærkt": ChatBot.hft_answer_8_D
            }
        elif sparer < 3.6:
            self.transitions = {
                "Ja": ChatBot.gratisTid_message_1,
                "Heh. Det er lidt fjollet": ChatBot.gratisTid_message_1
            }
ChatBot.hft_message_8 = HFT_message_8()

class HFT_answer_8_A(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Øhm. Det var et retorisk spørgsmål... <br>Min pointe er: Du sparer ikke så meget, som du tror."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_1
ChatBot.hft_answer_8_A = HFT_answer_8_A()

class HFT_answer_8_B(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Ok. Så vi kan nok godt blive enige om, at du faktisk ikke sparer så meget tid, som du tror. <br>Ikke sandt?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_1
ChatBot.hft_answer_8_B = HFT_answer_8_B()

class HFT_answer_8_C(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session): 
        return "Men jeg tror ikke rigtig på, at du kører så stærkt. <br>Jeg tror, du forestiller dig, at du sparer mere tid, end du rent faktisk gør." 
    
    def init_transitions(self, ):
        self.transitions = {
            "Det kan godt være": ChatBot.gratisTid_message_1,
            "Jeg er sikker på, at jeg i hvert fald sparer meget": ChatBot.gratisTid_message_1,
            "Men uanset. Det er nødvendigt for mig at komme hurtigt frem": ChatBot.gratisTid_message_1
        }
ChatBot.hft_answer_8_C = HFT_answer_8_C()

class HFT_answer_8_D(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session): 
        return "Du ligger ofte og kører 170 eller 180, hvor man må køre 130?" 
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det synes jeg, er fint": ChatBot.hft_answer_8_D_1, 
            "Ja": ChatBot.hft_answer_8_D_1, 
            "Ej. Så hurtigt kører jeg faktisk ikke": ChatBot.hft_answer_8_D_2, 
            "Nej. Det er nok umuligt": ChatBot.hft_answer_8_D_2
        }
ChatBot.hft_answer_8_D = HFT_answer_8_D()
    
class HFT_answer_8_D_1(MixQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session): 
        return "Er du klar over, hvor farligt det er? <br>Kører du på Jyllandsringen, eller på en eller anden lukket bane?" 
    
    def init_transitions(self, ):
        self.transitions = {
            "Hvad mener du?": ChatBot.gratisTid_message_1, 
            "Øh. Nej": ChatBot.gratisTid_message_1
        }
        self.defaultTransition = ChatBot.gratisTid_message_1
ChatBot.hft_answer_8_D_1 = HFT_answer_8_D_1()

#HFT_answer_8_D_1_A slettet

class HFT_answer_8_D_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så sparer du nok ikke så meget tid, som du tror."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_1
ChatBot.hft_answer_8_D_2 = HFT_answer_8_D_2()


############## 
#Gratis tid
class GratisTid_message_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men ved du hvad. Uanset hvor meget tid du sparer, så prøv at følg mig her:"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_2 
ChatBot.gratisTid_message_1 = GratisTid_message_1()

class GratisTid_message_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session): 
        antal_minutter = UserChoices.get_user_choice('HFT-antal_minutter', session)
        return "Forestil dig at du er i god tid. At du har de %s minutter, som du sparer ved at køre for stærkt." % (antal_minutter)
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_answer_3
ChatBot.gratisTid_message_2 = GratisTid_message_2()

#GratisTid_message_3 slettet

class GratisTid_answer_3(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du har god tid. Uanset om du kører hurtigt eller langsomt. <br>Hvordan kører du så?"
    
    def init_transitions(self, ):
        self.transitions = {
            "...Jeg kører nok, som jeg plejer": ChatBot.gratisTid_answer_3_A,
            "Jeg kører langsommere og holder ind til højre": ChatBot.gratisTid_answer_3_B,
            "Jeg kører efter reglerne... <br>Nej. Jeg kører nok som jeg plejer": ChatBot.gratisTid_answer_3_A,
            "Ok. Du fik mig. <br>Jeg kan godt se, hvad du mener": ChatBot.gratisTid_answer_3_A 
        }
ChatBot.gratisTid_answer_3 = GratisTid_answer_3()

class GratisTid_answer_3_A(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Hvis du kører, som du plejer, så handler det jo ikke rigtig om at spare tid vel?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej, det må jeg nok indrømme": ChatBot.gratisTid_message_4, 
            "Jo. Det er vigtigt for mig at komme frem i tide": ChatBot.gratisTid_answer_3_A_1 
        }
ChatBot.gratisTid_answer_3_A = GratisTid_answer_3_A()

class GratisTid_answer_3_A_1(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så hele forskellen er gjort af, om du sparer et par få minutter."
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.gratisTid_message_4 
        }
        self.defaultTransition = ChatBot.gratisTid_message_4
ChatBot.gratisTid_answer_3_A_1 = GratisTid_answer_3_A_1()

#GratisTid_answer_3_A_1_A slettet
#GratisTid_answer_3_A_1_AA slettet
#GratisTid_answer_3_A_2 slettet
#GratisTid_answer_3_A_2_A slettet

class GratisTid_answer_3_B(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kan vi måske ikke blive enige om, at det er det rigtige at gøre, også selv om du ikke sparer den tid? <br>Det er trods alt ikke meget tid, vi snakker om."
    
    def init_transitions(self, ):
        self.transitions = {
            "Jo, det kan vi nok godt": ChatBot.endMessage_2, 
            "Hmm": ChatBot.gratisTid_message_6, 
            "Jeg tror helt ærligt ikke, jeg kommer til at køre anderledes": ChatBot.gratisTid_message_6
        }
ChatBot.gratisTid_answer_3_B = GratisTid_answer_3_B()

class GratisTid_message_4(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kunne det ikke tænkes, at der så var en bedre løsning?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_5
ChatBot.gratisTid_message_4 = GratisTid_message_4()

class GratisTid_message_5(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du kunne køre afsted tidligere. Hvis det virkelig er vigtigt at komme på et bestemt tidspunkt, er det så ikke lidt uansvarligt at køre så sent, at du bliver nødt til at fare afsted?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Det kan jeg godt se": ChatBot.endMessage, 
            "Måske": ChatBot.gratisTid_message_6, 
            "Ok": ChatBot.gratisTid_message_6, 
            "Njah": ChatBot.gratisTid_message_6 
        }
        self.defaultTransition = ChatBot.gratisTid_message_6 
ChatBot.gratisTid_message_5 = GratisTid_message_5()

############## 
#2.3.3
class GratisTid_message_6(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Lad os prøve at forestille os en situation."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_7
ChatBot.gratisTid_message_6 = GratisTid_message_6()

class GratisTid_message_7(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du kører på motorvejen. Du sidder fast i en kø i overhalingsbanen. Lidt længere fremme ligger der en bil, som ikke vil trække ind til højre, og som kører langsomt i overhalingsbanen."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_8
ChatBot.gratisTid_message_7 = GratisTid_message_7()

class GratisTid_message_8(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Køen slæber sig afsted i et uudholdeligt tempo."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_9
ChatBot.gratisTid_message_8 = GratisTid_message_8()

class GratisTid_message_9(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Der går virkelig lang tid, før bilen endelig trækker ind. <br>Hvad synes du om bilisten?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_10
ChatBot.gratisTid_message_9 = GratisTid_message_9()

class GratisTid_message_10(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kunne man ikke sige, at han kører hensynsløst?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jo": ChatBot.gratisTid_message_11, 
            "Nej": ChatBot.gratisTid_message_11
        }
ChatBot.gratisTid_message_10 = GratisTid_message_10()

class GratisTid_message_11(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Eller sagt på en anden måde: <br>Problemet med den bilist er, at han kører, som det passer ham?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jo": ChatBot.gratisTid_message_12, 
            "Helt klart": ChatBot.gratisTid_message_12
        }
        self.defaultTransition = ChatBot.gratisTid_message_12
ChatBot.gratisTid_message_11 = GratisTid_message_11()

class GratisTid_message_12(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men kan du se det? <br>Du er ligesom den bilist."
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej. Jeg kan jo godt finde ud af at køre bil": ChatBot.gratisTid_answer_12_A,  
            "Nej. Det synes jeg ikke. Han kører jo langsomt": ChatBot.gratisTid_answer_12_A,
            "Ja, det er vel rigtig nok": ChatBot.gratisTid_message_13
        }
ChatBot.gratisTid_message_12 = GratisTid_message_12()

class GratisTid_answer_12_A(Text):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "I sætter begge jeres eget velbefindende over andres liv. Han vil gerne køre langsomt, også selv om det er farligt for andre. <br>Du vil gerne køre hurtigt, også selv om det er farligt for andre."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_answer_12_A_1
ChatBot.gratisTid_answer_12_A = GratisTid_answer_12_A()

class GratisTid_answer_12_A_1(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvorfor synes du, at det, du gør, er ok, mens det han gør, er forkert?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Det ved jeg ikke": ChatBot.gratisTid_message_13 
        }
        self.defaultTransition = ChatBot.gratisTid_answer_12_B
ChatBot.gratisTid_answer_12_A_1 = GratisTid_answer_12_A_1()

class GratisTid_answer_12_B(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis du skal være helt ærlig, er det så en god grund?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.gratisTid_message_15, 
            "Nej. Det er det nok ikke": ChatBot.gratisTid_message_13,
            "Nej. Ok": ChatBot.gratisTid_message_13
        }
ChatBot.gratisTid_answer_12_B = GratisTid_answer_12_B()

#GratisTid_answer_12_B_1 slettet
#GratisTid_answer_12_B_2 slettet

class GratisTid_message_13(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kan vi blive enige om, at du egentlig burde køre på en anden måde, end du gør?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.gratisTid_message_14 
        }
        self.defaultTransition = ChatBot.gratisTid_message_14
ChatBot.gratisTid_message_13 = GratisTid_message_13()

class GratisTid_message_14(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kan vi også blive enige om, at du burde trække ind til højre, når du har plads?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.endMessage_3,
            "Ok": ChatBot.endMessage_3_D,
            "Nej": ChatBot.gratisTid_message_15
        }
ChatBot.gratisTid_message_14 = GratisTid_message_14()

class GratisTid_message_15(Text):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Det lyder, som om det i virkeligheden handler om, at du godt kan li' at køre stærkt."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_message_16
ChatBot.gratisTid_message_15 = GratisTid_message_15()

class GratisTid_message_16(MixQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Du gider ikke trække ind til højre, fordi du vil have det sjovt og køre hurtigt?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det er nok det": ChatBot.gratisTid_answer_16, 
            "Nej. Ok. Jeg kan godt se, at det ikke holder": ChatBot.endMessage_3_D
        }
        self.defaultTransition = ChatBot.gratisTid_answer_16 
ChatBot.gratisTid_message_16 = GratisTid_message_16()

class GratisTid_answer_16(Text):
    emotion = "mouth_anim"
    bad_driver_points = 2

    def get_text(self, session):
        return "Men kan du så forstå, hvorfor andre på vejen måske ikke har specielt ondt af dig, når der ligger nogen i overhalingsbanen, som du ikke kan komme forbi?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.gratisTid_answer_16_1
ChatBot.gratisTid_answer_16 = GratisTid_answer_16()

class GratisTid_answer_16_1(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Jeg mener. Det er trods alt de andre bilisters liv, du sætter på spil, fordi du gerne vil have det sjovt?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Så slemt er det vel ikke": ChatBot.endMessage_3_disappoint,
            "Ok. Det kan jeg godt se": ChatBot.endMessage
        }
ChatBot.gratisTid_answer_16_1 = GratisTid_answer_16_1()

