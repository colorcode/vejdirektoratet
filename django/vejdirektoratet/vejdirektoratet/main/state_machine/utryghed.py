#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, NumberQuestion, ChatBot
from vejdirektoratet.main.state_machine.state_machine import StateMachine
from vejdirektoratet.main.state_machine.user_choice import UserChoices

############## 
#Utryghed
class Utryghed_message_1(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis jeg forstår dig rigtigt, så føler du dig mindre sikker, når du kører i højre side?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utryghed_message_2,
            "Nej. Det er ikke så meget det": ChatBot.skraldespand_message_1
        }
ChatBot.utryghed_message_1 = Utryghed_message_1()


############## 
#2.2.1
class Utryghed_message_2(OptionQuestion): 
    emotion = "mouth_anim"
    bad_driver_points = 1
    analytics_event = "Utryghed"

    def get_text(self, session):
        return "Hvad er det, der gør, at du føler dig usikker i inderbanen?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Jeg synes, det er utrygt at ligge bag ved lastbiler": ChatBot.lastbilerUtryghed_message_1, 
            "Jeg synes, det er ubehageligt, når der kommer andre biler, som man skal flette sammen med fra tilkørslerne": ChatBot.utrygVedFletning_message_1, 
            "Bilerne ligger af og til meget tæt i inderbanen": ChatBot.utryghed_message_3
        }

    def run(self, session):
        UserChoices.set_user_choice('mainline-problem', "Utryghed", session)
        return super(Utryghed_message_2, self).run(session)

ChatBot.utryghed_message_2 = Utryghed_message_2()

class Utryghed_message_3(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det er selvfølgelig ikke meningen, at du skal trække ind til højre, hvis der ikke er plads."
    
    def init_transitions(self, ):
        self.transitions = {
            "Det er klart": ChatBot.utryghed_message_4, 
            "Det vidste jeg ikke": ChatBot.utryghed_message_4, 
            "Det ved jeg godt": ChatBot.utryghed_message_4,
        }
ChatBot.utryghed_message_3 = Utryghed_message_3()

class Utryghed_message_4(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men er vi ikke også enige om, at du ikke altid husker at trække ind, selv om der er plads."
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja, det er rigtigt nok": ChatBot.utryghed_message_8,
            "Nej. Når der er plads holder jeg altid ind": ChatBot.utryghed_answer_4_A 
        }
ChatBot.utryghed_message_4 = Utryghed_message_4()

class Utryghed_answer_4_A(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis du overhaler, så trækker du ind, selvom du kan se, at der kommer en bil mere lidt længere fremme?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej, det gør jeg nok ikke": ChatBot.utryghed_answer_4_A_1,
            "Ja, det gør jeg faktisk altid": ChatBot.endMessage_3_C  
        }
ChatBot.utryghed_answer_4_A = Utryghed_answer_4_A()

class Utryghed_answer_4_A_1(MixQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Er det, fordi det gør dig utryg at skulle skifte vognbane flere gange efter hinanden?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utryghed_answer_4_A_1_A, 
            "Nej": ChatBot.utryghed_message_8 
        }
        self.defaultTransition = ChatBot.utryghed_message_8 
ChatBot.utryghed_answer_4_A_1 = Utryghed_answer_4_A_1()

class Utryghed_answer_4_A_1_A(MixQuestion): 
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Har du tænkt over, at der ikke er nogen, der tvinger dig til at overhale den næste bil med det samme?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej": ChatBot.utryghed_message_5, 
        }
        self.defaultTransition = ChatBot.utryghed_message_5 
ChatBot.utryghed_answer_4_A_1_A = Utryghed_answer_4_A_1_A()

class Utryghed_message_5(Text): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du kan altid sænke farten og vente med at overhale. Hvis du bliver utryg af at overhale, så skal du lade være."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utryghed_message_7
ChatBot.utryghed_message_5 = Utryghed_message_5()

# Utryghed_message_6 slettet

class Utryghed_message_7(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Er du klar over, at det er ulovligt ikke at trække ind, hvis der er plads til det?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej": ChatBot.utryghed_answer_7_A,
            "Ja": ChatBot.utryghed_message_8 
        }
ChatBot.utryghed_message_7 = Utryghed_message_7()

class Utryghed_answer_7_A(OptionQuestion): 
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Tror du, det gør en forskel nu, hvor du ved, at det er ulovligt?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej": ChatBot.utryghed_message_8, 
            "Ja": ChatBot.utryghed_message_8 
        }
ChatBot.utryghed_answer_7_A = Utryghed_answer_7_A()

############## 
#2.2.2
class Utryghed_message_8(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Har du selv prøvet at ligge i en kø i overhalingsbanen, fordi der er én billist, som ikke kan finde ud af at trække ind til højre?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utryghed_answer_8_A,
            "Nej": ChatBot.utryghed_answer_8_B
        }
ChatBot.utryghed_message_8 = Utryghed_message_8()

class Utryghed_answer_8_A(TextQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Når du ligger der i køen, hvad tænker du så om vedkommende?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utryghed_message_9
ChatBot.utryghed_answer_8_A = Utryghed_answer_8_A()

class Utryghed_answer_8_B(Text): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Prøv at forestille dig at det sker for dig: <br>Du har travlt og der er kø i overhalingsbanen fordi en eller anden ikke trækker ind."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utryghed_answer_8_BB
ChatBot.utryghed_answer_8_B = Utryghed_answer_8_B()

class Utryghed_answer_8_BB(TextQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvad tænker du om føreren af bilen?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utryghed_message_9
ChatBot.utryghed_answer_8_BB = Utryghed_answer_8_BB()

#Utryghed_answer_8_AB_1 slettet
#Utryghed_answer_8_AB_2 slettet

class Utryghed_message_9(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Når du kører i overhalingsbanen uden at trække ind til højre, så tænker alle dem, der kører bag dig på samme måde om dig, som du tænker om den bilist, vi lige talte om."
    
    def init_transitions(self, ):
        self.transitions = {
            "Det kan jeg godt se": ChatBot.endMessage,
            "Nej, for jeg kører præcis det, jeg må": ChatBot.politi_question_5,
            "Nej, for jeg kører så hurtigt, jeg kan": ChatBot.gratisTid_answer_12_B,
            "Det vil jeg tænke over": ChatBot.endMessage
        }
ChatBot.utryghed_message_9 = Utryghed_message_9()

############## 
#2.2.3 Lastbiler og utryghed 
class LastbilerUtryghed_message_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det kan jeg godt forstå. Lastbiler kan være ubehagelige at ligge bag ved."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lastbilerUtryghed_message_2
ChatBot.lastbilerUtryghed_message_1 = LastbilerUtryghed_message_1()

class LastbilerUtryghed_message_2(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Når du nærmer dig en lastbil bagfra, hvad er du så mest utryg ved?"
    
    def init_transitions(self, ):
        self.transitions = {
            "At den begynder at overhale, inden jeg er kommet forbi": ChatBot.lastbilerUtryghed_answer_2_B, 
            "Den spærrer for udsynet": ChatBot.lastbilerUtryghed_answer_2_B,
            "Jeg er utryg, fordi jeg ved, at jeg skal overhale": ChatBot.lastbilerUtryghed_answer_2_C 
        }
ChatBot.lastbilerUtryghed_message_2 = LastbilerUtryghed_message_2()

class LastbilerUtryghed_answer_2_B(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Ja, lastbiler kører heldigvis langsommere end personbiler. Men nu talte vi lige om, at du bliver utryg på motorvejen."
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det er rigtigt": ChatBot.lastbilerUtryghed_answer_2_B_1, 
            "Jeg er ikke utryg. Jeg gider bare ikke ligge bagved en lastbil": ChatBot.lastbilerUtryghed_message_2_B_2, 
            "Hvad mener du?": ChatBot.lastbilerUtryghed_answer_2_B_1
        }
ChatBot.lastbilerUtryghed_answer_2_B = LastbilerUtryghed_answer_2_B()

class LastbilerUtryghed_message_2_B_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du må selvfølgelig gerne overhale lastbiler, så længe du trækker ind til højre, når der er mulighed for det."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.dovnskab_message_1
ChatBot.lastbilerUtryghed_message_2_B_2 = LastbilerUtryghed_message_2_B_2()

class LastbilerUtryghed_answer_2_B_1(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det er virkelig nemt at blive revet med af tempoet på motorvejen. Hvis det gør dig utryg, så kan du måske tænke på lastbilerne som en venlig påmindelse om, at man ikke behøver at køre hurtigt."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lastbilerUtryghed_answer_2_B_1_AB
ChatBot.lastbilerUtryghed_answer_2_B_1 = LastbilerUtryghed_answer_2_B_1()

#LastbilerUtryghed_answer_2_B_1_A slettet

class LastbilerUtryghed_answer_2_B_1_AB(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return 'Hvis du er utryg, så kan det altid anbefales at køre i et roligt tempo i indersporet. <br>Så sørg for at trække ind, så snart du har muligheden for det.'
    
    def init_transitions(self, ):
        self.transitions = {
            "Det giver god mening. Det vil jeg tænke over": ChatBot.endMessage, 
            "Ok. Men det er, fordi jeg bliver utryg, når jeg skal flette": ChatBot.utrygVedFletning_message_1
        }
ChatBot.lastbilerUtryghed_answer_2_B_1_AB = LastbilerUtryghed_answer_2_B_1_AB()

#LastbilerUtryghed_answer_2_B_1_AC slettet

class LastbilerUtryghed_answer_2_C(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du behøver jo ikke overhale?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lastbilerUtryghed_answer_2_CC
ChatBot.lastbilerUtryghed_answer_2_C = LastbilerUtryghed_answer_2_C()

class LastbilerUtryghed_answer_2_CC(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du kan jo blive bagved lastbilen."
    
    def init_transitions(self, ):
        self.transitions = {
            "Men det er utrygt": ChatBot.lastbilerUtryghed_answer_2_CC_1, 
            "Det går for langsomt": ChatBot.lastbilerUtryghed_answer_2_CC_1, 
            "Ok": ChatBot.lastbilerUtryghed_answer_2_CC_2, 
        }
ChatBot.lastbilerUtryghed_answer_2_CC = LastbilerUtryghed_answer_2_CC()

class LastbilerUtryghed_answer_2_CC_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det kan være utrygt at ligge bag en lastbil, det kan jeg godt forstå. <br>Men hvis du holder god afstand til lastbilen, så er det et mindre problem."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lastbilerUtryghed_answer_2_CC_1_A 
ChatBot.lastbilerUtryghed_answer_2_CC_1 = LastbilerUtryghed_answer_2_CC_1()

class LastbilerUtryghed_answer_2_CC_1_A(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Og hvis du holder god afstand, er det også nemmere at komme op i fart inden du overhaler - når der på et tidspunkt er plads til det i yderbanen."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.lastbilerUtryghed_answer_2_CC_1_AA
ChatBot.lastbilerUtryghed_answer_2_CC_1_A = LastbilerUtryghed_answer_2_CC_1_A()

class LastbilerUtryghed_answer_2_CC_1_AA(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "På den måde kan du også hurtigt komme forbi og tilbage i inderbanen."
    
    def init_transitions(self, ):
        self.transitions = {
            "Det giver mening": ChatBot.endMessage,
            "Jeg synes ikke rigtig det løser mit problem": ChatBot.lastbilerUtryghed_message_3
        }
ChatBot.lastbilerUtryghed_answer_2_CC_1_AA = LastbilerUtryghed_answer_2_CC_1_AA()

class LastbilerUtryghed_answer_2_CC_2(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Der er nok ikke rigtig nogen grund til, at du bliver i overhalingsbanen, selv om der er plads til at trække ind."
    
    def init_transitions(self, ):
        self.transitions = {
            "Nej, det er der vel ikke": ChatBot.endMessage,
            "Jo. Der er en anden grund": ChatBot.utryghed_message_8 
        }
ChatBot.lastbilerUtryghed_answer_2_CC_2 = LastbilerUtryghed_answer_2_CC_2()

class LastbilerUtryghed_message_3(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvorfor ikke?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Fordi jeg bliver også utryg, når jeg skal flette. Det er rarere at ligge i en bane, hvor jeg ikke skal flette": ChatBot.utrygVedFletning_message_1,
            "Fordi jeg er ikke er tilfreds med at køre i samme tempo som en lastbil": ChatBot.lastbilerUtryghed_answer_2_B, 
            "Nogle gange glemmer jeg bare at trække ind. Jeg har ikke rigtig nogen grund": ChatBot.endMessage
        }
        self.defaultTransition = ChatBot.endMessage
ChatBot.lastbilerUtryghed_message_3 = LastbilerUtryghed_message_3()

############## 
#UtrygVedFletning
class UtrygVedFletning_message_1(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du synes, det er utrygt at flette ved tilkørselsrampen?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utrygVedFletning_answer_1_A, 
            "Nej": ChatBot.utrygVedFletning_answer_1_B 
        }
ChatBot.utrygVedFletning_message_1 = UtrygVedFletning_message_1()

class UtrygVedFletning_answer_1_A(TextQuestion): 
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Hvorfor?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utrygVedFletning_answer_1_AA
ChatBot.utrygVedFletning_answer_1_A = UtrygVedFletning_answer_1_A()

class UtrygVedFletning_answer_1_AA(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kender du flettereglerne?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.utrygVedFletning_answer_1_AA_1,  
            "Nej": ChatBot.utrygVedFletning_answer_1_AA_2 
        }
ChatBot.utrygVedFletning_answer_1_AA = UtrygVedFletning_answer_1_AA()

class UtrygVedFletning_answer_1_AA_1(Text): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis du kender reglerne, så er det faktisk ikke så vanskeligt. <br>Du skal bare sørge for at orientere dig og så følge reglerne."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utrygVedFletning_answer_1_AA_1_A
ChatBot.utrygVedFletning_answer_1_AA_1 = UtrygVedFletning_answer_1_AA_1()

class UtrygVedFletning_answer_1_AA_1_A(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det handler nok mest af alt om at øve sig. Hvilket du netop aldrig får gjort, hvis du altid ligger i en af overhalingsbanerne! <i class='far fa-smile-wink'></i>"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ok. Ja": ChatBot.endMessage_2, 
            "Hmm": ChatBot.endMessage_2,
            "Nej, det kan jeg godt se": ChatBot.endMessage_2
        }
ChatBot.utrygVedFletning_answer_1_AA_1_A = UtrygVedFletning_answer_1_AA_1_A()

#UtrygVedFletning_answer_1_AA_1_AA slettet

class UtrygVedFletning_answer_1_AA_2(TextQuestion): 
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Det er faktisk meget enkelt. Sagen er den, at når man skal flette, for eksempel ved tilkørsler på en motorvej, så har begge sider ansvaret for at give plads til hinanden."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utrygVedFletning_answer_1_AA_2_A
ChatBot.utrygVedFletning_answer_1_AA_2 = UtrygVedFletning_answer_1_AA_2()

class UtrygVedFletning_answer_1_AA_2_A(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det vil sige, at du skal være opmærksom på, hvordan de andre biler kører. Og så rette dig ind efter det."
    
    def init_transitions(self, ):
        self.transitions = {
            "Ok. Men det hjælper altså ikke på min nervøsitet": ChatBot.utrygVedFletning_message_2, 
            "Men det er jo vildt svært": ChatBot.utrygVedFletning_message_2,
            "Ok. Det er i grunden meget klart": ChatBot.utrygVedFletning_message_2,
            "Ja. Jeg vidste det egentlig godt": ChatBot.utrygVedFletning_message_2
        }
ChatBot.utrygVedFletning_answer_1_AA_2_A = UtrygVedFletning_answer_1_AA_2_A()

class UtrygVedFletning_answer_1_B(TextQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvad er så dit problem?"
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utryghed_message_8
ChatBot.utrygVedFletning_answer_1_B = UtrygVedFletning_answer_1_B()

class UtrygVedFletning_message_2(Text): 
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "For at gøre det enkelt kan man sige, at hvis du skal flette med en bil, der kører meget hurtigere end dig, så skal du give den plads foran dig."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utrygVedFletning_message_3
ChatBot.utrygVedFletning_message_2 = UtrygVedFletning_message_2()

class UtrygVedFletning_message_3(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis du derimod kører hurtigere, så skal du sørge for at den kommer ind bag dig."
    
    def init_transitions(self, ):
        self.transitions = {
            "Det tør jeg slet ikke": ChatBot.utrygVedFletning_message_4,  
            "Jeg er bare ikke så god til det": ChatBot.utrygVedFletning_message_4, 
            "Ok. Ja. Jeg kan også godt": ChatBot.utrygVedFletning_message_8
        }
ChatBot.utrygVedFletning_message_3 = UtrygVedFletning_message_3()

class UtrygVedFletning_message_4(Text): 
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Så gælder det om at lære det. Det kan jeg nok ikke gøre for dig... <br>som du husker, er jeg blot en chatbot <i class='fas fa-robot'></i>. <br>Men det er der andre, der kan..."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.utrygVedFletning_message_5
ChatBot.utrygVedFletning_message_4 = UtrygVedFletning_message_4()

class UtrygVedFletning_message_5(OptionQuestion): 
    emotion = "mouth_anim"
    user_variable = "UtrygVedFletning-koerelaererne"

    def get_text(self, session):
        return "Kørerlærerne. Du kan sagtens kontakte en af dem og spørge om de kan hjælpe dig med enkelt-timer, så du kan få lært det."
    
    def init_transitions(self, ):
         self.transitions = {
            "Det havde jeg ikke tænkt på": ChatBot.utrygVedFletning_message_8, 
            "Kan man det?": ChatBot.utrygVedFletning_message_8, 
            "Ok": ChatBot.utrygVedFletning_message_8 
        }
ChatBot.utrygVedFletning_message_5 = UtrygVedFletning_message_5()

#UtrygVedFletning_message_6 slettet
#UtrygVedFletning_message_7 slettet

class UtrygVedFletning_message_8(OptionQuestion): 
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Prøv det. Og for fremtiden kan vi så blive enige om, at du skal holde dig så meget som muligt i inderbanen?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det kan vi": ChatBot.endMessage_3,
            "Jeg vil prøve": ChatBot.endMessage_3
        }
ChatBot.utrygVedFletning_message_8 = UtrygVedFletning_message_8()
