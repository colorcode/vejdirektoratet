#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.share_picture import create_share_picture
from vejdirektoratet.main.state_machine.state import ChatBot, SignOff

##############
#Sign Off
from vejdirektoratet.main.state_machine.user_choice import UserChoices


class SignOff_message(SignOff):

    user_type_labels = {
        "perfect_driver": {
            "headline": "Mønsterbilisten",
            "headline_fb": "en mønsterbilist",
            "diagnosis_base": "Empatisk og korrekt bilist.",
            "notes_base": "Eksemplarisk. Kører bedre end 96 procent. Kørelærer er en karrieremulighed.",
            "advice_base": "Fortsæt - du har meget at give til dine medtrafikanter. Forsøg at rumme de andres fejl. Du har retten til konstant at kommentere dine venners kørsel.",
        },
        "average_driver": {
            "headline": "Den rutinerede, men ustabile bilist",
            "headline_fb": "den rutinerede, men ustabile bilist",
            "diagnosis_base": "Rutineret bilist, dog lettere ustabil.",
            "notes_base": "Godt kørende, men med lejlighedsvise, alvorlige nedsmeltninger. Læser trafikken smukt, men tilbøjelig til at tabe hovedet i trafikken. Selvopfattelse: er bedre end andre.",
            "advice_base": "Tag en dyb vejrtrækning og nyd turen. Kør godt, hele tiden! Accepter, at du ikke kan kontrollere trafikken – eller tiden.",
        },
        "dangerous_driver": {
            "headline": "Farlig på vejene",
            "headline_fb": "farlig på vejene",
            "diagnosis_base": "Perfekt i en togkupé, ustyrlig på en motorvej,",
            "notes_base": "Aggressiv, narcissistisk, upålidelig, risikovillig. Overvurderer konsekvent egne evner.",
            "advice_base": "Få et rejsekort, tag toget og kom igen i næste uge. Indtil da - undgå at være til fare for andre og dig selv.",
        },
    }

    user_problem_labels = {
        "Besvær": {
          "notes_extra": "{name} må være vildt stresset. Hader, hader, hader at skifte vejbane!",
        },
        "Utryghed":{
          "notes_extra": "Lidt utryg. Har muligvis et lastbilskompleks fra barndommen. Husk nu at der ro i inderbanen, kære ven.",
        },
        "Fartglæde":{
          "notes_extra": "Hemmeligt forelsket i Kevin Magnusson. Kommer {name} mon altid for sent? Husk det er en vane at komme for sent.",
        },
        "LukketInde":{
          "notes_extra": "Drømmer om at have vejen helt for sig selv. {name} hader at vente i kø.",
        },
        "Politi":{
          "notes_extra": "Hvis {name} Betjent havde et bødehæfte ville regnskoven være udryddet i 2020.",
        },
        "Skraldespand":{
          "notes_extra": "Uden for kategori. {name} er muligvis en chatbot også.",
        },
    }

    def get_user_type_and_score(self, points):


        if points is None:
            points = 0
             
        if points > 10:
            score = 6
        elif points > 6:
            score = 5
        elif points > 4:
            score = 4
        elif points > 2:
            score = 3
        elif points > 1:
            score = 2
        else:
            score = 1

        if score > 4:
            return "dangerous_driver", score
        if score > 2:
            return "average_driver", score
        return "perfect_driver", score


    def get_sign_off(self, session):

        points = UserChoices.get_user_choice('bad_driver_points', session)
        user_type, score = self.get_user_type_and_score(points)
        label = self.user_type_labels[user_type]
        user_problem = UserChoices.get_user_choice('mainline-problem', session)
        if user_problem is None:
            user_problem = "Skraldespand"
        user_name = UserChoices.get_user_choice('intro-user_name', session)
        notes_extra = self.user_problem_labels[user_problem]["notes_extra"].format(name=user_name)
        fb_headline = "%s er %s" % (user_name, label['headline_fb'])

        response = {
            'user_type': user_type,
            'user_problem': user_problem,
            'user_name': user_name,
            'fb_headline': fb_headline,
            'fb_cta': label['notes_base'] + " " + notes_extra + " Start din egen samtale og se, hvad du er!",
            'sign_off_headline': label['headline'],
            'sign_off_diagnosis': label['diagnosis_base'],
            'sign_off_notes': label['notes_base'] + " " + notes_extra,
            'sign_off_advice': label['advice_base'],
            'score': score
        }
        # try:
        #     share_picture_url = create_share_picture(response)
        #     response['sign_off_share_picture'] = share_picture_url
        # except:
        #     pass

        return response

    def init_transitions(self, ):
        self.transitions = None

    def next(self, input, session, context=None):
        return None

ChatBot.signOff_message = SignOff_message()