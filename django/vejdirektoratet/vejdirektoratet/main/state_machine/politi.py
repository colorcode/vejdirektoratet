#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, NumberQuestion, ChatBot

##############
#Politi
from vejdirektoratet.main.state_machine.user_choice import UserChoices


class Politi_message_1_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du synes med andre ord, at du har ret til at ligge i overhalingsbanen, fordi du kører det maksimalt tilladte på strækningen."

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_message_1_2
ChatBot.politi_message_1_1 = Politi_message_1_1()

class Politi_message_1_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Så hvis nogen skulle overhale dig, ville de nødvendigvis køre for hurtigt."

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_message_1a
ChatBot.politi_message_1_2 = Politi_message_1_2()

class Politi_message_1a(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Har jeg ret?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.politi_question_2,
            "Nej. Det er ikke så meget det": ChatBot.skraldespand_message_1
        }
ChatBot.politi_message_1a = Politi_message_1a()

######
#2.5.1
class Politi_question_2(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 2
    analytics_event = "Politi"

    def get_text(self, session):
        return "Kører du altid på den måde, eller er det kun i bestemte situationer?"

    def init_transitions(self, ):
        self.transitions = {
            "Altid": ChatBot.politi_question_3,
            "Som regel": ChatBot.politi_question_3,
            "I særlige situationer": ChatBot.politi_question_2_a3 
        }

    def run(self, session):
        UserChoices.set_user_choice('mainline-problem', "Politi", session)
        return super(Politi_question_2, self).run(session)
ChatBot.politi_question_2 = Politi_question_2()

class Politi_question_2_a3(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvilke situationer?"

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_3
ChatBot.politi_question_2_a3 = Politi_question_2_a3()

class Politi_question_3(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Har du prøvet at have en hale af biler efter dig, når du ligger i midter- eller yderbanen?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.politi_question_3_2a,
            "Måske... Jeg er ikke helt sikker": ChatBot.politi_question_3_2a,
            "Nej": ChatBot.politi_question_3_2b
        }
ChatBot.politi_question_3 = Politi_question_3()


class Politi_question_3_2a(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Når du har sådan en lang hale efter dig, hvordan tror du så, de andre bilister tænker om dig?"

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_4
ChatBot.politi_question_3_2a = Politi_question_3_2a()


class Politi_question_3_2b(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men en gang i mellem må du have prøvet at have en bil i bakspejlet, når du ligger i overhalingsbanen og holder dig på den rigtige side af fartgrænsen?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja. Det har jeg nok": ChatBot.politi_question_3_2b_a,
            "Det tror jeg ikke": ChatBot.politi_question_3_2b_b
        }
ChatBot.politi_question_3_2b = Politi_question_3_2b()


class Politi_question_3_2b_a(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvad tror du, bilisten i spejlet tænker om dig?"

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_4
ChatBot.politi_question_3_2b_a = Politi_question_3_2b_a()


class Politi_question_3_2b_b(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvis det skete, hvad tror du, bilisten i spejlet ville tænke?"

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_4
ChatBot.politi_question_3_2b_b = Politi_question_3_2b_b()


class Politi_question_4(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvordan har du det med det?"

    def init_transitions(self, ):

        self.transitions = {
            "Jeg har det fint. Jeg synes ikke, man skal give plads til folk, der vil køre for hurtigt": ChatBot.politi_question_5,
            "Jeg kan godt føle mig lidt presset, men jeg holder fast": ChatBot.politi_question_5,
            "Jeg er nok ret godt tilfreds. Jeg synes, det er det rigtige at gøre": ChatBot.politi_question_5,
            "Jeg er ligeglad": ChatBot.politi_question_5,
        }
        self.defaultTransition = ChatBot.politi_question_5
ChatBot.politi_question_4 = Politi_question_4()


class Politi_question_5(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Du synes, du er i din gode ret?"

    def init_transitions(self, ):

        self.transitions = {
            "Ja. Jeg bliver provokeret af folk, der bare vil brage afsted. Det er virkelig farligt": ChatBot.politi_question_6,
            "Måske": ChatBot.politi_question_6,
            "Jeg er usikker": ChatBot.politi_question_6,
        }
ChatBot.politi_question_5 = Politi_question_5()


class Politi_question_6(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Er du ansat i politiet?"

    def init_transitions(self, ):

        self.transitions = {
            "Ja": ChatBot.politi_question_7_1,
            "Nej": ChatBot.politi_question_6_b,
        }
ChatBot.politi_question_6 = Politi_question_6()

class Politi_question_6_b(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Kan vi så blive enige om, at det ikke er din opgave at regulere trafikken?"

    def init_transitions(self, ):

        self.transitions = {
            "Det kan vi måske godt": ChatBot.politi_question_7_1,
            "Jeg synes, det, jeg gør, er helt ok": ChatBot.politi_question_6_b_bc,
            "Jeg synes ikke, det gør noget, at jeg forhindrer nogen i at køre for stærkt": ChatBot.politi_question_6_b_bc,
        }
ChatBot.politi_question_6_b = Politi_question_6_b()

class Politi_question_6_b_bc(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Er du klar over, at det er ulovligt at lade være med at trække ind til højre, når der er plads?"

    def init_transitions(self, ):

        self.transitions = {
            "Nej. Det vidste jeg ikke": ChatBot.politi_question_7_1,
            "Ja. Det ved jeg godt": ChatBot.politi_question_7_1,
        }
ChatBot.politi_question_6_b_bc = Politi_question_6_b_bc()

#######
##2.5.2
#######


class Politi_question_7_1(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Prøv at forestille dig, at du ligger i midter- eller yderbanen og holder en hastighed på 110. Der er masser af plads til højre, og du har en lang hale af biler efter dig."

    def init_transitions(self, ):
        self.transitions = {
            "Ok": ChatBot.politi_question_7_2,
            "Det kan jeg godt forestille mig": ChatBot.politi_question_7_2,
            "Ja. Sådan er det tit når jeg kører": ChatBot.politi_question_7_2,
        }
ChatBot.politi_question_7_1 = Politi_question_7_1()


class Politi_question_7_2(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Længere tilbage i rækken er der en, som gerne vil køre rigtig hurtigt. Hvordan tror du, han reagerer?"

    def init_transitions(self, ):
        self.transitions = {
            "Det ved jeg ikke": ChatBot.politi_question_7_3,
            "Det er da ikke lige til at sige": ChatBot.politi_question_7_3,
            "Det gør da ingen forskel": ChatBot.politi_question_7_3,
        }
ChatBot.politi_question_7_2 = Politi_question_7_2()


class Politi_question_7_3(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Han kunne måske finde på at overhale indenom."

    def init_transitions(self, ):
        self.transitions = {
            "Men det er ikke mit ansvar, at han ikke kan finde ud af at køre ordentligt": ChatBot.politi_message_7_4_1,
            "Og hvad så. Det har da ikke noget med mig at gøre": ChatBot.politi_message_7_4_1,
            "Det er selvfølgelig ikke så godt": ChatBot.politi_message_7_4_1,
        }
        self.defaultTransition = ChatBot.politi_message_7_4_1
ChatBot.politi_question_7_3 = Politi_question_7_3()

class Politi_message_7_4_1(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Pointen er, at du måske forestiller dig, at du gør vejene mere sikre ved at lege politibetjent.."

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_message_7_4_2
ChatBot.politi_message_7_4_1 = Politi_message_7_4_1()

class Politi_message_7_4_2(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Men i virkeligheden gør du dem farligere. For hvis du har ret i, at dem bag dig bare vil køre for hurtigt, og du giver plads til højre, så er det måske ad den vej de overhaler."

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_7_5
ChatBot.politi_message_7_4_2 = Politi_message_7_4_2()


class Politi_question_7_5(MixQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Forestil dig, at den bil, der ligger lige bag dig godt kan finde ud af at trække ind. Bilen trækker ind i samme øjeblik, som der kommer en bagfra og overhaler inden om. Hvad tror du der sker?"

    def init_transitions(self, ):
        self.transitions = {
            "De støder sammen": ChatBot.politi_question_7_5_a,
            "Det ved jeg da ikke": ChatBot.politi_question_7_5_b,
        }
        self.defaultTransition = ChatBot.politi_question_7_5_b

ChatBot.politi_question_7_5 = Politi_question_7_5()


class Politi_question_7_5_a(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvordan ville du have det, hvis der skete en ulykke og det potentielt var din skyld?"

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_8

ChatBot.politi_question_7_5_a = Politi_question_7_5_a()


class Politi_question_7_5_b(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Uanset hvordan man ser på det, så kan det føre til en farlig situation, som ikke ville være opstået, hvis du havde gjort, som du burde og havde trukket ind til siden."

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_7_5_b_1
ChatBot.politi_question_7_5_b = Politi_question_7_5_b()

class Politi_question_7_5_b_1(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Hvordan ville du have det, hvis en ulykke potentielt var din skyld?"

    def init_transitions(self, ):
        self.transitions = ChatBot.politi_question_8

ChatBot.politi_question_7_5_b_1 = Politi_question_7_5_b_1()


#######
##2.5.3
#######

class Politi_question_8(OptionQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Synes du stadig, det er en god idé at ligge i overhalingsbanen for at bremse trafikken?"

    def init_transitions(self, ):
        self.transitions = {
            "Nej": ChatBot.politi_question_8_a,
            "Jo": ChatBot.politi_question_8_b,
        }
ChatBot.politi_question_8 = Politi_question_8()


class Politi_question_8_a(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Tak. Det er altid bedst at trække helt ind til siden."

    def init_transitions(self, ):
        self.transitions = ChatBot.endMessage_2
ChatBot.politi_question_8_a = Politi_question_8_a()

class Politi_question_8_b(OptionQuestion):
    emotion = "mouth_anim"
    bad_driver_points = 1

    def get_text(self, session):
        return "Så du insisterer på at køre ulovligt?"

    def init_transitions(self, ):
        self.transitions = {
            "Ja!": ChatBot.endMessage_3_disappoint,
            "Nej. Ok. Jeg kan godt se, det du siger": ChatBot.endMessage,
        }
ChatBot.politi_question_8_b = Politi_question_8_b()
