# StateMachine/StateMachine.py
# Takes a list of Inputs to move from State to
# State using a template method.
import logging
from django.core.cache import caches
from vejdirektoratet.main.state_machine.user_choice import UserChoices

cache = caches['HTH_UserCache']

class StateMachine:

    @staticmethod
    def start_machine(initial_state, session):
        UserChoices.set_current_state(initial_state, session)
        StateMachine.run(session)

    @staticmethod
    def run(session, current_state=None):
        if not current_state:
            current_state = UserChoices.get_current_state(session)
        while True:
            next_state = current_state.execute(session)
            if next_state:
                current_state = UserChoices.set_current_state(next_state, session)
            else:
                break

    @staticmethod
    def run_next(input, session, chat_bot, context=None):

        def recover_user_choices():
            conversation_id = UserChoices.get_user_choice("conversation_id", session)

            if conversation_id is None:
                conversation_id = kwargs['conversation_id']

            if conversation_id is None:
                logging.exception('Could not recover conversation_id')
                return None

            cached_session = cache.get(conversation_id)
            if cached_session is None:
                logging.exception('Could not recover cached_session')
                return None

            #Continue old conversation
            session.scope['user_choices'] = cached_session
            UserChoices.set_user_choice("conversation_id", conversation_id, session)

            recovered_current_state = UserChoices.get_current_state(session)
            if recovered_current_state is None:
                logging.exception('Could not recover current_state')
                return None

            logging.exception('Recovered current_state')
            return recovered_current_state

        current_state = UserChoices.get_current_state(session)
        if current_state is None:
            #Error, current state is not set.
            kwargs = session.scope['url_route']['kwargs']
            current_state = recover_user_choices()
            if current_state is None:
                #Recovery failed. Restarting chatbot
                conversation_id = kwargs['conversation_id']
                session.scope['user_choices'] = {}
                UserChoices.set_user_choice("conversation_id", conversation_id, session)
                chat_bot.start_machine(chat_bot.intro, session)
                return

        if current_state.type != "sign_off":
            next_state = current_state.next(input, session, context)
            UserChoices.set_current_state(next_state, session)
            StateMachine.run(session, next_state)