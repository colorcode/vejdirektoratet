#!/usr/bin/python
# -*- coding: utf-8 -*-

import string, sys
from vejdirektoratet.main.state_machine.state import OptionQuestion, Text, State, TextQuestion, MixQuestion, NumberQuestion, ChatBot
from vejdirektoratet.main.state_machine.state_machine import StateMachine
from vejdirektoratet.main.state_machine.user_choice import UserChoices

############## 
#Skraldespand
class Skraldespand_message_1(OptionQuestion):
    emotion = "mouth_anim"
    next_problem_translate = None
    analytics_event = "Skraldespanden"

    def get_text(self, session):
        return "Hvad er det, der gør, at du ikke altid trækker helt ind til højre? Hvis du nu skal være helt ærlig."
    
    def init_transitions(self, ):
        self.transitions = {
            "Det er irriterende hele tiden at skulle skifte spor. Det er nemmere at blive til venstre": ChatBot.besvaer_message_1, 
            "Det er utrygt at ligge i højre side. Det er der, man skal flette, og det er der, lastbilerne kører": ChatBot.utryghed_message_1, 
            "Det går alt for langsomt i højre side": ChatBot.fartGlaede_message_1, 
            "Jeg kan godt blive nervøs for, om jeg får mulighed for at komme ud igen": ChatBot.lukketInde_message_1,
            "Jeg synes ikke, jeg behøver at trække ind til højre, når jeg alligevel kører det maksimale, som er tilladt på strækningen. <br>Der er jo alligevel ikke nogen, som skal overhale": ChatBot.politi_message_1_1
        }

    def next(self, input, session, context=None):
        super_next = super(Skraldespand_message_1, self).next(input, session, context)
        if self.next_problem_translate is None:
            self.next_problem_translate = {
                ChatBot.besvaer_message_1: "Besvær",
                ChatBot.utryghed_message_1: "Utryghed",
                ChatBot.fartGlaede_message_1: "Fartglæde",
                ChatBot.lukketInde_message_1: "LukketInde",
                ChatBot.politi_message_1_1: "Politi",
                ChatBot.mainline_answer_1: "Skraldespand",
            }
        #Save main problem
        try:
            problem = self.next_problem_translate[super_next]
            UserChoices.set_user_choice('mainline-problem', problem, session)
        except:
            pass

        return super_next

ChatBot.skraldespand_message_1 = Skraldespand_message_1()

#################
#ikke inkluderet 
#gå til mainline i stedet for

class Skraldespand_message_3(OptionQuestion): #Hvis man ikke forstå svaret
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Laver du sjov med mig?"
    
    def init_transitions(self, ):
        self.transitions = {
            "Ja": ChatBot.skraldespand_message_4, 
            "Undskyld": ChatBot.skraldespand_message_4, 
            "Nej. Helt ærligt. Jeg mener det alvorligt": ChatBot.skraldespand_message_5
        }
ChatBot.skraldespand_message_3 = Skraldespand_message_3()

class Skraldespand_message_4(Text):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Det er ok. <i class='fas fa-smile-beam'></i> <br>Men svar nu ærligt denne gang."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.Skraldespand_message_1
ChatBot.skraldespand_message_4 = Skraldespand_message_4()

class Skraldespand_message_5(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Ok så forklar mig det én gang til."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.Skraldespand_message_1 #gå til kategori
ChatBot.skraldespand_message_5 = Skraldespand_message_5()

class Skraldespand_message_6(TextQuestion):
    emotion = "mouth_anim"

    def get_text(self, session):
        return "Jeg forstår det stadig ikke helt."
    
    def init_transitions(self, ):
        self.transitions = ChatBot.Skraldespand_message_1 
ChatBot.skraldespand_message_6 = Skraldespand_message_6()

#################