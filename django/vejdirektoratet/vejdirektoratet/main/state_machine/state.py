# StateMachine/State.py
# A State has an operation, and can be moved
# into the next State given an Input:
import logging
import random
from vejdirektoratet.main.models import TrafficQuestion, TrafficAnswer
from vejdirektoratet.main.state_machine.action import UserAction
from vejdirektoratet.main.state_machine.user_choice import UserChoices

from vejdirektoratet.main.state_machine.state_machine import StateMachine

class ChatBot(StateMachine):
    pass


class State():
    has_user_input = False
    type = None
    defaultTransition = None
    statistic_state = False
    analytics_event = None
    question_object = None
    user_variable = None
    bad_driver_points = None


    emotions = {
        "better_anim": True,
        "surprise_anim": True,
        "mouth_anim": True,
        "angry_anim":True,
        "rollingEyes_anim":True,
        "suttleSurprise_anim":True,
        "sceptical_anim":True,
        "rollingEyesTalk_anim": True,
        "angryTalk_anim": True,
        "winkTalk_anim": True,
        "blinkTalk_anim":True,
        "blink_talk_anim":True,
        "subtleSurprise_talk_anim":True,
        "sceptical_talk_anim":True,
        "scepticalTalk_anim":True
    }
    default_emotion = "mouth_anim"

    def __init__(self):
        self.transitions = None

    def execute(self, session):
        self.run(session)
        if not self.has_user_input:
            return self.next(None, session, context=None)

    def run(self, session):
        if not self.transitions:
            self.init_transitions_type(session)
        print (self.get_text(session))
        if self.bad_driver_points:
            UserChoices.increment_bad_driver_points(self.bad_driver_points, session)

        response = self.response_json(session)
        session.send_json(content=response)

    def get_emotion(self):
        if hasattr(self, 'emotion') and self.emotion in self.emotions:
            return self.emotion
        return self.default_emotion

    def response_json(self, session):

        return {
            'text': self.get_text(session),
            'type': self.type,
            "id": type(self).__name__,
            "emotion":{"anim_function": self.get_emotion()},
            "analytics_event": self.analytics_event
        }

    def save_question(self, session):
        if self.user_variable:
            defaults = {"question_text": self.get_text(session)}
            self.question_object, _ = TrafficQuestion.objects.get_or_create(user_variable=self.user_variable,
                                                                        question_class=type(self).__name__,
                                                                        defaults=defaults)
        else:
            logging.error("No suer variable defined for state %s" % type(self).__name__)

    def next(self, input, session, context=None):

        def get_next_state():

            def first_lower(s):
                if s is None or len(s) == 0:
                    return s
                return s[0].lower() + s[1:]

            if issubclass(type(self.transitions), State):
                return self.transitions
            if input in self.transitions:
                return self.transitions[input]
            if self.defaultTransition:
                return self.defaultTransition
            else:

                try:
                    if context is not None and 'id' in context:
                        #Recover state
                        string_state = first_lower(context['id'])
                        fetched_current_state = getattr(ChatBot, string_state)
                        next_fetched = fetched_current_state.next(input, session)
                        if next_fetched is not None:
                            logging.exception("Recovered fetched state: %s, wrong state: %s" % (type(next_fetched).__name__, type(self).__name__))
                            return next_fetched
                except:
                    pass

                logging.exception("Input not supported for current state: %s, for class: %s" % (input, type(self).__name__))
                random_key = random.choice(list(self.transitions.keys()))
                return self.transitions[random_key]

        if not self.transitions:
            self.init_transitions_type(session)
            if self.has_user_input and self.statistic_state:
                #Save question
                self.save_question(session)

        next_state = get_next_state()
        if next_state is None:
            logging.error("next_state is None for input: %s, for class: %s" % (input, type(self).__name__))
            return ChatBot.intro

        if self.has_user_input:
            #Save user choice to session
            if self.user_variable:
                UserChoices.set_user_choice(self.user_variable, input, session)

            #Save user choice to statistics
            if self.statistic_state:

                if self.question_object is None:
                    self.save_question(session)

                #Save answer
                answer, created = TrafficAnswer.objects.get_or_create(question=self.question_object,
                                                                      answer="%s" % input)
                answer.count += 1
                answer.save()

        return next_state



    def init_transitions_type(self, session):
        try:
            return self.init_transitions_with_session(session)
        except AttributeError:
            return self.init_transitions()

    def init_transitions(self, ):
        raise NotImplementedError('Missing init_transitions method for state', self)

    def get_text(self, session):
        raise NotImplementedError('Missing get_text', self)


class OptionQuestion(State):
    type = "choice"
    user_variable = None
    has_user_input = True

    def response_json(self, session):
        options = []
        for choice in self.transitions:
            options.append({"id": "%s"%choice, "title": "%s"%choice})

        response = {
            "type": self.type,
            "text": self.get_text(session),
            "options": options,
            "id": type(self).__name__,
            "emotion":{"anim_function": self.get_emotion()}
        }
        return response


class MixQuestion(OptionQuestion):
    type = "mix_choice"
    defaultTransition = None


class TextQuestion(State):
    type = "text_input"
    user_variable = None
    has_user_input = True


class NumberQuestion(TextQuestion):
    type = "number_input"


class Text(State):
    type = "message"
    has_user_input = False


class SignOff(State):
    type = "sign_off"
    has_user_input = False
    analytics_event = "Afslutning"

    def run(self, session):
        if not self.transitions:
            self.init_transitions_type(session)
        session.send_json(content=self.response_json(session))

    def save_question(self, session):
        defaults = {"question_text": "Final diagnose"}
        self.question_object, _ = TrafficQuestion.objects.get_or_create(user_variable='DiagnoseScore',
                                                                        question_class=type(self).__name__,
                                                                        defaults=defaults)

    def response_json(self, session):

        sign_off_dict = self.get_sign_off(session)
        sign_off_dict['type'] = self.type
        sign_off_dict['id'] = type(self).__name__
        sign_off_dict['analytics_event'] = self.analytics_event

        #Save diagnose:
        if self.question_object is None:
            self.save_question(session)
        answer, created = TrafficAnswer.objects.get_or_create(question=self.question_object,
                                                                      answer="%s" % sign_off_dict['score'])
        answer.count += 1
        answer.save()

        return sign_off_dict
