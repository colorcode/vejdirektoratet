from django.core.cache import caches
cache = caches['HTH_UserCache']


class UserChoices():

    @staticmethod
    def cache_user_session(session):
        conversation_id = session.scope['user_choices']['conversation_id']
        cache.set(conversation_id, session.scope['user_choices'], 60*60)

    @staticmethod
    def set_current_state(state, session):
        session.scope['user_choices']["currentState"] = state
        UserChoices.cache_user_session(session)
        return state

    @staticmethod
    def get_current_state(session):
        try:
            return session.scope['user_choices']["currentState"]
        except:
            return None

    @staticmethod
    def get_user_choice(variable_id, session):
        try:
            return session.scope['user_choices'][variable_id]
        except:
            return None

    @staticmethod
    def set_user_choice(variable_id, choice, session):
        print ('[Saving', variable_id+'='+choice+']')
        session.scope['user_choices'][variable_id] = choice
        UserChoices.cache_user_session(session)

    @staticmethod
    def increment_bad_driver_points(bad_driver_points, session):
        if 'bad_driver_points' in session.scope['user_choices']:
            session.scope['user_choices']['bad_driver_points'] += bad_driver_points
        else:
            session.scope['user_choices']['bad_driver_points'] = bad_driver_points
        UserChoices.cache_user_session(session)