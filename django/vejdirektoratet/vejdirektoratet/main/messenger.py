from fbmessenger import BaseMessenger

class Messenger(BaseMessenger):
    def __init__(self, verify_token, page_access_token):
        self.page_access_token = page_access_token
        super(BaseMessenger, self).__init__(self.page_access_token)

    def message(self, message):
        self.send({'text': 'Received: {0}'.format(message['message']['text'])})

    def delivery(self, message):
        print 'delivery'

    def read(self, message):
        print 'read'

    def account_linking(self, message):
        print 'account_linking'

    def postback(self, messages):
        print 'postback'

    def optin(self, messages):
        print 'optin'