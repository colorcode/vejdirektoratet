import json
from django.shortcuts import render

# Create your views here.
from django.utils.safestring import mark_safe


def home_view(request):

    user = request.user

    ctx = {

    }

    return render(request, "html/views/home.html", ctx)


def chat_view(request):
    return render(request, 'html/views/chat.html', {})

def conversation(request, conversation_name):
    return render(request, 'html/views/room.html', {
        'conversation_name_json': mark_safe(json.dumps(conversation_name))
    })
    