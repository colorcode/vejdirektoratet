Django
psycopg2

django-celery
django-suit
raven

kombu
redis
django-redis-cache==1.7.1

dkimpy

xlrd
gunicorn

django-storages
django-cors-headers

requests

channels_redis

opencv-python
scipy
scikit-learn
matplotlib
Pillow

boto