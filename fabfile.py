import os
from fabric.api import env, run, local, settings, lcd

from coat.django.settings import DjangoSettings, VirtualEnvSettings
from coat.django import commands
from fabric.operations import sudo

env.local_base_path = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
env.django_settings = DjangoSettings()
env.virtualenv_settings = VirtualEnvSettings()
from fabric.context_managers import cd

def env_live():

    env.django_settings = DjangoSettings()
    env.virtualenv_settings = VirtualEnvSettings(
        env_dir="env",
    )
    env.django_settings.update(
        django_appname="vejdirektoratet",
        management_commands=(
            'migrate',
            'collectstatic --noinput'
        ),
        versions_dir="versions",
        wsgi_file="versions/current/django/vejdirektoratet/vejdirektoratet/wsgi.py",
        settings_file="localsettings_live.py",
        supervisor_path="/home/ubuntu/web_apps",
        supervisor_app_name="vejdirektoratet",
    )

    env.hosts = ["colorcode_host"]
    env.user = "ubuntu"
    env.base_dir = "/home/ubuntu/web_apps/vejdirektoratet"

    env.use_ssh_config = True
    env.key_filename = '~/.ssh/aws/aws-git-micro-key.pem'

    env.virtualenv_settings.update(
        commands=[
            ('pip3 -q install '
             '-r django/requirements/base.txt '
             '-r django/requirements/test.txt'),
        ],
    )

def env_must():

    env.django_settings = DjangoSettings()
    env.virtualenv_settings = VirtualEnvSettings(
        env_dir="env",
    )
    env.django_settings.update(
        django_appname="vejdirektoratet",
        management_commands=(
            'migrate',
            'collectstatic --noinput'
        ),
        versions_dir="versions",
        wsgi_file="versions/current/django/vejdirektoratet/vejdirektoratet/wsgi.py",
        settings_file="localsettings_live.py",
        supervisor_path="/home/ubuntu/web_apps",
        supervisor_app_name="vejdirektoratet:",
    )

    env.hosts = ["must_host"]
    env.user = "ubuntu"
    env.base_dir = "/home/ubuntu/web_apps/vejdirektoratet"

    env.use_ssh_config = True
    env.key_filename = '~/.ssh/aws/aws-git-micro-key.pem'

    env.virtualenv_settings.update(
        commands=[
            ('pip3 -q install '
             '-r django/requirements/base.txt '
             '-r django/requirements/test.txt'),
        ],
    )

def local_uname():
    local('uname -a')

def remote_uname():
    run('uname -a')

def deploy(*args, **kwargs):

    print ('args', args, kwargs)

    #run("pkill gunicorn")
    commands.deploy(*args, **kwargs)

    with cd(env.django_settings.supervisor_path):
        run("sudo supervisorctl restart %s" % env.django_settings.supervisor_app_name)