'use strict';
/**
 * Return a function which generates messages.
 */

exports.generateMsg = function(){

  var id = 0;
  return function(json, fn) {
    var key = 'cache::'+ json
      , cached = cache[key];

    // We have a cached version of this size, return that instead.
    if (!cached) {
      cached = cache[key] = json; // Fill a buffer with f's
    }
    var msg = {id: id, value: cached.toString()}; //simple msg with id # and fill
    id++;
    fn(undefined, JSON.stringify(msg));
    return JSON.stringify(msg);
  }
};


exports.generateMsg_newer = function(){

  var id = 0;
  return function(size, fn) {

    var key = 'cache::'+ size
      , cached = cache[key];

    // We have a cached version of this size, return that instead.
    if (!cached) {
      cached = cache[key] = Buffer.alloc(size, 'f'); // Fill a buffer with f's
    }
    var msg = {id: id, value: cached.toString()}; //simple msg with id # and fill
    id++;
    fn(undefined, JSON.stringify(msg));
    return JSON.stringify(msg);
  }
};

exports.generateMsg_old = function utf(size, fn) {
    console.log('generate msg..');
  var key = 'utf8::'+ size
    , cached = cache[key];

  // We have a cached version of this size, return that instead.
  if (cached) return fn(undefined, cached);

  cached = cache[key] = new Buffer(size).toString('utf-8');
  fn(undefined, cached);
};

// cache for messages of same size
var cache = Object.create(null);
