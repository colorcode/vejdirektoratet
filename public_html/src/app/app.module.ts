import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AuthService } from './service/auth.service';
import { AuthGuardService } from './service/auth-guard.service';
import { ChatComponent } from './chat/chat.component';
import { ResultComponent } from './result/result.component';
import { AnimationComponent } from './chat/animation/animation.component';
import { HomeAnimationComponent } from './home/home-animation/home-animation.component';
import { CookieService } from './service/cookie.service';
import { DataService } from './service/data.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ChatComponent,
    ResultComponent,
    AnimationComponent,
    HomeAnimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [AuthService , AuthGuardService , CookieService , DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
