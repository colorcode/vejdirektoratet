import {trigger, animate, style, group, animateChild, query, stagger, transition} from '@angular/animations';

export const routerTransitionSwipe = trigger('swipeAnimation', [
    transition('* <=> *', [
      // Initial state of new route
      query(':enter',
        style({
          position: 'absolute',
          width:'100%',
          height:"100%",
          transform: 'translateX(110%)'
        }),
        {optional:true}),

      // move page off screen right on leave
      query(':leave',
        animate('500ms ease',
          style({
            position: 'absolute',
            width:'100%',
            height:"100%",
            transform: 'translateX(-110%)'
          })
        ),
      {optional:true}),

      // move page in screen from left to right
      query(':enter',
        animate('500ms ease',
          style({
            opacity: 1,
            transform: 'translateX(0%)'
          })
        ),
      {optional:true})
  ])
])

export const routerTransitionZoom = trigger('zoomAnimation', [
  transition('* <=> *', [
    // Initial state of new route
    query(':enter',
      style({
        position: 'absolute',
        width:'100%',
        height:"100%",
        transform: 'translateX(-100%)'
      }),
      {optional:true}),

    // move page off screen right on leave
    query(':leave',
      animate('500ms ease',
        style({
          position: 'absolute',
          width:'100%',
          height:"100%",
          transform: 'translateX(100%)'
        })
      ),
    {optional:true}),

    // move page in screen from left to right
    query(':enter',
      animate('500ms ease',
        style({
          opacity: 1,
          transform: 'translateX(0%)'
        })
      ),
    {optional:true})
])
])
