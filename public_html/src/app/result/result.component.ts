import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  constructor(private authService:AuthService) { }

  startChat(){
    this.authService.startChat();

    // GA event
    (<any>window).ga('send', 'event', {
      eventCategory: 'Navigate',
      eventLabel: 'CTA Button',
      eventAction: 'Restart test',
      eventValue: 10
    });
  }

  addDynamicJS(myScript: string): HTMLElement {
    const script = document.createElement('script');
    script.src = myScript;
    script.async = true;
    script.defer = true;
    return document.body.appendChild(script);
  }

  ngOnInit() {
    this.authService.chatAllow = false;
  }

}
