import { Component, OnInit } from '@angular/core';
import { CookieService } from './service/cookie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  title = 'app';

  constructor(private cookieService:CookieService , private router:Router){ 
    this.router.events.subscribe(event => {
      if(router.url == "/"){
        // GA event
        (<any>window).ga('set', 'page', router.url);
        (<any>window).ga('send', 'pageview');
      }
      if(router.url == "/chat"){
        // GA event
        (<any>window).ga('set', 'page', router.url);
        (<any>window).ga('send', 'pageview');
      }
      if(router.url == "/result"){
        // GA event
        (<any>window).ga('set', 'page', router.url);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  setCookie(){
    //console.log("Set cookie");
    this.cookieService.setCookie(this.cookieService.cookieName , this.cookieService.makeid() , 1000);
    document.getElementById("cookie-alert").style.display = "none";
    (<any>window).ga('set', 'userId', this.cookieService.makeid());
  }

  ngOnInit(){
    if(this.cookieService.getCookie(this.cookieService.cookieName)){
      //console.log("cookie found");
      let userId = this.cookieService.getCookie(this.cookieService.cookieName);
      (<any>window).ga('set', 'userId', userId);
      document.getElementById("cookie-alert").style.display = "none";
    }else{
      //console.log("no cookie");
      document.getElementById("cookie-alert").style.display = "block";
    }
  }
}
