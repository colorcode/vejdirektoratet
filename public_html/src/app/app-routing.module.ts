import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { ResultComponent } from './result/result.component';
import { AuthGuardService } from './service/auth-guard.service';

const routes: Routes = [
  {path:"" , component:HomeComponent , data: { state: 'home' }},
  {path:"chat" , component:ChatComponent , canActivate:[AuthGuardService] , data: { state: 'chat' }},
  {path:"result" , component:ResultComponent , canActivate:[AuthGuardService] , data: { state: 'result' }},
  {path:"**" , component:HomeComponent , data: { state: 'home' }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes , {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
