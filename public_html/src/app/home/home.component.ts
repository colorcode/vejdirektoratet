import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private authService:AuthService , private dataService:DataService) { }

  startChat(){
    this.authService.startChat();

    // GA event
    (<any>window).ga('send', 'event', {
      eventCategory: 'Navigate',
      eventLabel: 'CTA Button',
      eventAction: 'Go to chatbot',
      eventValue: 10
    });
  }

  ngOnInit() {
    eval('home_anim("#psyk_chair")');
    if(this.dataService.scriptLoaded){
      eval('removeMain("./js/main.js")');
    }
  }

}
