import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { CookieService } from '../service/cookie.service';
import { AuthService } from '../service/auth.service';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  generate:boolean = false;
  productionLink:string;
  buildLink:string;
  constructor(private cookieService:CookieService , private authService:AuthService , private dataService:DataService){ }

  addDynamicJS(myScript: string): HTMLElement {
    const script = document.createElement('script');
    script.src = myScript;
    script.async = true;
    script.defer = true;
    return document.body.appendChild(script);
  }

  ngOnInit() {
    this.authService.showResult();

    if(!this.generate){
      this.cookieService.generateUserId();
      this.generate = true;
    }
    this.productionLink = "wss://vejdirektoratet2.colorcode.dk/ws/chat/"+this.cookieService.userId+"/";
    this.buildLink = "ws://localhost:8000/ws/chat/"+this.cookieService.userId+"/";

    setTimeout(function () {
        eval( 'lockScreensize(true)' );
        //console.log("screen lock function activated");
    }, 300);
    this.addDynamicJS('./js/main.js').onload = () => {
      this.dataService.scriptLoaded = true;
      if(environment.production){
        //console.log("Production mode");
        eval( 'startWS("'+this.productionLink+'")' );
        this.dataService.serverLink = this.productionLink;
        return;
      }
      //console.log("Build mode");
      eval( 'startWS("'+this.buildLink+'")' );
      this.dataService.serverLink = this.buildLink;
      //eval( 'startWS("'+this.productionLink+'")' );
    }
  }
}
