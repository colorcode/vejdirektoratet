import { Injectable } from "../../../node_modules/@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "../../../node_modules/@angular/router";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuardService implements CanActivate{
    constructor(private router: Router , private authService:AuthService){ }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;
        return this.loginGuard(url);
    }

    loginGuard(url:string){
        if(url == "/chat"){
            if(this.authService.chatAllow){
                return true;
            }
            this.router.navigate(['/']);
            return false;
        }else if(url == "/result"){
            if(this.authService.resultAllow){
                return true;
            }
            this.router.navigate(['/']);
            return false;
        }
    }
}