import { Injectable } from "../../../node_modules/@angular/core";

@Injectable()
export class AuthService{
    chatAllow:boolean = false;
    resultAllow:boolean = false;
    
    constructor(){ }

    startChat(){
        return this.chatAllow = true;
    }

    showResult(){
        return this.resultAllow = true;
    }

}