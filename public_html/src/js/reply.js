// Bot start typing animation
function botTypingStart(){
    // Bot typing animation
    typeMsg.classList.add("typing");

    // Bot stop typing
    if(queue.length == number && animationRunning == false){
        setTimeout( function(){
            botTypingStop();
        }, 1000)
    }
}

// Bot Stop typing animation
function botTypingStop(){
    var typeMsg = document.getElementById("type-message");

    if(typeMsg.classList.contains("typing")){
        typeMsg.classList.remove("typing");
    };
    if(messageQueue.length > 0){
        botReplyMessage(messageQueue[messageNumber]);
    }
}

// Bot reply messages
function botReplyMessage(data){
    // Bot reply
    if (data === null || data === undefined){
      //console.log('botReplyMessage Error data is null');
      return;
    }

    if(data.type === "sign_off"){
      setTimeout(function(){
          chatSocket.close();
          window.location.hash = "/result";
          setTimeout( function(){
            endTest(data);
          }, 50);
      },4000)
      return;
    }
    
    setTimeout( function(){
        // Display message to user
        displayMessage("bot" , data.text);

        // Display input type
        showInputType(data);

        // SVG Animation - Chat page
        animationQueue(data.emotion);

        // Display one message at a time
        messageNumber++;
        // Insert waiting animation in queue
        waitingAnimation = true;
        if(messageNumber == messageQueue.length){
            messageQueue = [];
            messageNumber = 0;
            allow = true;
        }else{
            botTypingStart();
        }
    }, 300);
};
