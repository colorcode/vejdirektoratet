var inputMsg = document.getElementById("inputMsg");
var chatBox = document.getElementById("chat-box");
var btnSendMsg = document.getElementById("btn-submit");

var typeMsg = document.getElementById("type-message");

var chatSocket;
var activeTime = 0;
var closeTime = 0;
var keepConnection = false;
var userReset = false;

var allow = true;
var messageNumber = 0;
var messageQueue = [];

    document.addEventListener("click" , function(e){
        // Press Send btn to submit
        var inputMsg = document.getElementById("inputMsg");
        if(e.target.classList == "submit-message"){
            if(inputMsg.value.length != 0 && allowSend == true){
                var inputMessage = inputMsg.value;
                if(selectionBoxVisible == false){
                    getUserAnswer(inputMessage);
                }else{
                    getUserAnswer(inputMessage);
                    hideSelectionBox();
                }
            }
        }

        // Selection box, click to answer
        if(e.target.classList == "select"){
            var checkMarkIcon = "<svg id='m-check-mark' width='49px' height='34.6px' viewBox='0 0 245 173' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><g id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-opacity='1'><polyline id='checkmark' stroke='#000000' stroke-width='30' points='5.640625 83.7607422 83.2539062 161.663086 238.97168 6.11328125'></polyline></g></svg>"
            var selectedId = e.target.getAttribute("data-boxId");
            var selectedValue = e.target.getAttribute("value");
            if(boxSelected == false){
                // find and get value from answer Id
                document.getElementById(selectedId).insertAdjacentHTML("beforeend" , checkMarkIcon);
                boxSelected = true;
                var positionOfTitle = questionData.options.findIndex(function(item){
                    return item.id === selectedValue
                });

                var selectedTitle = questionData.options[positionOfTitle].title;
                setTimeout(function(){
                    getUserAnswer(selectedTitle);
                    hideSelectionBox();
                }, 800);
            }
        }

        // restart chat
        if(e.target.classList == "reset-message"){
            if(questionData.id != 'EndMessage_3' && userReset == false){
                showRestartChat()
            }
        }

        // Inactive message
        if(e.target.classList.contains("ws-connect")){
            var stayConneted = e.target.getAttribute("ws-connetion");
            var wsLink = e.target.getAttribute("ws-path");
            var wsConnection = eval(stayConneted);
            if(!wsConnection){
                resetConversation();
            }else{
                wsLink = setContinueInLink(wsLink);
            }
            check(wsLink , wsConnection);
            hideinactive();
        }
    })

    // Press Enter to submit
    document.addEventListener("keyup" , function(e){
        var inputMsg = document.getElementById("inputMsg");
        if(e.keyCode === 13){
            e.preventDefault();
            if(inputMsg.value.length != 0 && allowSend == true){
                var inputMessage = inputMsg.value;
                if(selectionBoxVisible == false){
                    getUserAnswer(inputMessage);
                }else{
                    getUserAnswer(inputMessage);
                    hideSelectionBox();
                }
            }
        }
    })

   
//***************************************
// BOT
// Bot reply
function startWS(webSocketPath){
    chatSocket = new WebSocket(webSocketPath);

    chatSocket.onclose = function(e) {
        //console.error('Chat socket closed unexpectedly');

        closeTime = e.timeStamp;
        if(!userReset){
            if(window.location.hash == "#/chat"){
                setTimeout(function(){
                    inactive(webSocketPath);
                },500);
            }
        }else{
            check(webSocketPath , keepConnection);
            userReset = false;
        }
    };

    chatSocket.onmessage = function(e) {
        var data = JSON.parse(e.data);
        activeTime = e.timeStamp;
        
        // Get stored in conversation log
        questionData = data;
        
        // Reset wait animation on Intro message
        if(data.id == "Intro"){
            waitingQueue = 0;
            number = 0;
            queue = [];
        }
        
        messageQueue.push(data);
        if(messageNumber <= 1 && allow == true){
            allow = false;
            botTypingStart();
        }

        // Track conversation in GA
        trackConversation(data)
    };
}

function check(webSocketPath , keepConnection){
    if(!keepConnection){
        var checkContinueLink = webSocketPath.indexOf("continue/");
        if(checkContinueLink >= 0){
            webSocketPath = removeContinueInLink(webSocketPath);
        }
        var newWebSocketPath = webSocketPath.slice(0 , -26)+wsCreateId()+"/";
        startWS(newWebSocketPath);
    }else{
        startWS(webSocketPath);
    }
}