var countSendMsg = 0;
var allowSend = true;

//***************************************
// USER
// save input messages
var inputName = "John Doe";
var conversationLog = [];
function sendUserInput(data){
    var conversation = {};
    conversation.question = questionData;
    conversation.answer = data;
    conversationLog.push(conversation);
    if(data.id === "Message_1"){
        inputName = data.value;
    }
    displayMessage("user" , data.value);

    // User input send to websocket is in lowercase
    data.value = data.value;
    chatSocket.send(JSON.stringify(data));

    // Avoid spamming
    countSendMsg++;
    if(countSendMsg > 0){
        allowSend = false;
        setTimeout( function(){
            allowSend = true;
            countSendMsg = 0;
        }, 2000)
    }

    // Reset keyup i MixQuestion
    var inputBox = document.getElementById('inputMsg');
    inputBox.onkeyup = null;

    // Close keyboard mobile
    $("#inputMsg").blur();

    // Stop waiting animation in queue
    waitingAnimation = false;

    // Reset waiting animation
    waitingQueue = 0;

    return;
}
