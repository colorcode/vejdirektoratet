function displayResult(data){
    createResult(data);
}

function createResult(board_info){
    var fullDate = new Date().getDate() + "/" +  (new Date().getMonth() + 1 ) + "-" + (new Date().getYear() -100);
    document.getElementById("result_date").innerHTML = fullDate;
    document.getElementById("result_name").innerHTML = board_info.user_name;
    document.getElementById("result_diagnose").innerHTML = board_info.sign_off_diagnosis;
    document.getElementById("result_notes").innerHTML = board_info.sign_off_notes;
    document.getElementById("result_advice").innerHTML = board_info.sign_off_advice;
    document.getElementById("result_score").className += " score-"+board_info.score;

    showResult();

    $( '.fb-share-image' ).click(function(e){
        e.preventDefault();
        ga('send', 'event', 'Share result', 'CTA button', 'Share on facebook');

        var base_url = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');

        FB.ui({
          method: 'share_open_graph',
          action_type: 'og.shares',
          action_properties: JSON.stringify({
            object : {
              'og:url': base_url, 
              'og:title': board_info.fb_headline,
              'og:site_name':'Hold Til Højre - Det Er Det Modsatte Af Venstre!',
              'og:description': board_info.fb_cta,
              'og:image': "https://cdn-colorcode-dk.s3.amazonaws.com/vejdirektoratet_clip_boards/share-result-no-board-1200.jpg", //
              'og:image:width':1200,
              'og:image:height':630,
              'og:updated_time': new Date().toISOString()
            }
          })
        }, function(response){
          //console.log("response is ",response);
        });
    });
}

function showResult(){
  document.getElementById("graphic-overlay").style.opacity = '1';
  document.getElementById("result_date").style.opacity = '1';
  document.getElementById("result_name").style.opacity = '1';
  document.getElementById("result_score").style.opacity = '1';
  document.getElementById("result_diagnose").style.opacity = '1';
  document.getElementById("result_notes").style.opacity = '1';
  document.getElementById("result_advice").style.opacity = '1';
}
