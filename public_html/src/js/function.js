var questionData;
var selectionBoxVisible = false;
var boxSelected = false;
var board_info;
var showInactiveMsg = false;
var allowScreensizeLock = false;

// Create of selection box
// options att. is an array contains multiple choice
function createSelectionBox(data){
    if(selectionBoxVisible == false){
        for(var i = 0; i < data.options.length; i++){
            var selectionDiv = document.createElement("div");
            var selectionBox = document.createElement("div");
            var selectionText = document.createElement("p");
            selectionDiv.className = "selection";
            selectionBox.className = "checkBox";
            selectionBox.id = "select-"+i;
            selectionText.setAttribute("data-boxId" , "select-"+i);
            selectionText.setAttribute("value" , data.options[i].id);
            selectionText.className = "select";
            selectionText.innerHTML = data.options[i].title;
            selectionDiv.appendChild(selectionBox);
            selectionDiv.appendChild(selectionText);
            document.getElementById("select-box").appendChild(selectionDiv);
            setTimeout( function(){
                document.getElementById("select-box").classList.add("visible");
            }, 100);
        }
        selectionBoxVisible = true;
        gotoBottom("msg-box");
        hideInputField();
    }
}

function createMixQuestion(data){
    if(selectionBoxVisible == false){
        for(var i = 0; i < data.options.length; i++){
            var selectionDiv = document.createElement("div");
            var selectionBox = document.createElement("div");
            var selectionText = document.createElement("p");

            selectionDiv.className = "selection";
            selectionBox.className = "checkBox";
            selectionBox.id = "select-"+i;
            selectionText.className = "select";
            selectionText.innerHTML = data.options[i].title;
            selectionText.setAttribute("data-boxId" , "select-"+i);
            selectionText.setAttribute("value" , data.options[i].id);

            selectionDiv.appendChild(selectionBox);
            selectionDiv.appendChild(selectionText);
            document.getElementById("select-box").appendChild(selectionDiv);
            setTimeout( function(){
                document.getElementById("select-box").classList.add("visible");
            }, 100);
        }

        // Fixed message
        var selectionFixDiv = document.createElement("div");
        var selectionFixText = document.createElement("p");
        var text_description = "Skriv dit svar i inputfeltet";

        selectionFixDiv.className = "selection";
        selectionFixText.className = "mixSelect";
        selectionFixText.innerHTML = text_description;

        selectionFixDiv.appendChild(selectionFixText);
        document.getElementById("select-box").appendChild(selectionFixDiv);
        document.getElementById("select-box").classList.add("visible");

        // See input text live
        selectionFixText.id = "printchatbox";
        selectionFixText.innerHTML = text_description;
        selectionBoxVisible = true;
        gotoBottom("msg-box");
        showInputField();


        var inputBox = document.getElementById('inputMsg');
        inputBox.onkeyup = function(){
            document.getElementById('printchatbox').innerHTML = inputBox.value || "Andet";
        }
    }
}

// Hide selection box
function hideSelectionBox(){
    document.getElementById("select-box").innerHTML = "";
    document.getElementById("select-box").classList.remove("visible");
    selectionBoxVisible = false;
    boxSelected = false;
}

// Show Input field
function showInputField(inputType){
    var inputMsg = document.getElementById("inputMsg");
    var chatBox = document.getElementById("chat-box");
    var btnSendMsg = document.getElementById("btn-submit");
    if(inputType === "number_input"){
        inputMsg.setAttribute("type" , "number");
    }else{
        inputMsg.setAttribute("type" , "text");
    }
    inputMsg.removeAttribute("disabled");
    inputMsg.removeAttribute("readonly");
    chatBox.classList.remove("hide");
    btnSendMsg.classList.remove("hide");
}

// Hide Input field
function hideInputField(){
    var inputMsg = document.getElementById("inputMsg");
    var chatBox = document.getElementById("chat-box");
    var btnSendMsg = document.getElementById("btn-submit");
    inputMsg.setAttribute("disabled" , "true");
    inputMsg.setAttribute('readonly', 'readonly');
    chatBox.classList.add("hide");
    btnSendMsg.classList.add("hide");
}

// Collect user data
function getUserAnswer(answer){
    answerData = {};
    answerData.type = questionData.type;
    answerData.value = answer;
    answerData.id = questionData.id;
    sendUserInput(answerData);
}

// Display End Test Message
function endTest(sign_off_data){

    sign_off_data_example = {
      id:"SignOff_message",
      sign_off_advice:"Fortsæt - du har meget at give til dine medtrafikanter. Forsøg at rumme de andres fejl. Du har retten til konstant at kommentere dine venners kørsel.",
      sign_off_diagnosis:"Empatisk og korrekt bilist.",
      sign_off_headline:"Mønsterbilisten",
      sign_off_notes:"Eksemplarisk. Kører bedre end 96 procent. Kørelærer er en karrieremulighed. Hvis Bo Betjent havde et bødehæfte ville regnskoven være udryddet i 2020.",
      sign_off_share_picture:"",
      type:"sign_off",
      user_name:"Bo",
      user_problem:"Politi",
      user_type:"perfect_driver",
      score: 3
    };

    board_info = sign_off_data;
    checkImgLoaded(board_info)
    
}

function checkImgLoaded(board_info){
    $('<img/>').attr('src', '/assets/img/result_board.png').on('load', function() {
        $(this).remove(); // prevent memory leaks as @benweet suggested
        displayResult(board_info);
    });
}

function showInputType(data){
    // selecting answer options
    if(data.type === "text_input" || data.type === "number_input"){
        showInputField(data.type);
        hideSelectionBox();
    }else if(data.type === "choice"){
        createSelectionBox(data);
    }else if(data.type === "mix_choice"){
        createMixQuestion(data);
    }else{
        hideInputField();
        hideSelectionBox();
    }
}

function resetConversation(){
    document.getElementById("display-message").innerHTML = "";
    showInputType("")
    messageNumber = 0;
    messageQueue = [];
    questionData;
    allow = true;
    botTypingStop();
}

function setContinueInLink(wsLink){
    var checkContinue = wsLink.indexOf("continue/");
    if(checkContinue == -1){
        var continueWebSocketPath = wsLink.slice(0 , -26)+ "continue/" + wsLink.slice(-26);
        return continueWebSocketPath;
    }
    return wsLink;
}

function removeContinueInLink(wsLink){
    var checkContinue = wsLink.indexOf("continue/");
    if(checkContinue >= 0){
        var webSocketPath = wsLink.slice(0 , -35) + wsLink.slice(-26);
        return webSocketPath;
    }
    return wsLink;
}

function setCookie(){
  document.getElementById("cookie-alert").style.display ="none";
}

window.addEventListener("orientationchange", function() {
  setTimeout(function () {
    lockScreensize();
  }, 300);
});

function lockScreensize(value) {
  if (value == true){
    allowScreensizeLock = true;
  }
  if (allowScreensizeLock == true){
    let viewheight = window.innerHeight;
    let viewwidth = window.innerWidth;
    let viewport = document.querySelector("meta[name=viewport]");
    viewport.setAttribute("content", "height=" + viewheight + "px, width=" + viewwidth + "px, initial-scale=1.0");

    TweenLite.set("#psyk_large", {clearProps:"all"});

  }
}

function inactive(webSocketPath){
    if(!showInactiveMsg){
        var inactiveMessage = '\
        <div id="inactive_message">\
            <div class="message_content">\
                <p>Samtalen er pauset. Vil du fortsætte?</p>\
                <div class="btn_wrap">\
                    <button class="cta-btn ws-connect" ws-connetion=true ws-path="'+webSocketPath+'">Fortsæt</button>\
                    <button class="cta-btn ws-connect" ws-connetion=false ws-path="'+webSocketPath+'">Start forfra</button>\
                </div>\
            </div>\
        </div>';

        document.querySelector('body').insertAdjacentHTML('beforeend' , inactiveMessage);

        showInactiveMsg = true;
    }
}
function hideinactive(){
    $('#inactive_message').remove();
    showInactiveMsg = false;
}

function showRestartChat(){
    var restartChat = '\
    <div id="restart_chat_message" class="restart_chat">\
        <div class="message_content">\
            <p>Vil du genstarte samtalen?</p>\
            <div class="btn_wrap">\
                <button class="cta-btn restart_btn-yes">Ja</button>\
                <button class="cta-btn restart_btn-no">Nej</button>\
            </div>\
        </div>\
    </div>';

    document.querySelector('body').insertAdjacentHTML('beforeend' , restartChat);
    var classRestarChat = document.getElementsByClassName('restart_chat');

    // prevent from creating multiple restart div
    if(classRestarChat.length > 1){
        classRestarChat[1].remove();
    }

    for(var i = 0; i < classRestarChat.length; i++){
        if(classRestarChat.length > 1){
            classRestarChat[i].remove();
        }
    }
    document.addEventListener('click' , function(e){
        if(e.target.classList.contains("restart_btn-yes")){
            resetConversation();
            hideRestartChat();
            chatSocket.close();
            userReset = true;
        }
        if(e.target.classList.contains("restart_btn-no")){
            hideRestartChat()
            userReset = false;
        }
    })
}

function hideRestartChat(){
    $('#restart_chat_message').remove();
}

function wsCreateId(){
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  for (var i = 0; i < 25; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

// go to campaign site
document.addEventListener('click' , function(e){
    if(e.target.classList.contains("campaign-site-btn")){
        window.location = "http://www.vejdirektoratet.dk/DA/om-os/Kampagner/Holdtilhojre/Sider/default.aspx";
    }
})

function trackConversation(chat){
    if(chat.analytics_event){
        ga('send', 'event', 'Track Conversation', chat.analytics_event);
    }
}

function removeMain(serverLink){
    var mainScript = document.querySelector('script[src="'+serverLink+'"]');
    mainScript.remove();
}