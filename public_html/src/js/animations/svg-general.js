var waiting = [
    blink_anim,
    blink_anim,
    blink_anim,
    deepThought_anim,
    blink_anim,
    blink_anim,
    //rollingEyes_anim,
    blink_anim,
    waiting_anim,
    waiting_anim,
    wink_anim
]
var waitingQueue = 0;
var waitingAnimation = true;
var animationRunning = false;
var number = 0;
var queue = [];

function animationQueue(value){
    var doc = document.getElementById("psyk_large");
    if(doc){
        queue.push(value);
        //console.log(value);
    
        //console.log("Animation queue" , queue.length);
        if(queue.length > number && animationRunning == false){
            startAnimations(queue[number]);
        }
    }
    else{
        number = 0;
        queue = []; 
        animationRunning = false;
        waitingAnimation = true;
        waitingQueue = 0;
    }
}

function startAnimations(animation){
    //console.log("start anim");
    animation.anim_function = eval(animation.anim_function);
    animation.anim_function.loaded = false;
    
    var doc = $("#psyk_large");//contentDocument.documentElement
    doc.on('load', function (){
        if (animation.anim_function.loaded ){
            return;
        }
        animation.time_line = animation.anim_function(doc);
        animation.anim_function.loaded = true;
    })
    doc.load();
    
    animationRunning = true;
}

function animationEnd(){ // ( svg-animations.js ) tl - will run this when animation ends
    //console.log("Animation has ended");
    animationRunning = false;
    number++;

    if(queue.length > number){
        startAnimations(queue[number])
    }else{
        if(typeMsg.classList.contains("typing")){
            setTimeout(function(){
                botTypingStop();
            }, 500);
        }
    }

    if(queue.length == number && animationRunning == false && waitingAnimation == true){
        //console.log("Waiting.....")
        waitingQueue++;
        if(waitingQueue == waiting.length){
            waitingQueue = 0;
        }
        setTimeout(function(){
            animationQueue({"anim_function": waiting[waitingQueue]})
        }, 500);
    }
}
