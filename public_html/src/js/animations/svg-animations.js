/**
* Created by bokanstruphansen on 14/06/16.
*/

function home_anim(doc){

  var htl = new TimelineMax({repeat: -1, repeatDelay: 1});
  htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.6, 1, 1.5, -2, 2, 0, 1, "center", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.6, 1, 1.5, -2, 2, 0, 1, "center", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.6, 1, 1.5, 0, 0, -4, 1, "bottom left", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.6, 1, 1.5, 0, 0, 4, 1, "bottom center", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.6, 1, 1.5, 0, 2, 0, 1, "center", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#butterfly", 1, 1, 1, -4, -5, -1, 1, "bottom center", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#hair_shadow", 1, 1, 1, 0, 0, -1, 1, "bottom left", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#XMLID_22_", 0.7, 0.9, 1.7, 0, 0, -2, 0.95, "bottom right", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#face_1_", 1, 1, 1, 0, -5, -3, 1, "bottom center", Power2.easeInOut));
  htl.insert(mainBlink(doc, "#psyk", 1, 1, 1, 0, 0 , -0.5, 1, "bottom center", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#right_x5F_arm", 0.7, 1, 1.6, 0, -8 , -3, 1, "top right", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#left_x5F_arm", 1, 1, 1, 0, 0 , 1, 1, "top left", Power1.easeInOut));
  htl.insert(mainBlink(doc, "#neck_1_", 1, 1, 1, 0, -5, 1, 1, "top center", Power2.easeInOut));
  htl.delay(2);
}

function surprise_anim(doc){

    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(surpriseGlasses(doc, "#glasses", 1.8, 1.5));
    htl.insert(surpriseHair(doc, "#hair_shadow", 1, 1.8, 1.5));
    htl.insert(surpriseHair(doc, "#hair", 1, 1.8, 1.5));
    htl.insert(surpriseHair(doc, "#XMLID_23_", 0, 1.8, 1.5));
    htl.insert(surpriseFace(doc, "#face_1_", 1, 1.75, 1.5));
    htl.insert(surpriseMouth("#XMLID_22_", 1.8, 1.5));
    htl.insert(surpriseEyebrow(doc,  "#eyebrow_x5F_right", "#eyebrow_x5F_left", 10, -10, 1.8, 1.5));
    htl.insert(surpriseEyebrow(doc,  "#cheek_x5F_right", "#cheek_x5F_left", 5, -5, 1.8, 1.5));

    return htl;
}

function moveBody(doc){
  var tl = new TimelineMax({repeat: -1});
  tl.insert(rockIt(doc, "#psyk", 2,"bottom center",  0.5, 5));
  tl.insert(rockItElastic(doc, "#butterfly", -10,"51% 55%", 1, 5));

  return tl;
}

function surpriceUpperface(doc){
  var tl = new TimelineMax({repeat: -1});
  tl.insert(surpriseGlasses(doc, "#glasses", 0.5, 2));
  tl.insert(surpriseEyebrow(doc,  "#eyebrow_x5F_right", "#eyebrow_x5F_left", 10, -10, 0.5, 2));
  tl.insert(surpriseCheeks(doc,  "#cheek_x5F_right", "#cheek_x5F_left", 5, -5, 0.5, 2));
  tl.insert(lookLeft(doc, "#pupil_x5F_right", "#pupil_x5F_left", 0.5, 1));


  return tl;
}

function rockIt(doc, elem_id, rotation, origin, delay, delayBetween){
  var tl = new TimelineMax();

  tl.to(elem_id, 1, {
      rotationZ: -rotation,
      ease:Power3.easeInOut,
      transformOrigin: origin
  }, delay)
  .to(elem_id, 1, {
      rotationZ: rotation,
      ease:Power3.easeInOut
  }, delay + delayBetween)
  .to(elem_id, 1, {
      rotationZ: 0,
      ease:Power3.easeInOut
  }, delay + delayBetween + delayBetween);


  return tl;
}
function rockItElastic(doc, elem_id, rotation, origin, delay, delayBetween){
  var tl = new TimelineMax();

  tl.to(elem_id, 2, {
      rotationZ: -rotation,
      ease:Elastic.easeOut,
      transformOrigin: origin
  }, delay)
  .to(elem_id, 2, {
      rotationZ: rotation,
      ease:Elastic.easeOut
  }, delay + delayBetween)
  .to(elem_id, 2, {
      rotationZ: 0,
      ease:Elastic.easeOut
  }, delay + delayBetween + delayBetween);

  return tl;
}
function lookLeft(doc, elem_1, elem_2, delay, delayBetween){
  var tl = new TimelineMax();

  tl.to(doc.querySelector(elem_1), 0.5, {
      x:10,
      transformOrigin:"bottom left",
      ease:Quad.easeInOut
    }, delay)
    .to(doc.querySelector(elem_2), 0.5, {
      x:5,
      transformOrigin:"bottom left",
      ease:Quad.easeInOut
    }, delay)
    .to(doc.querySelector(elem_1), 0.5, {
        x:0,
        ease:Quad.easeInOut
    }, delay+delayBetween)
    .to(doc.querySelector(elem_2), 0.5, {
        x:0,
        ease:Quad.easeInOut
    }, delay+delayBetween);

  return tl;
}

function mouth_anim(doc){

    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(talkingTeeth(doc,  "#face_1_"));
    htl.insert(talkingTeeth(doc,  "#neck"));
    htl.insert(talkingTeeth(doc,  "#neck_1_"));
    htl.insert(talkingTeeth(doc,  "#butterfly"));
    htl.insert(talkingMouth(doc, "#mouth_x5F_happy"));
    htl.insert(talkingTeeth(doc,  "#cheek_x5F_left"));
    htl.insert(talkingTeeth(doc,  "#cheek_x5F_right"));
    htl.insert(talkingTeeth(doc,  "#eyebrow_x5F_right"));
    htl.insert(talkingTeeth(doc,  "#eyebrow_x5F_left"));
    htl.insert(talkingTeeth(doc,  "#pupil_x5F_right"));
    htl.insert(talkingTeeth(doc,  "#pupil_x5F_"));
    htl.insert(talkingLeftArm(doc, "#left_x5F_arm"));
    htl.insert(talkingRightArm(doc, "#right_x5F_arm"));
    htl.insert(talkingTorso(doc, "#torso"));
    htl.insert(mainBlink(doc, "#hair", 1, 0, 0, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 1, 0, 0, 2, 2, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 1, 0, 0, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 1, 0, 0, 1, 1, 0, 1, "bottom left", Power1.easeInOut));

    return htl;
}
function lowMouth_anim(doc){

    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(talkingTeeth(doc,  "#face_1_"));
    htl.insert(talkingTeeth(doc,  "#neck"));
    htl.insert(talkingTeeth(doc,  "#neck_1_"));
    htl.insert(talkingTeeth(doc,  "#butterfly"));
    htl.insert(talkingMouth(doc, "#mouth_x5F_happy"));
    htl.insert(talkingTeeth(doc,  "#cheek_x5F_left"));
    htl.insert(talkingTeeth(doc,  "#cheek_x5F_right"));
    htl.insert(talkingLeftArm(doc, "#left_x5F_arm"));
    htl.insert(talkingRightArm(doc, "#right_x5F_arm"));
    htl.insert(talkingTorso(doc, "#torso"));

    return htl;
}

function angry_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(angryEyebrows(doc, "#eyebrow_x5F_right", "bottom left", 60));
    htl.insert(angryEyebrows(doc, "#eyebrow_x5F_left", "bottom right", -60));
    htl.insert(angryGlasses(doc, "#glasses", 5, 5));
    htl.insert(angryGlasses(doc, "#face_1_", 15, 15));
    htl.insert(angryGlasses(doc, "#neck_1_", 5, 5));
    htl.insert(angryEyebrows(doc, "#cheek_x5F_right", "bottom left", -8));
    htl.insert(angryEyebrows(doc, "#cheek_x5F_left", "bottom left", -8));
    htl.insert(angryEyebrows(doc, "#hair", "bottom right", -1));
    htl.insert(angryEyebrows(doc, "#hair_horn", "bottom right", -1));
    htl.insert(angryEyes(doc, "#pupil_x5F_right", 0.7));
    htl.insert(angryEyes(doc, "#pupil_x5F_left", 0.8));
    htl.insert(angryMouth(doc, "#XMLID_22_", 1.2, -3));
    htl.insert(angryMouth(doc, "#teeth", 1, -3));

    return htl;
}

function better_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(betterRightHand(doc, "#right_x5F_arm"));
    htl.insert(betterFace(doc, "#face_1_"));
    htl.insert(betterGlasses(doc, "#glasses"));
    htl.insert(betterBody(doc, "#XMLID_24_"));
    htl.insert(betterEyes(doc, "#pupil_x5F_left", -8, 1.05, 0, 0));
    htl.insert(betterEyes(doc, "#pupil_x5F_right", -8, 1.05, 0, 0));
    htl.insert(betterEyes(doc, "#XMLID_22_", 0, 0.5, 40, 20));
    htl.insert(betterBreath(doc, "#breath_1", 1.7, 2.9));
    htl.insert(betterEyebrows(doc, "#eyebrow_x5F_right", "bottom left", 20));
    htl.insert(betterEyebrows(doc, "#eyebrow_x5F_left", "bottom right", -20));
    htl.insert(betterEyebrows(doc, "#cheek_x5F_right", "bottom left", 10));
    htl.insert(betterEyebrows(doc, "#cheek_x5F_left", "bottom left", 10));

    return htl;
}

function waiting_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(waitingEyes(doc, "#pupil_x5F_left"));
    htl.insert(waitingEyes(doc, "#pupil_x5F_right"));
    htl.insert(waitingHead(doc, "#face_1_"));
    htl.insert(waitingMouth(doc, "#XMLID_22_"));

    return htl;
}

function elevator_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(inBetweenEyes(doc, "#pupil_x5F_left",10,2,0, "center", 1));
    htl.insert(inBetweenEyes(doc, "#pupil_x5F_right",10,2,0,"center", 1));
    htl.insert(inBetweenEyes(doc, "#XMLID_22_",2,2,-5,"top right",3));
    htl.insert(inBetweenDown(doc, "#body_x5F_top"));
    htl.insert(inBetweenDown(doc, "#face_1_"));
    htl.insert(inBetweenDown(doc, "#right_x5F_arm"));
    htl.insert(inBetweenDown(doc, "#left_x5F_arm"));
    htl.repeatDelay(1).yoyo(true);

    return htl;
}

function blink_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.2, 1, 0.1, 0, 0, 0, 0, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.2, 1, 0.1, 0, 0, 0, 0, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.2, 1, 0.1, -1, -5, -5, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_right", 0.2, 1, 0.1, -1, -5, 5, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.2, 1, 0.1, 2, 2, -5, 1, "bottom right", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.2, 1, 0.1, -2, 2, 8, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#glasses", 0.2, 1, 0.1, 1.5, 0, 0, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.2, 1, 0.1, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.2, 1, 0.1, 2, 2, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 0.2, 1, 0.1, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.2, 1, 0.1, 1, 1, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.5, 0.6, 0.1, 0, 0, 0, 0.9, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.3, 0.9, 0.1, 0, 0, 0.2, 1, "center center", Power1.easeInOut));
    htl.delay(.6);

    return htl;
}

function wink_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.2, 1, 0.1, 0, 0, 0, 0, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_right", 0.2, 1, 0.1, -1, -5, 5, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.2, 1, 0.1, -2, 2, 8, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#glasses", 0.2, 1, 0.1, 1.5, 0, 0, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.2, 1, 0.1, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.2, 1, 0.1, 2, 2, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 0.2, 1, 0.1, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.2, 1, 0.1, 1, 1, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.3, 0.9, 0.1, 0, 0, 0, 0.8, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.3, 0.9, 0.1, 0, 0, 1, 1, "center center", Power1.easeInOut));

    return htl;
}

function winkLeft_anim(doc){
    var htl = new TimelineMax();
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.2, 1, 0.1, 0, 0, 0, 0, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.2, 1, 0.1, -1, -5, -5, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.2, 1, 0.1, 2, 2, -5, 1, "bottom right", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#glasses", 0.2, 1, 0.1, 1.5, 0, 0, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.2, 1, 0.1, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.2, 1, 0.1, 2, 2, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 0.2, 1, 0.1, 0, 0, 0.5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.2, 1, 0.1, 1, 1, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.5, 0.6, 0.1, 0, 0, 0, 0.9, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.3, 0.9, 0.1, 0, 0, -1, 1, "center center", Power2.easeInOut));
    htl.repeatDelay(1).repeat(10);

    return htl;
}

function rollingEyes_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainEyes(doc, "#pupil_x5F_right"));
    htl.insert(mainEyes(doc, "#pupil_x5F_left"));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.5, 1, 0.2, 0, -5, 0, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_right", 0.5, 1, 0.2, 0, -5, 0, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left",  0.5, 1, 0.2, 0, -2, 3, 1, "bottom right", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right",  0.5, 1, 0.2, 0, -2, -5, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.8, 0.8, 0.8, 0, 10, 0, 0.6, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.5, 0.8, 0.8, 0, 0, 2, 1, "center center", Power1.easeInOut));

    return htl;
}

function wave_anim(doc){
    var htl = new TimelineMax();
    htl.insert(leftArmWave(doc, "#left_x5F_arm", 0.5, 0.1, "top left", Power1.easeInOut));
    htl.insert(bubbleWave(doc, "#talkBubble"));
    htl.add( function(){ console.log('Woohoo!') } )

    htl.repeatDelay(1).repeat(10);

    return htl;
}

function deepThought_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.6, 1, 1.5, 12, -8, 0, 0.9, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.6, 1, 1.5, 12, -8, 0, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.6, 1, 1.5, -1, -5, -5, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.6, 1, 1.5, 2, 2, -10, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.6, 1, 1.5, 2, 2, -10, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.6, 1, 1.5, 1, 1, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.7, 1, 1.5, 0, 2, 0, 0.9, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.6, 1, 1.5, 0, 0, -5, 1, "center center", Power2.easeInOut));

    return htl;
}

function suttleSurprise_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.6, 1, 1.5, 0, -1, 0, 1.1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.6, 1, 1.5, 0, -1, 0, 1.1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.6, 1, 1.5, -1, 6, -10, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_right", 0.6, 1, 1.5, -1, 4, 10, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.6, 1, 1.5, 2, 2, -10, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.6, 1, 1.5, 2, 2, 10, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.6, 1, 1.5, 1, 1, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.7, 0.9, 1.5, 0, 5, 0, 1.1, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.6, 1, 1.5, 0, 0, 5, 1, "center center", Power2.easeInOut));

    return htl;
}

function puppyEyes_anim(doc){
    var htl = new TimelineMax();
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.6, 1, 1.5, 0, -10, 0, 1.4, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.6, 1, 1.5, 0, -10, 0, 1.4, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.6, 1, 1.5, -1, -2, -10, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_right", 0.6, 1, 1.5, -1, -4, 10, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.6, 1, 1.5, 2, 2, -10, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.6, 1, 1.5, 2, 2, 10, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_lower", 0.6, 1, 1.5, 0, 0, 1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.6, 1, 1.5, 1, 1, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.7, 0.9, 1.5, 0, 10, 0, 0.7, "center center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.6, 1, 1.5, 0, 0, 1, 1, "center center", Power2.easeInOut));
    htl.repeatDelay(1).repeat(10);

    return htl;
}

function sceptical_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.6, 1, 1.5, -10, 2, 0, 0.9, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.6, 1, 1.5, -10, 2, 0, 1.05, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_left", 0.6, 1, 1.5, -1, -4, -10, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#cheek_x5F_right", 0.6, 1, 1.5, -1, 4, -10, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.6, 1, 1.5, -5, -5, -10, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.6, 1, 1.5, 2, 15, -20, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair", 0.6, 1, 1.5, 0, 0, -1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 0.6, 1, 1.5, 0, 0, -2, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_horn", 0.6, 1, 1.5, -2, -2, 0, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.7, 0.9, 1.5, -20, 10, -6, 0.7, "top right", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 0.6, 1, 1.5, 0, 25, -4, 1, "center center", Power2.easeInOut));
    htl.insert(mainBlink(doc, "#butterfly", 0.6, 1, 1.5, 5, 10, 2, 1, "center center", Power2.easeInOut));
    htl.insert(mainBlink(doc, "#left_x5F_arm", 0.6, 1, 1.5, 0, 0, 2, 1, "top left", Power2.easeInOut));
    htl.insert(mainBlink(doc, "#right_x5F_arm", 0.6, 1, 1.5, 0, -10, -2, 1, "top right", Power2.easeInOut));
    htl.insert(mainBlink(doc, "#neck_1_", 0.6, 1, 1.5, 0, 10, -1, 1, "top center", Power2.easeInOut));

    return htl;
}

function waitingBody_anim(doc){
    var htl = new TimelineMax({onComplete:animationEnd});
    htl.insert(mainBlink(doc, "#pupil_x5F_left", 0.6, 1, 1.5, -4, -1, 0, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#pupil_x5F_right", 0.6, 1, 1.5, -4, -1, 0, 1, "center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_right", 0.6, 1, 1.5, 0, 0, -4, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#eyebrow_x5F_left", 0.6, 1, 1.5, 0, 0, 4, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#butterfly", 1, 1, 1, -4, -5, -1, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#hair_shadow", 1, 1, 1, 0, 0, -1, 1, "bottom left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#XMLID_22_", 0.7, 0.9, 1.7, 0, 0, -2, 0.95, "bottom right", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#face_1_", 1, 1, 1, 0, -5, -3, 1, "bottom center", Power2.easeInOut));
    htl.insert(mainBlink(doc, "#psyk", 1, 1, 1, 0, 0 , -0.5, 1, "bottom center", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#right_x5F_arm", 0.7, 1, 1.6, 0, -8 , -3, 1, "top right", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#left_x5F_arm", 1, 1, 1, 0, 0 , 1, 1, "top left", Power1.easeInOut));
    htl.insert(mainBlink(doc, "#neck_1_", 1, 1, 1, 0, -5, 1, 1, "top center", Power2.easeInOut));

    htl.repeatDelay(1);

    return htl;
}

function bubbleWave(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 5, {
        autoAlpha:0
    },"+=2.5")
    .to(elem_id, 0, {
        autoAlpha:0
    })
    return tl;
}

function mainEyes(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 1.3, {
        rotation:360,
        transformOrigin:"50% 1.5px",
        ease:Linear.easeNone
    },"+=1")

    return tl;
}

function leftArmWave(doc, elem1, delay, delayBetween, toTransform, toEase) {
    var tl = new TimelineMax();
    tl.to(elem1, 1, {
        x: -60,
        y: 50,
        rotationZ: -70,
        transformOrigin: toTransform,
        ease: toEase
    }, delay)
    .to(elem1, 0.5, {
        x:-100,
        y:80,
        rotationZ: -90,
        transformOrigin: toTransform,
        ease:  toEase
    }, '+= '+ delayBetween +'')
    .to(elem1, 0.5, {
        x: -60,
        y: 50,
        rotationZ: -70,
        transformOrigin: toTransform,
        ease:  toEase
    })
    .to(elem1, 0.5, {
        x:-100,
        y:80,
        rotationZ: -90,
        transformOrigin: toTransform,
        ease:  toEase
    })
    .to(elem1, 0.8, {
        y:0,
        x:0,
        rotationZ: 0,
        transformOrigin: toTransform,
        ease:  toEase
    }, "+=0.1")

    return tl;
}

function mainBlink(doc, elem1, length, delay, delayBetween, toX, toY, toRotateZ, toScale, toTransform, toEase) {
    var tl = new TimelineMax();
    tl.to(elem1, length, {
        x: toX,
        y: toY,
        rotationZ: toRotateZ,
        scale: toScale,
        transformOrigin: toTransform,
        ease: toEase
    }, delay)
    .to(elem1, length, {
        y:0,
        x:0,
        rotationZ: 0,
        scale:1,
        transformOrigin: toTransform,
        ease:  toEase
    }, '+= '+ delayBetween +'')

    return tl;
}

function hairBounce(doc, elem1) {
    var tl = new TimelineMax();
    tl.to(elem1, 0.5, {
        y:-5,
        x:-15,
        rotationZ: -1,
        scale: 1.05,
        transformOrigin: "top left",
        ease: Bounce.easeOut,
    })
    .to(elem1, 0.5, {
        y:0,
        x:0,
        rotationZ: 0,
        scale:1,
        transformOrigin: "top left",
        ease:  Bounce.easeOut,
    })

    return tl;
}

function inBetweenDown(doc, elem_id) {
    var tl = new TimelineMax();
    tl.to(elem_id, 4,  {
        y:1000,
        ease:Power0.easeOut
    }, 1)
    .to(elem_id, 2,  {
        y:0,
        ease: Power1.easeInOut,
    }, "+=0.5")
    return tl
}

function inBetweenEyes(doc, elem_id, x, y, z, transform, delay){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.8, {
        x:x,
        y:y,
        rotationZ: z,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    }, delay)
    .to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    }, "+=1")
    .to(elem_id, 0.6, {
        x:x,
        y:y,
        rotationZ: z,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    }, "+=1")
    .to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    }, "+=1")
    return tl
}



function moveBody(doc){
  var tl = new TimelineMax({repeat: -1});
  tl.insert(rockIt(doc, "#psyk", 2,"bottom center",  0.5, 5));
  tl.insert(rockItElastic(doc, "#butterfly", -10,"51% 55%", 1, 5));

  return tl;
}

function surpriceUpperface(doc){
  var tl = new TimelineMax({repeat: -1});
  tl.insert(surpriseGlasses(doc, "#glasses", 0.5, 2));
  tl.insert(surpriseEyebrow(doc,  "#eyebrow_x5F_right", "#eyebrow_x5F_left", 10, -10, 0.5, 2));
  tl.insert(surpriseCheeks(doc,  "#cheek_x5F_right", "#cheek_x5F_left", 5, -5, 0.5, 2));
  tl.insert(lookLeft(doc, "#pupil_x5F_right", "#pupil_x5F_left", 0.5, 1));


  return tl;
}

function rockIt(doc, elem_id, rotation, origin, delay, delayBetween){
  var tl = new TimelineMax();

  tl.to(doc.querySelector(elem_id), 1, {
      rotationZ: -rotation,
      ease:Power3.easeInOut,
      transformOrigin: origin
  }, delay)
  .to(doc.querySelector(elem_id), 1, {
      rotationZ: rotation,
      ease:Power3.easeInOut
  }, delay + delayBetween)
  .to(doc.querySelector(elem_id), 1, {
      rotationZ: 0,
      ease:Power3.easeInOut
  }, delay + delayBetween + delayBetween);


  return tl;
}
function rockItElastic(doc, elem_id, rotation, origin, delay, delayBetween){
  var tl = new TimelineMax();

  tl.to(doc.querySelector(elem_id), 2, {
      rotationZ: -rotation,
      ease:Elastic.easeOut,
      transformOrigin: origin
  }, delay)
  .to(doc.querySelector(elem_id), 2, {
      rotationZ: rotation,
      ease:Elastic.easeOut
  }, delay + delayBetween)
  .to(doc.querySelector(elem_id), 2, {
      rotationZ: 0,
      ease:Elastic.easeOut
  }, delay + delayBetween + delayBetween);

  return tl;
}
function lookLeft(doc, elem_1, elem_2, delay, delayBetween){
  var tl = new TimelineMax();

  tl.to(doc.querySelector(elem_1), 0.5, {
      x:10,
      transformOrigin:"bottom left",
      ease:Quad.easeInOut
    }, delay)
    .to(doc.querySelector(elem_2), 0.5, {
      x:5,
      transformOrigin:"bottom left",
      ease:Quad.easeInOut
    }, delay)
    .to(doc.querySelector(elem_1), 0.5, {
        x:0,
        ease:Quad.easeInOut
    }, delay+delayBetween)
    .to(doc.querySelector(elem_2), 0.5, {
        x:0,
        ease:Quad.easeInOut
    }, delay+delayBetween);

  return tl;
}

function waitingNote(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 1.5, {
        x:1600,
        y:430,
        rotationZ:10,
        autoAlpha:0
    },"+=4.2")
    .to(elem_id, 0, {
        x:1550,
        y:500,
        rotationZ:0,
        autoAlpha:0
    })
    .to(elem_id, 1.2, {
        x:1600,
        y:430,
        rotationZ:10,
        autoAlpha:1
    },"+=0.2")
    .to(elem_id, 0.2, {
        x:1600,
        y:430,
        rotationZ:0,
        autoAlpha:0
    })
    .to(elem_id, 0, {
        x:1550,
        y:500,
        rotationZ:0,
        autoAlpha:0
    })

    return tl;
}

function waitingEyes(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        x:-10,
        y:-10,
        ease: Power1.easeInOut,
    }, "+=0.5")
    .to(elem_id, 1, {
        x:0,
        y:0,
        ease: Power1.easeInOut,
    },"+=1")
    .to(elem_id, 1, {
        x:15,
        y:5,
        ease: Power1.easeInOut,
    },"+=1")
    .to(elem_id, 1, {
        x:0,
        y:0,
        ease: Power1.easeInOut,
    },"+=1")

    return tl
}

function waitingHead(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ:5,
        ease: Power1.easeInOut,
        transformOrigin: "bottom center",
    }, "+=0.5")
    .to(elem_id, 1, {
        x:0,
        y:0,
        rotationZ:0,
        transformOrigin: "bottom center",
        ease: Power1.easeInOut,
    },"+=1")
    .to(elem_id, 1, {
        x:0,
        y:0,
        rotationZ:-5,
        transformOrigin: "bottom center",
        ease: Power1.easeInOut,
    },"+=1")
    .to(elem_id, 1, {
        x:0,
        y:0,
        rotationZ:0,
        transformOrigin: "bottom center",
        ease: Power1.easeInOut,
    },"+=1")

    return tl
}

function waitingMouth(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        scale:0.9
    }, "+=0.5")
    .to(elem_id, 1, {
        scale:1
    },"+=1")
    .to(elem_id, 0.6, {
        scale:0.4, x:70, y:10, rotationZ:-5
    },"+=0.5")
    .to(elem_id, 0.4, {
        scale:0.8, x:50, y:7.5, rotationZ:-5
    },"+=0.5")
    .to(elem_id, 0.4, {
        scale:0.4, x:70, y:10, rotationZ:-5
    },"+=0.5")
    .to(elem_id, 0.5, {
        scale:1, x:0, y:0, rotationZ:0
    },"+=1")

    return tl
}

function betterGlasses(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.8, {
        rotationZ:30,
        x:20,
        y:-50,
        ease: Power0.easeNone,
        transformOrigin: "bottom right",
    }, "+=1.3")
    .to(elem_id, 0.8, {
        rotationZ:60,
        x:180,
        y:520,
        ease: Power0.easeOut,
        transformOrigin: "bottom right",
    })
    .to(elem_id, 0.8, {
        rotationZ:30,
        x:20,
        y:-50,
        ease: Power0.easeNone,
        transformOrigin: "bottom right",
    }, "+=2.05")
    .to(elem_id, 0.8, {
        x:0,
        y:0,
        rotationZ:0,
        transformOrigin: "bottom right",
        ease: Power0.easeNone
    }, "+=0.05")

    return tl
}

function betterRightHand(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.8, {
        css:{scale:0.95, transformOrigin:"top right", rotationZ:-118, x:-300, y:-100,},
    })
    .to(elem_id, 0.8, {
        ease: Power0.easeNone,
        css:{scale:1, transformOrigin:"top right", rotationZ:-118, x:-300, y:-110,}
    },"+=0.5")
    .to(elem_id, 0.8, {
        ease: Power0.easeNone,
        css:{scale:1, transformOrigin:"top right", rotationZ:-78, x:-200, y:-110,}
    })
    .to(elem_id, 0.8, {
        ease: Power0.easeNone,
        css:{scale:1, transformOrigin:"top right", rotationZ:-118, x:-300, y:-110,}
    }, "+=2")
    .to(elem_id, 0.8, {
        ease: Power0.easeNone,
        css:{scale:0.95, transformOrigin:"top right", rotationZ:-118, x:-340, y:-100,},
    })
    .to(elem_id, 0.8, {
        ease: Power0.easeNone,
        css:{scale:1, transformOrigin:"top right", rotationZ:0, x:0, y:0,},
    }, "+=0.5")


    return tl
}

function betterFace(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        x:15,
        y:10,
        rotationZ:5,
        ease: Power2.easeOut,
        transformOrigin: "bottom center",
    }, "+=0.6")
    .to(elem_id, 0.6, {
        x:-5,
        y:-5,
        rotationZ:-5,
        transformOrigin: "bottom center",
        ease: Power2.easeOut,
    }, "+=1.2")
    tl.to(elem_id, 0.6, {
        x:-20,
        y:20,
        rotationZ:5,
        ease: Power2.easeOut,
        transformOrigin: "bottom center",
    }, "+=2")
    .to(elem_id, 0.6, {
        x:-38,
        y:12,
        rotationZ:0,
        transformOrigin: "bottom center",
        ease: Power2.easeOut,
    },"+=1.5")

    return tl
}

function betterBody(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        x:5,
        y:-10,
        rotationZ:1,
        ease: Power1.easeInOut,
        transformOrigin: "top center",
    }, "+=2.4")
    .to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ:0,
        transformOrigin: "top center",
        ease: Power1.easeInOut,
    },"+=2")

    return tl
}

function betterBreath(doc, elem_id, length, delay){
    var tl = new TimelineMax();
    tl.
    to(elem_id, length, {
        x: 1300,
        y: 670,
        autoAlpha:0,
    }, delay)

    return tl
}

function betterEyes(doc, elem_id, glance, scale, x, y ){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.3, {
        scale:1.1, x:-10, y:-5,
        ease: Power1.easeInOut
    }, "+=0.1")
    .to(elem_id, 0.3, {
        scale:scale, x:x, y:y,
        ease: Power1.easeInOut
    }, "+=1.8")
    .to(elem_id, 0.3, {
        scale:1, x: 0, y:0,
        ease: Power1.easeInOut
    }, "+=1.2")
    .to(elem_id, 0.3, {
        scale:1, x: glance, y:0,
        ease: Power1.easeInOut
    }, "+=2,7")
    .to(elem_id, 0.5, {
        scale:1, x:0, y:0,
        ease: Power1.easeInOut
    }, "+=1.5")

    return tl
}

function betterEyebrows(doc, elem_id, transform, rotation){
    var tl = new TimelineMax();
    tl.to(elem_id, 1, {
        x:0,
        y:0,
        rotationZ: rotation,
        transformOrigin:transform,
        ease: Power1.easeInOut,
    }, "+=2")
    .to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    }, "+=.8")
    return tl;
}

function angryMouth(doc, elem_id, size, rotate){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        ease: Power1.easeInOut,
        scale:size, rotationZ:rotate,
    })
    .to(elem_id, 0.6, {
        scale:1, rotationZ:0,
        ease: Power1.easeInOut,
    }, "+=2")

    return tl;
}

function angryEyes(doc, elem_id, scaleSize, color){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        scale:scaleSize, transformOrigin:"51% 55%",
        ease: Power1.easeInOut
    })
    .to(elem_id, 0.6, {
        scale: 1, transformOrigin:"51% 55%",
        ease: Power1.easeInOut
    }, "+=2")

    return tl
}

function angryArm(doc, elem_id, rotation, x, y, transform){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        x:x,
        y:y,
        rotationZ: rotation,
        transformOrigin:transform,
        ease: Power1.easeInOut,
    })
    .to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    },"+=2");
    return tl;
}

function angryBow(doc, elem_id){
    var tl = new TimelineMax();

    tl.to(elem_id, 1, {
        scale:1.4, transformOrigin:"51% 55%", rotationZ:-360,
        ease:  Power1.easeOut
    })
    .to(elem_id, 1, {
        scale:1, transformOrigin:"51% 55%", rotationZ:0,
        ease:  Power1.easeOut
    }, "+=1.2");
    return tl;
}

function angryGlasses(doc, elem_id, x, y){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        x:x,
        y:y,
        rotationZ: 0,
        transformOrigin:"bottom center",
        ease: Power1.easeInOut,
    })
    .to(elem_id, 0.6, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin: "bottom center",
        ease: Power1.easeInOut,
    },"+=2")
    return tl
}

function angryEyebrows(doc, elem_id, transform, rotation){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.6, {
        rotationZ: rotation,
        transformOrigin:transform,
        ease: Power1.easeInOut,
    })
    .to(elem_id, 0.6, {
        rotationZ: 0,
        transformOrigin: transform,
        ease: Power1.easeInOut,
    },"+=2")
    return tl
}

function talkingTorso(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 1, {
        rotationZ: -0.5,
        transformOrigin:"bottom center",
        ease: Power1.easeInOut,
    })
    .to(elem_id, 1, {
        rotationZ: 0,
        transformOrigin:"bottom center",
        ease: Power1.easeInOut,
    })
    return tl
}

function talkingLeftArm(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 1, {
        x:-10,
        y:0,
        rotationZ: -5,
        transformOrigin:"top left",
        ease: Power1.easeInOut,
    })
    .to(elem_id, 1, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin:"top left",
        ease: Power1.easeInOut,
    })
    return tl
}

function talkingRightArm(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.8, {
        x:-5,
        y:-18,
        rotationZ: -5,
        transformOrigin:"top right",
        ease: Power0.easeNone,
    })
    .to(elem_id, 0.8, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin:"top right",
        ease: Power0.easeNone,
    })

    return tl
}

function talkingTeeth(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.2, {
        x:0.5,
        y:0.5,
        rotationZ: 0,
        transformOrigin:"bottom left",
        ease: Power1.easeInOut,
    })
    .to(elem_id, 0.2, {
        x:0,
        y:0,
        rotationZ: -1,
        transformOrigin:"bottom right",
    })
    .to(elem_id, 0.2, {
        x:0.25,
        y:0.25,
        rotationZ: 0,
        transformOrigin:"bottom left",
    })
    .to(elem_id, 0.2, {
        x:0.25,
        y:0.25,
        rotationZ: 1,
        transformOrigin:"bottom right",
    })
    tl.to(elem_id, 0.2, {
        x:-0.5,
        y:-0.5,
        rotationZ: 0,
        transformOrigin:"bottom left",
    })
    .to(elem_id, 0.2, {
        x:0,
        y:0,
        rotationZ: 1,
    })
    .to(elem_id, 0.2, {
        x:-0.5,
        y:-0.5,
        rotationZ: 0,
        transformOrigin:"bottom left",
    })
    .to(elem_id, 0.2, {
        x:-0.5,
        y:-0.5,
        rotationZ: 0.5,
        transformOrigin:"bottom left",
    })
    .to(elem_id, 0.2, {
        x:0,
        y:0,
        rotationZ: 0,
        transformOrigin:"bottom left",
    })

    return tl
}

function talkingMouth(doc, elem_id){
    var tl = new TimelineMax();
    tl.to(elem_id, 0.2, {
        scale:1.1, transformOrigin:"center center",
        attr: {
            scale:1.1, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:0.8, transformOrigin:"center center",
        attr: {
            scale:0.8, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:1, transformOrigin:"center center",
        attr: {
            scale:1, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:0.8, transformOrigin:"center center",
        attr: {
            scale:0.8, transformOrigin:"center center"
        }
    })
    tl.to(elem_id, 0.2, {
        scale:1.2, transformOrigin:"center center",
        attr: {
            scale:1.2, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:1, transformOrigin:"center center",
        attr: {
            scale:1, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:1.2, transformOrigin:"center center",
        attr: {
            scale:1.2, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:0.8, transformOrigin:"center center",
        attr: {
            scale:0.8, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        scale:1, transformOrigin:"center center",
        attr: {
            scale:1, transformOrigin:"center center"
        }
    })
    .to(elem_id, 0.2, {
        x:0,
        y:0,
        rotationZ: 0,
        ease: Power1.easeInOut
    });

    return tl
}

function surpriseBody(doc, elem_id, delay, delayBetween){
    var tl = new TimelineMax();

    tl.to(elem_id, 0.8, {
        scale:1.5,
        ease:ExpoScaleEase.config(1, 1.5, Power1.easeOut)
    }, delay)
    .to(elem_id, 0.8, {
        scale:1,
        ease:ExpoScaleEase.config(1.5, 1, Power1.easeOut)
    }, '+= '+ delayBetween +'');

    return tl;
}

function surpriseFace(doc, elem_id, rotation, delay, delayBetween){
    var tl = new TimelineMax();

    tl.to(elem_id, 0.5, {
        x:10,
        rotationZ: -5,
        transformOrigin:"bottom left",
        ease:Quad.easeInOut
    }, delay)
    .to(elem_id, 0.5, {
        x:0,
        rotationZ: 0,
        transformOrigin:"bottom left",
        ease:Quad.easeInOut
    }, '+= '+ delayBetween +'');

    return tl;
}

function surpriseHair(doc, elem_id, rotation, delay, delayBetween){
    var tl = new TimelineMax();

    tl.to(elem_id, 0.5, {
        rotationZ: rotation,
        ease:Quad.easeInOut,
        transformOrigin:"top right"
    }, delay)
    .to(elem_id, 0.5, {
        rotationZ: 0,
        ease:Quad.easeInOut,
        transformOrigin:"top right"
    }, '+= '+ delayBetween +'');

    return tl;
}
function surpriseMouth(elem_id, delay, delayBetween){
    var tl = new TimelineMax();

    tl.to(elem_id, 0.5, {
        scale:1.2
    }, delay)
    .to(elem_id, 0.5, {
        scale:1
    }, '+= '+ delayBetween +'');

    return tl;
}

function surpriseBow(doc, elem1, length, ease, rotation, delay){
    console.log('anim', doc);
    var tl = new TimelineMax();

    tl.to(elem1, length, {
        rotationZ:rotation,
        transformOrigin:"51% 55%",
        ease: ease
    }, delay)

    return tl;
}
function spinBow(doc, elem1, delay, delayBetween){
    console.log('anim', doc);

    var tl = new TimelineMax();

    tl.to(doc.querySelector(elem1), 3,{
        rotationZ:20,
        transformOrigin:"51% 55%",
        ease: Back.easeOut
    }, delay)

    return tl;
}
function surpriseGlasses(doc, elem_id, delay, delayBetween){
    var tl = new TimelineMax();

    tl.to(elem_id, 0.5, {
        y:-10,
        ease:Quad.easeInOut
    }, delay)
    .to(elem_id, 0.5, {
        y:0,
        ease:Quad.easeInOut
    }, '+= '+ delayBetween +'');

    return tl;
}

function surpriseEyebrow(doc, elem_id_1, elem_id_2, rotationLeft, rotationRight, delay, delayBetween){

    if (delay == undefined) {
      delay = 0;
    }
    var tl = new TimelineMax();

    tl.to(elem_id_1, 0.5, {
          y: -10,
          x: -5,
          rotationZ: rotationLeft,
          transformOrigin:"top center",
          ease: Quad.easeInOut,
      }, delay)
      .to(elem_id_1, 0.5, {
          y: 0,
          x: 0,
          rotationZ: 0,
          transformOrigin:"top center",
          ease: Quad.easeInOut,
      }, '+= '+ delayBetween +'');

      tl.to(elem_id_2, 0.5, {
        y: 0,
        x: 5,
        rotationZ: rotationRight,
        transformOrigin:"top center",
        ease: Quad.easeInOut,
    }, delay)
    .to(elem_id_2, 0.5, {
        y: 0,
        x: 0,
        rotationZ: 0,
        transformOrigin:"top center",
        ease: Quad.easeInOut,
    }, "-=0.5");

    return tl;
  }
  function surpriseCheeks(doc, elem_id_1, elem_id_2, rotationLeft, rotationRight, delay, delayBetween){

      if (delay == undefined) {
        delay = 0;
      }
      var tl = new TimelineMax();

      tl.to(elem_id_1, 0.5, {
            y: -20,
            x: -5,
            rotationZ: rotationLeft,
            transformOrigin:"top center",
            ease: Quad.easeInOut,
        }, delay)
        .to(elem_id_1, 0.5, {
            y: 0,
            x: 0,
            rotationZ: 0,
            transformOrigin:"top center",
            ease: Quad.easeInOut,
        }, '+= '+ delayBetween +'');

        tl.to(elem_id_2, 0.5, {
          y: -5,
          x: 5,
          rotationZ: rotationRight,
          transformOrigin:"top center",
          ease: Quad.easeInOut,
      }, delay)
      .to(elem_id_2, 0.5, {
          y: 0,
          x: 0,
          rotationZ: 0,
          transformOrigin:"top center",
          ease: Quad.easeInOut,
      }, "-=0.5");

      return tl;
    }

function swayHair(doc, elem1){

    var tl = new TimelineMax({repeat: -1});

    tl.insert(TweenMax.fromTo(elem1, 2, {
        rotationZ: 0,
        y: 0
    }, {
        y: 20,
        rotationZ: -25,
        repeat: -1,
        yoyo: true,
        ease: Quad.easeInOut
    }));
    return tl;
}

function sway(doc, elem_id){

    var balloon = elem_id;
    var tl = new TimelineMax({
        repeat: -1,
        ease:Linear.easeNone
    });

    //Up and down motion
    tl.insert(TweenMax.fromTo(balloon, 2, {
        y: 0
    }, {
        y: -10,
        repeat: -1,
        yoyo: true,
        ease: Quad.easeInOut
    }));
    tl.insert(TweenMax.fromTo(balloon, 1.5, {
        rotation: 1
    }, {
        transformOrigin: "70% 50%",
        rotation: -1,
        repeat: -1,
        yoyo: true,
        ease: Quad.easeInOut
    }));

    return tl;
}

function twitch_left(doc, elem_id){

  var elem = elem_id;
  var tl = new TimelineMax({
      ease:Linear.easeNone
  });

  //Up and down motion
  tl.insert(TweenMax.fromTo(elem, 0.5, {
      y: 0
  }, {
      y: 25,
      x: 10,
      yoyo: true,
      rotationZ: -30,
      transformOrigin:"bottom center",
      ease: Quad.easeInOut
  }));

  return tl;
}

function twitch_right(doc, elem_id, delay, delayBetween){

  if (delay == undefined) {
    delay = 0;
  }
  var tl = new TimelineMax();


  tl.to(elem_id, 0.5, {
        y: 25,
        x: 10,
        rotationZ: 30,
        transformOrigin:"bottom center",
        ease: Quad.easeInOut,
    }, '+= '+ delay +'')
    .to(elem_id, 0.5, {
        y: 0,
        x: 0,
        rotationZ: 0,
        transformOrigin:"bottom center",
        ease: Quad.easeInOut,
    }, '+= '+ delayBetween +'')
  return tl;
}

function rotateButterfly(doc, elem1, delay){

    var tl = new TimelineMax({repeat: -1});

    tl.insert(TweenMax.to(doc.querySelector(elem1), 1,
    {
        rotationZ:720,
        transformOrigin:"51% 55%", ease:Circ.easeInOut, delay: delay
    }))

    return tl;
};

