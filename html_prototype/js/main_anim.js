
var debug = false;


$(document).ready(function() {

    //display_title_page();

    $('#fullpage').fullpage({
        anchors: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],
        css3: true,
        scrollingSpeed: 850,
        onLeave: function(index, nextIndex, direction){
            console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " +  direction);
            change_left_image(nextIndex);
            toggleArrow(nextIndex);
        },
        afterLoad: function(anchorLink, index){
            toggleArrow(index);
            checkOrientation();
        }
    });

    //document.getElementById("svg_object").addEventListener("load", function() {
        change_left_image(1);
    //});    


    $(document).on('click', '.settings-button', function(event) {
      $("#settings-container").toggleClass( "visible" );
    });
    $(document).on('click', '.arrow.up', function(event) {
      $.fn.fullpage.moveSectionUp();  
    });
    $(document).on('click', '.arrow.down', function(event) {
      $.fn.fullpage.moveSectionDown();  
    });

    $(window ).resize(function() {
        checkOrientation();
        $.fn.fullpage.reBuild();
    });          
});


function checkOrientation(){
    if(window.innerHeight > window.innerWidth){
        $("#main").addClass("horisontal");
    } else if ($("#main").hasClass("horisontal")){
        $("#main").removeClass("horisontal");
    } else {
        return;
    }
}

function toggleArrow(index){
    var sectionsLength = $('.section').length;
    if(index==1){
        $(".arrow.up").addClass("hidden");
    } else if($(".arrow.up").hasClass("hidden")) {
        $(".arrow.up").removeClass("hidden")
    };
    if(index==sectionsLength){
        $(".arrow.down").addClass("hidden");
    } else if($(".arrow.down").hasClass("hidden")) {
        $(".arrow.down").removeClass("hidden")
    }      
}

function display_title_page(){

    var display_story = debug ? 1.5 : 0;

    $(left_image_id).css('opacity', '1');
    var tl = new TimelineMax();

    tl
      .to($('#title_page') , 1.5 - display_story, { opacity:0 , ease:Back.easeInOut, delay: 2 - display_story })
      .to($('.story') , 1.5 - display_story, {  opacity:1 , ease:Power1.easeInOut })
      .to($('#title_page') , 0, { display: 'none'});
}


function change_reading_light(){
    console.log('Changing reading light');
    $("body").toggleClass( "night" );
}



//var running_function = false;
function load_callback(section_index){

    section_image_links[section_index].callback();
    //document.getElementById("svg_object").onload = null;
/*
    if (section_index == running_function)
        return;

    setTimeout( function(){
        running_function = section_index;
        section_image_links[section_index].callback();
    }, 500 );
*/
}


var left_image_id = false;
var current_section_index = null;

function change_left_image(section_index){

    var new_id = "#" + section_images_meta[section_index].svg_id;
    var old_image = $(left_image_id),
        new_image = $(new_id);

    var tl = new TimelineMax();

    if (left_image_id && new_id && left_image_id != new_id){
        tl.to(old_image , 0.6, { opacity:0 , ease:Back.easeInOut })
          .to(new_image , 0.6, {  opacity:1 , ease:Power1.easeInOut });
        changeAnimation(current_section_index, section_index);
    } else {
        TweenMax.to(new_image , 0.6, {  opacity:1 , ease:Power1.easeInOut });
    }

    left_image_id = new_id;
    current_section_index = section_index;
}