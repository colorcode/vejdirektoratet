// Display chat messages
function displayMessage(author , message){
    var newMsg = document.createElement("li");
    var newMsgText = document.createElement("p");

    newMsgText.innerHTML = message;
    if(author == "user"){
        newMsg.className = "user-msg";
        inputMsg.value = "";
    }else{
        newMsg.className = "bot-msg";
    }
    newMsg.classList.add("msg");
    newMsg.appendChild(newMsgText);
    
    document.getElementById("display-message").appendChild(newMsg);
    setTimeout( function(){
        newMsg.classList.add("visible");
    }, 100);
    gotoBottom("msg-box");
    return;
}

// scroll down to the latest message
function gotoBottom(id){
    var element = document.getElementById(id);
    element.scrollTop = element.scrollHeight - element.clientHeight;
}