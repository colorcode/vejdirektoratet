//***************************************
// BOT
// Bot reply
socket.on("reply message" , function(data){
    questionData = data;
    botReplyMessage(data);
    // selecting answer options
    if(data.type === "text_input" || data.type === "number_input"){
        showInputField();
        hideSelectionBox();
    }else if(data.type === "choice"){
        setTimeout( function(){
            createSelectionBox(data);
            hideInputField();
        }, 1800);
    }else{
        hideInputField();
        hideSelectionBox();
    }
});

// Bot reply messages
function botReplyMessage(data){
    var typeMsg = document.getElementById("type-message");

    // Bot typing animation
    setTimeout( function(){
        typeMsg.classList.add("typing");
    }, 300)

    // Bot reply
    setTimeout( function(){
        // remove animation
        if(typeMsg.classList.contains("typing")){
          typeMsg.classList.remove("typing")
        };

        setTimeout( function(){
            displayMessage("bot" , data.text)
        }, 300)
    }, 1500)
};