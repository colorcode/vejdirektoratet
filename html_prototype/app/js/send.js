var socket = io();
var countSendMsg = 0;
var allowSend = true;

//***************************************
// USER
// save input messages
var conversationLog = [];
function sendUserInput(data){
    var conversation = {};
    conversation.question = questionData;
    conversation.answer = data;
    console.log(conversationLog);
    conversationLog.push(conversation);

    socket.emit("user send message" , data);
    displayMessage("user" , data.value);

    // Avoid spamming
    countSendMsg++;
    if(countSendMsg > 0){
        allowSend = false;
        setTimeout( function(){
            allowSend = true;
            countSendMsg = 0;
        }, 2000)
    }
    return;
}
