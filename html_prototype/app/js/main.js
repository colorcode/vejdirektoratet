var inputMsg = document.getElementById("inputMsg");
var chatBox = document.getElementById("chat-box");
var btnSendMsg = document.getElementById("btn-submit");
var selectionBoxVisible = false;
var boxSelected = false;
var questionData;

// Afspil ved start
$(document).ready(function(){
    socket.on("welcome" , function(data){
        questionData = data;
        botReplyMessage(data);
        showInputField();
    })
})

// Press Send btn to submit
document.addEventListener("click" , function(e){
    if(e.target.classList == "submit-message"){
        if(inputMsg.value.length != 0 && allowSend == true){
            var inputMessage = inputMsg.value;
            getUserAnswer(inputMessage);
        }
    }
})

// Press enter to submit
document.addEventListener("keyup" , function(e){
    if(e.keyCode === 13){
        e.preventDefault();
        if(inputMsg.value.length != 0 && allowSend == true){
            var inputMessage = inputMsg.value;
            getUserAnswer(inputMessage);
        }
    }
})

// Selection box click to answer
document.addEventListener("click" , function(e){
    if(e.target.classList == "select"){
        var checkMarkIcon = "<svg id='m-check-mark' width='49px' height='34.6px' viewBox='0 0 245 173' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><g id='Page-1' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' stroke-opacity='1'><polyline id='checkmark' stroke='#000000' stroke-width='30' points='5.640625 83.7607422 83.2539062 161.663086 238.97168 6.11328125'></polyline></g></svg>"
        var selectedId = e.target.getAttribute("data-boxId");
        var selectedValue = e.target.getAttribute("value");
        if(boxSelected == false){
            // find and get value from answer Id
            document.getElementById(selectedId).insertAdjacentHTML("beforeend" , checkMarkIcon);
            boxSelected = true;
            var positionOfTitle = questionData.options.findIndex(function(item){
                return item.id === selectedValue
            });
            var selectedTitle = questionData.options[positionOfTitle].title;

            // 
            setTimeout(function(){
                getUserAnswer(selectedTitle);
                hideSelectionBox();
            }, 800);
        }
    }
})

// Create of selection box
// options att. is an array contain multiple choice
function createSelectionBox(data){
    if(selectionBoxVisible == false){
        for(var i = 0; i < data.options.length; i++){
            var selectionDiv = document.createElement("div");
            var selectionBox = document.createElement("div");
            var selectionText = document.createElement("p");
            selectionDiv.className = "selection";
            selectionBox.className = "checkBox";
            selectionBox.id = "select-"+i;
            selectionText.className = "select";
            selectionText.setAttribute("data-boxId" , "select-"+i);
            selectionText.setAttribute("value" , data.options[i].id);
            selectionText.innerHTML = data.options[i].title;
            selectionDiv.appendChild(selectionBox);
            selectionDiv.appendChild(selectionText);
            document.getElementById("select-box").appendChild(selectionDiv);
            setTimeout( function(){
                document.getElementById("select-box").classList.add("visible");
            }, 100);
        }
        selectionBoxVisible = true;
        gotoBottom("msg-box");
        hideInputField();
    }
}

// Hide selection box
function hideSelectionBox(){
    document.getElementById("select-box").innerHTML = "";
    document.getElementById("select-box").classList.remove("visible");
    selectionBoxVisible = false;
    boxSelected = false;
}

// Show Input field
function showInputField(){
    inputMsg.removeAttribute("disabled");
    chatBox.classList.remove("hide");
    btnSendMsg.classList.remove("hide");
}

// Hide Input field
function hideInputField(){
    inputMsg.setAttribute("disabled" , "true");
    chatBox.classList.add("hide");
    btnSendMsg.classList.add("hide");
}

// Collect user data
function getUserAnswer(answer){
    answerData = {};
    answerData.type = questionData.type;
    answerData.value = answer;
    answerData.id = questionData.id;
    sendUserInput(answerData);
}