//***************************************
// Test server
 var testServer = { 
     "approve":
     [
        {
            "type":"text_input" , 
            "text":"Hej, hvad hedder du?" ,
            "id":"name_answer" ,
            "emotion":{"anim_function": "mouth_anim"}
        } ,
        {
            "type":"number_input" , 
            "text":"Hvor gammel er du?" ,
            "id":"age_answer",
            "emotion":{"anim_function": "better_anim"}
        } , 
        {
            "type":"choice" , 
            "text":"Hvad er din yndlingsfarve?" ,
            "options":[
                {"id":"farve_1" , "title":"Grøn"} ,
                {"id":"farve_2" , "title":"Blå"} ,
                {"id":"farve_3" , "title":"Rød"} ,
                {"id":"farve_4" , "title":"Gul"}
            ] ,
            "id":"color_choice",
            "emotion":{"anim_function": "surprise_anim"}
        } ,
        {
            "type":"choice" , 
            "text":"Har du et kørekort?" ,
            "options":[
                {"id":"svar_1" , "title":"Ja"} ,
                {"id":"svar_2" , "title":"Nej"} 
            ] ,
            "id":"driver_license_choice",
            "emotion":{"anim_function": "angry_anim"}
        } ,
        {
            "type":"text_input" , 
            "text":"Hvilken forsikring har du?" ,
            "id":"insurance_answer",
            "emotion":{"anim_function": "surprise_anim"}
        } ,
        {
            "type":"message" , 
            "text":"Tak for dit svar." ,
            "id":"end_message",
            "emotion":{"anim_function": "better_anim"}
        } 
    ] , 
    "error":
    {
        "type":"text" , 
        "text":"Angive venligst et tal" , 
        "id":"Error_number_input",
        "emotion":{"anim_function": "better_anim"}
    }
 }
module.exports = testServer;