var express = require("express");
var app = express();
var http = require("http");
var server =  http.createServer(app);
var path = require("path");
var io = require("socket.io")(server);
var chalk = require("chalk");

// Link to Test server
var testServer = require(__dirname + "/test_server.js");

app.use(express.static(path.join(__dirname + "/../vejdirektoratet/dist")));

//***************************************
// Display messages
global.gLog = (sStatus , sMessage) => {

    switch(sStatus){
        case 'ok':
            console.log(chalk.green(sMessage))
        break;
        case 'err':
            console.log(chalk.red(sMessage))
        break;
        case 'ex':
            console.log(chalk.magenta(sMessage))
        break;
        case 'info':
            console.log(chalk.grey.bgWhite(sMessage))
        break;
    }
}

//***************************************
// Navigate page
app.get("/" , (req , res) => {
    try{
        // Javascript
        //res.sendFile(path.join(__dirname , "/../app/index.html"));

        // Angular CLI
        res.sendFile(path.join(__dirname , "/../vejdirektoratet/dist/index.html"));
        return;
    }catch(err){
        return gLog("err" , "Get request error: " + err);
    }
})
app.get("/chat" , (req , res) => {
    try{
        // Javascript
        //res.sendFile(path.join(__dirname , "/../app/index.html"));

        // Angular CLI
        res.sendFile(path.join(__dirname , "/../vejdirektoratet/dist/index.html"));
        return;
    }catch(err){
        return gLog("err" , "Get request error: " + err);
    }
})

//***************************************
// Socket.io
io.on("connection" , (socket) => {
    try{
        // Test server
        var questionNumber = 0;

        // Create private room
        socket.join(socket.id);

        // Socket.io starts here
        gLog("info" , "Connection to socket.io");

        // Show welcome message
        io.to(socket.id).emit("welcome" , testServer.approve[0])

        socket.on("user send message" , (data) => {
            // Users input
            console.log("message: " , data)

            // Reply message
            if(data.type === "number_input"){
                if( Number.isInteger(Number(data.value)) === true){
                    questionNumber++;
                    io.to(socket.id).emit("reply message" , testServer.approve[questionNumber]);
                    return;
                }else{
                    io.to(socket.id).emit("reply message" , testServer.error);
                    setTimeout(function(){
                        io.to(socket.id).emit("reply message" , testServer.approve[questionNumber]);
                    },500);
                    return;
                }
            }else if(data.type === "text_input" || data.type === "choice"){
                questionNumber++;
                io.to(socket.id).emit("reply message" , testServer.approve[questionNumber]);
                return;
            }
            
        });

        socket.on("disconnect" , () => {
            socket.disconnect() 
        });

    }catch(err){
        return gLog("err" , "Error in socket.io: " + err);
    }
})

//***************************************
// Listen to port
var port = process.env.PORT || "4200";
app.set("port" , port);
server.listen(port , (err) => {
    if(err){
        console.log("Error listen to port: " + port);
        return;
    }
    console.log("Listen to port: " + port);
})
